from cassieRLEnvMirror import *

import argparse
import os
import sys
import gym
from gym import wrappers
import random
import numpy as np

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

from params import Params

import pickle
from model import ActorCriticNet, Shared_obs_stats

env = cassieRLEnvMirror()

u = pd_in_t()
u.leftLeg.motorPd.torque[3] = 0 # Feedforward torque
u.leftLeg.motorPd.pTarget[3] = -2
u.leftLeg.motorPd.pGain[3] = 1000
u.leftLeg.motorPd.dTarget[3] = -2
u.leftLeg.motorPd.dGain[3] = 100
u.rightLeg.motorPd = u.leftLeg.motorPd

num_inputs = env.observation_space.shape[0]
num_outputs = env.action_space.shape[0]

model = ActorCriticNet(num_inputs, num_outputs,[256, 256])
model.load_state_dict(torch.load("torch_model/cassie3dMirror2kHz256X256Oct09.pt"))
with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
	shared_obs_stats = pickle.load(input)

weight = model.p_fcs[0].weight
state = env.reset_by_phase(phase=4)
state = Variable(torch.Tensor(state).unsqueeze(0))
state = shared_obs_stats.normalize(state)
output1 = (model.p_fcs[0](state))
output1 = F.relu(output1)
print(output1)
activation_index = []
output1_np = output1.data.squeeze().numpy()
for i in range(output1_np.shape[0]):
	if output1_np[i] > 0:
		activation_index.append(i)
		#print(i)
print((activation_index))

state = env.reset_by_phase(phase=4)
noise = np.random.normal(0, 0.1, 46)
state[0:46] += noise
for i in range(10000):
	state = Variable(torch.Tensor(state).unsqueeze(0))
	state = shared_obs_stats.normalize(state)
	if env.phase == 4:
		output1 = (model.p_fcs[0](state))
		output1 = F.relu(output1)
#print(output1)
		activation_index_2 = []
		output1_np = output1.data.squeeze().numpy()
		for i in range(output1_np.shape[0]):
			if output1_np[i] > 0:
				activation_index_2.append(i)
		#print((activation_index_2))

		print("diff", (list(set(activation_index)-(set(activation_index_2)))))
		print("diff2", (list(set(activation_index_2)-(set(activation_index)))))
		activation_index = activation_index_2.copy()
		#print((list(set(activation_index_2)-(set(activation_index)))))
	mu, log_std, v = model(state)
	#print("mu", mu)
	env_action = mu.data.squeeze().numpy()
	state, reward, done, _ = env.step(env_action)
	if env.phase == 4:
		state[5] += 103.1#np.random.normal(0, 0.1, 1)
	env.vis.draw(env.sim)
	#print("reward", reward)
	#if done:
	#	print("robot falls")
	#	break
'''output2 = model.p_fcs[1](output1)
output2 = F.relu(output2)
print(output2)
final_output = F.tanh(model.mu(output2))
print(final_output)
mu, log_std, v = model(state)
print(mu)'''
