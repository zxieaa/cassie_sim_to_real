import pickle
import numpy as np
import matplotlib.pyplot as plt

'''with open ("trajectory/torque_forward_Nov15", 'rb') as fp:
	torque = pickle.load(fp)

with open ("trajectory/target_forward_Nov15", 'rb') as fp:
	target = pickle.load(fp)

with open ("trajectory/motor_angle_forward_Nov15", 'rb') as fp:
	pos = pickle.load(fp)

with open ("trajectory/motor_angle_forward_Nov15_noisy", 'rb') as fp:
	pos_noisy = pickle.load(fp)

with open ("trajectory/motor_vel_forward_Nov15_noisy", 'rb') as fp:
	vel_noisy = pickle.load(fp)

with open ("trajectory/motor_vel_forward_Nov15", 'rb') as fp:
	vel = pickle.load(fp)

with open ("trajectory/ref_Nov15", 'rb') as fp:
	ref = pickle.load(fp)'''

with open ("trajectory/torque_v0_June", 'rb') as fp:
	torque0 = pickle.load(fp)

with open ("trajectory/target_v0_June", 'rb') as fp:
	target0 = pickle.load(fp)

with open ("trajectory/motor_angle_v0_June", 'rb') as fp:
	pos0 = pickle.load(fp)

with open ("trajectory/motor_vel_v0_June", 'rb') as fp:
	vel0 = pickle.load(fp)

with open ("trajectory/torque_v1_June", 'rb') as fp:
	torque1 = pickle.load(fp)

with open ("trajectory/target_v1_June", 'rb') as fp:
	target1 = pickle.load(fp)

with open ("trajectory/motor_angle_v1_June", 'rb') as fp:
	pos1 = pickle.load(fp)

with open ("trajectory/motor_vel_v1_June", 'rb') as fp:
	vel1 = pickle.load(fp)

with open('trajectory/pelvis_pos_speed0', 'rb') as fp:
	recorded_pelvis_pos = pickle.load(fp)
with open('trajectory/pelvis_vel_speed0', 'rb') as fp:
	recorded_pelvis_vel = pickle.load(fp)
with open('trajectory/left_foot_pos_speed0', 'rb') as fp:
	recorded_left_foot_pos = pickle.load(fp)
with open('trajectory/right_foot_pos_speed0', 'rb') as fp:
	recorded_right_foot_pos = pickle.load(fp)
with open('trajectory/stance_foot_pos_speed0', 'rb') as fp:
	recorded_stance_foot_pos = pickle.load(fp)

'''with open ("trajectory/motor_angle_forward_Jan_noisy", 'rb') as fp:
	pos_noisy = pickle.load(fp)

with open ("trajectory/motor_vel_forward_Jan_noisy", 'rb') as fp:
	vel_noisy = pickle.load(fp)

with open ("trajectory/motor_vel_forward_Jan", 'rb') as fp:
	vel = pickle.load(fp)

with open ("trajectory/ref_Nov15", 'rb') as fp:
	ref = pickle.load(fp)'''

motor_torque0 = np.zeros((5*1682, 10))
motor_pos0 = np.zeros((5*1682, 10))
motor_target0 = np.zeros((5*1682, 10))
motor_vel0 = np.zeros((5*1682, 10))
motor_torque1 = np.zeros((5*1682, 10))
motor_pos1 = np.zeros((5*1682, 10))
motor_target1 = np.zeros((5*1682, 10))
motor_vel1 = np.zeros((5*1682, 10))

pelvis_pos = np.zeros((5 * 1682, 3))
pelvis_vel = np.zeros((5 * 1682, 3))
left_foot_pos = np.zeros((5 * 1682, 3))
right_foot_pos = np.zeros((5 * 1682, 3))
stance_foot_pos = np.zeros((5 * 1682, 3))
'''motor_pos_noisy = np.zeros((5*1682, 10))
motor_ref = np.zeros((5*1682, 10))
motor_vel_noisy = np.zeros((5*1682, 10))
motor_vel = np.zeros((5*1682, 10))'''

for i in range(5*1682):
	for j in range(10):
		motor_torque0[i][j] = torque0[i][j]
		motor_pos0[i][j] = pos0[i][j]*180/np.pi
		motor_target0[i][j] = target0[i][j]*180/np.pi
		motor_vel0[i][j] = vel0[i][j]
		motor_torque1[i][j] = torque1[i][j]
		motor_pos1[i][j] = pos1[i][j]*180/np.pi
		motor_target1[i][j] = target1[i][j]*180/np.pi
		motor_vel1[i][j] = vel1[i][j]
		'''motor_pos_noisy[i][j] = pos_noisy[i][j]*180/np.pi
		motor_ref[i][j] = ref[i][j]*180/np.pi
		motor_vel_noisy[i][j] = vel_noisy[i][j]
		motor_vel[i][j] = vel[i][j]'''
	for j in range(3):
		pelvis_pos[i][j] = recorded_pelvis_pos[i][j]
		pelvis_vel[i][j] = recorded_pelvis_vel[i][j]
		left_foot_pos[i][j] = recorded_left_foot_pos[i][j]
		right_foot_pos[i][j] = recorded_right_foot_pos[i][j]
		stance_foot_pos[i][j] = recorded_stance_foot_pos[i][j]

motor_torque0 = motor_torque0.T
motor_pos0 = motor_pos0.T
motor_target0 = motor_target0.T
motor_vel0 = motor_vel0.T
motor_torque1 = motor_torque1.T
motor_pos1 = motor_pos1.T
motor_target1 = motor_target1.T
motor_vel1 = motor_vel1.T

pelvis_pos = pelvis_pos.T
pelvis_vel = pelvis_vel.T
right_foot_pos = right_foot_pos.T
left_foot_pos = left_foot_pos.T
stance_foot_pos = stance_foot_pos.T

for t in range(1):
	plt.plot(pelvis_pos[0][t*1682:(t+1)*1682] - stance_foot_pos[0][t*1682:(t+1)*1682], pelvis_vel[0][t*1682:(t+1)*1682], 'r')
	#plt.plot(pelvis_pos[1][t*1682:(t+1)*1682] - right_foot_pos[1][t*1682:(t+1)*1682], pelvis_vel[1][t*1682:(t+1)*1682], 'b')
	#plt.plot(left_foot_pos[1][t*1682:(t+1)*1682], 'g')
	#plt.plot(right_foot_pos[1][t*1682:(t+1)*1682], 'b')
plt.show()

'''motor_pos_noisy = motor_pos_noisy.T
motor_ref = motor_ref.T
motor_vel_noisy = motor_vel_noisy.T
motor_vel = motor_vel.T'''

'''for i in range(2, 3):
	plt.subplot(1, 1, 1)
	plt.xlabel('time (ms)')
	plt.ylabel('left knee(rad)')
	for t in range(5):
		if t == 0:
			plt.plot(motor_pos[i][t*1682:(t+1)*1682:2] / 180 * np.pi,'b', label='behavior cloning')#, motor_target[i],'r')
			plt.plot(motor_pos_noisy[i][t*1682:(t+1)*1682:2] / 180 * np.pi,'g', label='DASS')
		else:
			plt.plot(motor_pos[i][t*1682:(t+1)*1682:2] / 180 * np.pi,'b')#, motor_target[i],'r')
			plt.plot(motor_pos_noisy[i][t*1682:(t+1)*1682:2] / 180 * np.pi,'g')'''
	#plt.plot(motor_ref[i][:],'r')
	#plt.plot(motor_torque[i][:], 'g')
	#plt.plot(motor_pos[i][:])


'''legend = ["left abduction(rad)", "left yaw(rad)", "left hip(rad)", "left knee(rad)", "left toe(rad)", "right abduction(rad)", "right yaw(rad)", "right hip(rad)", "right knee(rad)", "right toe(rad)"]
for t in range(1):
	for i in range(5):
		plt.subplot(1, 5, i+1)
		if i % 5 == 0:
			plt.ylabel('rad/s')
		plt.xlabel(legend[i])
		plt.plot(motor_pos0[i][t*1682:(t+5)*1682] / 180 * np.pi, motor_vel0[i][t*1682:(t+5)*1682], 'g')#, motor_target[i],'r')
		plt.plot(motor_pos1[i][t*1682:(t+5)*1682] / 180 * np.pi, motor_vel1[i][t*1682:(t+5)*1682], 'r')
	plt.show()'''
		#plt.plot(motor_pos_noisy[i][t*1682:(t+1)*1682] / 180 * np.pi, motor_vel_noisy[i][t*1682:(t+1)*1682], 'b')
		#plt.subplots_adjust(left=0.0, right=10.1, top=10.1, bottom=0.1, wspace=0.1, hspace=0.1)
		#plt.plot(motor_ref[i][t*1682:(t+1)*1682],'r')
	#for i in range(10):
		#plt.subplot(4, 5, i + 11)
		#plt.plot(motor_torque[i][t*1682:(t+1)*1682])

#plt.legend(loc='upper right')
#plt.tight_layout()
#plt.savefig('phase_portrait.png')
#plt.show()
#plt.savefig('phase_portrait.png', bbox_inches='tight')
