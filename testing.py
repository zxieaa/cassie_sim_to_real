from cassiemujoco import *
from cassieRLEnv import *
from cassieRLEnvInvariant import *
from cassieRLEnvWithMoreState import *
from cassieRLEnvAccelPenalty import *
from cassieRLEnvSimpleJump import *
from cassieRLEnvMultiSkill import *
from cassieRLEnvHorizon import *
from cassieRLEnvMultiDirection import *
from cassieRLEnvMultiTraj import *
from cassieRLEnvMirror import *
from cassieRLEnvTerrain import *
from cassieRLEnvStablePelvis import * 
from cassieRLEnvNoRef import *
from cassieRLEnvBias import *
from cassieRLEnvSym import *
from cassieRLEnvStanding import *
import time as t

import argparse
import os
import sys
import gym
from gym import wrappers
import random
import numpy as np

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

from params import Params

import pickle
from model import ActorCriticNet, Shared_obs_stats, ActorCriticNetWithPhase, ActorCriticNetWithBias
from symmetric_net import *

#env = cassieRLEnStepInPlaceInvariant()
#env = cassieRLEnvMultiTraj()
#env = cassieRLEnvNoRef()
#env = cassieRLEnvHigherFootSlowInvariant()
env = cassieRLEnvSym()
#env = cassieRLEnvStablePelvis()

#env_name = "mocca_envs:CassieMocapPhaseRSI2DEnv-v0"
#env = gym.make(env_name)

u = pd_in_t()
u.leftLeg.motorPd.torque[3] = 0 # Feedforward torque
u.leftLeg.motorPd.pTarget[3] = -2
u.leftLeg.motorPd.pGain[3] = 1000
u.leftLeg.motorPd.dTarget[3] = -2
u.leftLeg.motorPd.dGain[3] = 100
u.rightLeg.motorPd = u.leftLeg.motorPd

num_inputs = env.observation_space.shape[0]
num_outputs = env.action_space.shape[0]

#model = ActorCriticNet(num_inputs, num_outputs,[256, 256])
model = SymmetricPolicyValue(**env.mirror_counts, num_layers=2, hidden_size=[30, 30, 98], log_std=-2, varying_std=False)
env.delay = False
env.noisy = False
with open('torch_model/cassie3dRLEnvSym_shared_obs_stats.pkl', 'rb') as input:
	shared_obs_stats = pickle.load(input)

state_list = []
#print(model.p_fcs[0].weight.data)

def run_test():
	t.sleep(1)
	#env.scale = 2
	#env.sim.model.hfield_size[0][2] = 0.4
	state = env.reset()
	#print(env.speed)
	#env.speed = 0
	#env.speed = 0.0
	total_reward = 0
	done = False
	total_10_reward = 0
	current_scale = 0
	clock = 0
	done = False
	current_phase = 0

	for i in range(10):
		state = env.reset()
		#print("speed",env.speed, "y_speed", env.y_speed)
		#print("push bias", env.push_bias)
		#env.speed = -1.0
		total_reward = 0
		counter = 0
		interpolate = 1
		while counter < 100000 and not done:
			start = t.time()
			#print(env.speed)
			for j in range(1):
				#print(env.speed)
				#env.sim.apply_force(np.array([-0,0,0,0,0,0]))

				counter += 1
				clock += 1
				state = Variable(torch.Tensor(state).unsqueeze(0))
				sym_state = th.cat([
					state[0,0:8],
					-state[0,8:14],
					state[0,31:],
					state[0,14:31],
				]).unsqueeze(0)
				state = shared_obs_stats.normalize(state)
				sym_state = shared_obs_stats.normalize(sym_state)

				state2 = th.cat([
					sym_state[0,0:8],
					-sym_state[0,8:14],
					sym_state[0,31:],
					sym_state[0,14:31],
				]).unsqueeze(0)

				#print(state2 - state)

				# print(state)
				mu, _, v = model(state)
				sym_mu, _, sym_v = model(sym_state)
				sym_sym_mu = th.cat([
					sym_mu[0,7:12],
					sym_mu[0,2:7],
				]).unsqueeze(0)
				print(mu[0, 2:12] - sym_sym_mu)
				print(v)
				print(v - sym_v)
				#print(mu)
				#sym_sym_mu
				#print(mu[0,2:] - sym_sym_mu)
				#state = shared_obs_stats.normalize(state)
				'''state[0, 46:] = torch.zeros(1, 39)
				state[0, 46] = env.speed
				state[0, 47] = (env.phase % 14) / 28.0
				state[0, 48] = env.push_bias'''
				#print(state.data)
				#mu, log_std, v = model(state)
				eps = torch.randn(mu.size())
				mu = (mu + np.exp(-2)*0*Variable(eps))

				env_action = mu.data.squeeze().numpy() * 1
				#print(env_action[1])
				#env_action[1] = 0
				#env_action[6] = 0
				#print(env_action)
				#print("target", env_action[0]*180/np.pi)
				#print(env.speed)

				#env_action = env_action1
				#print("scale", env.scale)
				#print("interpolate", interpolate)
				#env_action = interpolate*env_action1+(1-interpolate)*env_action2
				state, reward, done, _ = env.step(env_action)
				#print(state)
				env.vis.draw(env.sim)
				#env.render()
				#print(reward)
				total_reward += reward
				force = np.zeros(12)
				#print(env.get_foot_forces())
				#t.sleep(1)
				#env.get_foot_height()
			while True:
				stop = t.time()
				#print(stop-start)
				if stop - start > 0.03 * (env.control_rate / 60):
					break
				#print("stop")
			#total_reward += reward
		done = False
		counter = 0
		total_10_reward += total_reward
		print("total rewards", total_reward)
	print(total_10_reward)

def play_kin():
	env.vis.draw(env.sim)
	#env.set_data()
	env.phase = 0
	#env.speed = -0.0
	env.reset()
	env.phase = 0
	t.sleep(1)
	while True:
		#print(env.phase)
		start = t.time()
		while True:
			stop = t.time()
			#print(stop-start)
			#print("stop")
			if stop - start > 0.033:
				break
		pos, vel = env.get_kin_state()
		if env.phase == 6:
			print(pos[[7, 8, 9, 14, 20, 21, 22, 23, 28, 34]])
			print(vel)
			print(env.phase)
		env.phase += 1
		#print(env.speed)
		if env.phase >= env.max_phase:
			env.phase = 0
			env.counter += 1
			#break
		env.set_state(pos, vel)
		y = env.sim.step_pd(u)
		env.vis.draw(env.sim)
#play_kin()
run_test()
