from cassieRLEnvWithMoreState import *
from cassiemujoco_ctypes import *
import math
from quaternion_function import *


class cassieRLEnvAccelPenalty(cassieRLEnvWithFootForces):
    def __init__(self):
        super().__init__()
        self.motor_velocity_last = np.zeros(4)
        self.motor_acceleration_sq = np.zeros(4)
        #self.P = np.array([200, 200, 200, 200, 50, 200, 200, 200, 200, 50])
        #self.D = np.array([30.0, 30.0, 30.0, 30.0, 5.0, 30.0, 30.0, 30.0, 30.0, 5.0])
        self.D = np.array([30.0, 30.0, 8.0, 9.6, 5.0, 30.0, 30.0, 8.0, 9.6, 5.0])
        self.max_acc = 0
        self.action = np.zeros(10)
        self.prev_action = 0

    def step_simulation(self, action):
        actual_action = np.copy(action)
        actual_action[0] *= 0.5
        actual_action[1] *= 0.5
        actual_action[5] *= 0.5
        actual_action[6] *= 0.5
        #print("action", self.prev_action / action[0])
        #self.prev_action = action[0]
        super().step_simulation(actual_action)
        self.action = np.copy(action)
        temp_vel = np.zeros(4)
        temp_vel[0] = self.cassie_state.motor.velocity[0]
        temp_vel[1] = self.cassie_state.motor.velocity[1]
        temp_vel[2] = self.cassie_state.motor.velocity[5]
        temp_vel[3] = self.cassie_state.motor.velocity[6]
        self.motor_acceleration_sq += ((temp_vel - self.motor_velocity_last) / 5e-4)**2
        #print("acc"self.motor_acceleration_sq[0])
        self.motor_velocity_last = np.copy(temp_vel)

    def reset(self):
        self.motor_velocity_last = np.zeros(4)
        self.motor_acceleration_sq = np.zeros(4)
        return super().reset()

    def reset_for_test(self):
        self.motor_velocity_last = np.zeros(4)
        self.motor_acceleration_sq = np.zeros(4)
        return super().reset_for_test()

    def compute_reward(self):
        rms_motor_acceleration = np.sum(np.sqrt(self.motor_acceleration_sq / 60))
        if rms_motor_acceleration > self.max_acc and self.time > 10:
            self.max_acc = rms_motor_acceleration
        self.motor_acceleration_sq = np.zeros(4)

        ref_pos, ref_vel = self.get_kin_state()
        weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
        joint_penalty = 0

        joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
        vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

        for i in range(10):
            error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
            joint_penalty += error*30

        pelvis_pos = self.cassie_state.pelvis.position[:]
        pelvis_pos[2] -= self.cassie_state.terrain.height

        desired_x = ref_pos[0]*np.cos(self.orientation)
        desired_y = ref_pos[1]*np.sin(self.orientation)

        com_penalty = (pelvis_pos[0]-desired_x)**2 + (pelvis_pos[1]-desired_y)**2 + (pelvis_pos[2]-ref_pos[2])**2
        #print("x", pelvis_pos[0]-desired_x, "y", pelvis_pos[1]-desired_y)

        yaw = quat2yaw(self.sim.qpos()[3:7])

        orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2
        orientation_penalty += (self.sim.qvel()[3])**2+(self.sim.qvel()[4])**2+(self.sim.qvel()[5])**2

        spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
        spring_penalty *= 1000

        foot_forces = self.get_foot_forces()
        #print(foot_forces)
        force_penalty = 0
        if self.foot_forces[0] < 10 and foot_forces[2] > 10:
            force_penalty += (foot_forces[2]-200)**2
        if self.foot_forces[1] < 10 and foot_forces[8] > 10:
            force_penalty += (foot_forces[8]-200)**2
        self.foot_forces[0] = foot_forces[2]
        self.foot_forces[1] = foot_forces[8]
            # print(0.5*np.exp(-joint_penalty), 0.3*np.exp(-com_penalty), 0.2*np.exp(-30*orientation_penalty))
        action_penalty = np.sum(abs(self.action))/10.0

        total_reward = 0.3*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-10*orientation_penalty)+0.1*np.exp(-force_penalty/1000.0)+0.2*np.exp(-rms_motor_acceleration/300.0)#0.0*np.exp(-force_penalty/1000.0)+0.0*np.exp(-rms_motor_acceleration/300.0)
        print('joint penalty: {}, com penalty: {}, orientation penalty: {}, acc penalty: {}'.format(joint_penalty, com_penalty, orientation_penalty, rms_motor_acceleration))

        return total_reward