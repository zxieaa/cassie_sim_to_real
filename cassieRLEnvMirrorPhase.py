from cassiemujoco_ctypes import *
import math
import gym
import gym.spaces
import pickle
from quaternion_function import *
from cassieRLEnvMirror import *


class cassieRLEnvMirrorPhase(cassieRLEnvMultiTraj):
	def __init__(self):
		super().__init__()
		self.observation_space = np.zeros(47)

		self.ref_pos = []
		for i in range(14):
			self.phase = i
			ref_pos, ref_vel = self.get_kin_next_state()
			self.ref_pos.append(ref_pos[[7, 8, 9, 14, 20, 21, 22, 23, 28, 34]])
		for i in range(14, 28):
			ref_pos_pseudo = np.copy(self.ref_pos[i-14])
			ref_pos = np.zeros(10)
			ref_pos[[0, 1]] = -ref_pos_pseudo[[5, 6]]
			ref_pos[[5, 6]] = -ref_pos_pseudo[[0, 1]]
			ref_pos[[2, 3, 4]] = ref_pos_pseudo[[7, 8, 9]]
			ref_pos[[7, 8, 9]] = ref_pos_pseudo[[2, 3, 4]]
			self.ref_pos.append(ref_pos)

		self.record_state = False
		self.recorded = False
		self.leg_state = []
		self.time_limit = 400


	def get_mirror_state(self):
		state = self.cassie_state
		ref_pos, ref_vel = np.copy(self.get_kin_next_state())
		pos_index = np.array([2,3,4,5,6,21,22,23,28,29,30,34,7,8,9,14,15,16,20])
		vel_index = np.array([0,1,2,3,4,5,19,20,21,25,26,27,31,6,7,8,12,13,14,18])
		ref_vel[1] = -ref_vel[1]
		euler = quaternion2euler(ref_pos[3:7])
		euler[0] = -euler[0]
		euler[2] = -euler[2]
		ref_pos[3:7] = euler2quat(z=euler[2],y=euler[1],x=euler[0])
		#print(ref_pos[3:7])
		quaternion = euler2quat(z=-self.orientation, y=0, x=0)
		iquaternion = inverse_quaternion(quaternion)

		pelvis_euler = quaternion2euler(np.copy(state.pelvis.orientation[:]))
		pelvis_euler[0] = -pelvis_euler[0]
		pelvis_euler[2] = -pelvis_euler[2]
		pelvis_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
		#print(pelvis_orientation, state.pelvis.orientation[:])

		translational_velocity = np.copy(state.pelvis.translationalVelocity[:])
		translational_velocity[1] = -translational_velocity[1]

		translational_acceleration = np.copy(state.pelvis.translationalAcceleration[:])
		translational_acceleration[1] = -translational_acceleration[1]

		rotational_velocity = np.copy(state.pelvis.rotationalVelocity)
		rotational_velocity[0] = -rotational_velocity[0]
		rotational_velocity[2] = -rotational_velocity[2]

		motor_position = np.zeros(10)
		motor_position[0:5] = np.copy(state.motor.position[5:10])
		motor_position[5:10] = np.copy(state.motor.position[0:5])
		motor_position[0] = -motor_position[0]
		motor_position[1] = -motor_position[1]
		motor_position[5] = -motor_position[5]
		motor_position[6] = -motor_position[6]

		motor_velocity = np.zeros(10)
		motor_velocity[0:5] = np.copy(state.motor.velocity[5:10])
		motor_velocity[5:10] = np.copy(state.motor.velocity[0:5])
		motor_velocity[0] = -motor_velocity[0]
		motor_velocity[1] = -motor_velocity[1]
		motor_velocity[5] = -motor_velocity[5]
		motor_velocity[6] = -motor_velocity[6]

		joint_position = np.zeros(6)
		joint_position[0:3] = np.copy(state.joint.position[3:6])
		joint_position[3:6] = np.copy(state.joint.position[0:3])

		joint_velocity = np.zeros(6)
		joint_velocity[0:3] = np.copy(state.joint.velocity[3:6])
		joint_velocity[3:6] = np.copy(state.joint.velocity[0:3])

		left_toeForce = np.copy(state.rightFoot.toeForce[:])
		left_toeForce[1] = -left_toeForce[1]
		left_heelForce = np.copy(state.rightFoot.heelForce[:])
		left_heelForce[1] = -left_heelForce[1]
		#print("phase", self.phase)
		#print("left", left_toeForce)

		right_toeForce = np.copy(state.leftFoot.toeForce[:])
		right_toeForce[1] = -right_toeForce[1]
		right_heelForce = np.copy(state.leftFoot.heelForce[:])
		right_heelForce[1] = -right_heelForce[1]
		#print("right", right_toeForce)
		
		new_orientation = quaternion_product(iquaternion, pelvis_orientation)
		new_translationalVelocity = rotate_by_quaternion(translational_velocity, iquaternion)
		new_translationalAcceleration = rotate_by_quaternion(translational_acceleration, iquaternion)
		new_rotationalVelocity = rotate_by_quaternion(rotational_velocity, quaternion)

		useful_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], motor_position, new_translationalVelocity[:], rotational_velocity, motor_velocity, new_translationalAcceleration[:], joint_position, joint_velocity]))
		return np.concatenate([useful_state, [((self.phase + 14) % 28) / 28]])

	def get_mirror_action(self, action):
		mirror_action = np.zeros((1, 10))
		mirror_action[0, [0, 1]] = -action[0, [5, 6]]
		mirror_action[0, [5, 6]] = -action[0, [0, 1]]
		mirror_action[0, [2, 3, 4]] = action[0, [7, 8, 9]]
		mirror_action[0, [7, 8, 9]] = action[0, [2, 3, 4]]
		return mirror_action
	def get_state(self):
		if len(self.state_buffer) > 0:
			random_index = random.randint(0, len(self.state_buffer)-1)
			state = self.state_buffer[random_index]
		else:
			state = self.cassie_state
		state = self.cassie_state

		ref_pos, ref_vel = np.copy(self.get_kin_next_state())

		if self.phase < 14:
			pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
			vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])
			quaternion = euler2quat(z=self.orientation, y=0, x=0)
			iquaternion = inverse_quaternion(quaternion)
			new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
			#print(new_orientation)
			new_translationalVelocity = rotate_by_quaternion(state.pelvis.translationalVelocity[:], iquaternion)
			#print(new_translationalVelocity)
			new_translationalAcceleration = rotate_by_quaternion(state.pelvis.translationalAcceleration[:], iquaternion)
			new_rotationalVelocity = rotate_by_quaternion(state.pelvis.rotationalVelocity[:], quaternion)
			useful_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], state.motor.position[:], new_translationalVelocity[:], state.pelvis.rotationalVelocity[:], state.motor.velocity[:], new_translationalAcceleration[:], state.joint.position[:], state.joint.velocity[:]]))
			return np.concatenate([useful_state, [(self.phase % 14) / 14]])
		else:
			pos_index = np.array([2,3,4,5,6,21,22,23,28,29,30,34,7,8,9,14,15,16,20])
			vel_index = np.array([0,1,2,3,4,5,19,20,21,25,26,27,31,6,7,8,12,13,14,18])
			ref_vel[1] = -ref_vel[1]
			euler = quaternion2euler(ref_pos[3:7])
			euler[0] = -euler[0]
			euler[2] = -euler[2]
			ref_pos[3:7] = euler2quat(z=euler[2],y=euler[1],x=euler[0])
			#print(ref_pos[3:7])
			quaternion = euler2quat(z=-self.orientation, y=0, x=0)
			iquaternion = inverse_quaternion(quaternion)

			pelvis_euler = quaternion2euler(np.copy(state.pelvis.orientation[:]))
			pelvis_euler[0] = -pelvis_euler[0]
			pelvis_euler[2] = -pelvis_euler[2]
			pelvis_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
			#print(pelvis_orientation, state.pelvis.orientation[:])

			translational_velocity = np.copy(state.pelvis.translationalVelocity[:])
			translational_velocity[1] = -translational_velocity[1]

			translational_acceleration = np.copy(state.pelvis.translationalAcceleration[:])
			translational_acceleration[1] = -translational_acceleration[1]

			rotational_velocity = np.copy(state.pelvis.rotationalVelocity)
			rotational_velocity[0] = -rotational_velocity[0]
			rotational_velocity[2] = -rotational_velocity[2]

			motor_position = np.zeros(10)
			motor_position[0:5] = np.copy(state.motor.position[5:10])
			motor_position[5:10] = np.copy(state.motor.position[0:5])
			motor_position[0] = -motor_position[0]
			motor_position[1] = -motor_position[1]
			motor_position[5] = -motor_position[5]
			motor_position[6] = -motor_position[6]

			motor_velocity = np.zeros(10)
			motor_velocity[0:5] = np.copy(state.motor.velocity[5:10])
			motor_velocity[5:10] = np.copy(state.motor.velocity[0:5])
			motor_velocity[0] = -motor_velocity[0]
			motor_velocity[1] = -motor_velocity[1]
			motor_velocity[5] = -motor_velocity[5]
			motor_velocity[6] = -motor_velocity[6]

			joint_position = np.zeros(6)
			joint_position[0:3] = np.copy(state.joint.position[3:6])
			joint_position[3:6] = np.copy(state.joint.position[0:3])

			joint_velocity = np.zeros(6)
			joint_velocity[0:3] = np.copy(state.joint.velocity[3:6])
			joint_velocity[3:6] = np.copy(state.joint.velocity[0:3])

			left_toeForce = np.copy(state.rightFoot.toeForce[:])
			left_toeForce[1] = -left_toeForce[1]
			left_heelForce = np.copy(state.rightFoot.heelForce[:])
			left_heelForce[1] = -left_heelForce[1]
			#print("phase", self.phase)
			#print("left", left_toeForce)

			right_toeForce = np.copy(state.leftFoot.toeForce[:])
			right_toeForce[1] = -right_toeForce[1]
			right_heelForce = np.copy(state.leftFoot.heelForce[:])
			right_heelForce[1] = -right_heelForce[1]
			#print("right", right_toeForce)
			
			new_orientation = quaternion_product(iquaternion, pelvis_orientation)
			new_translationalVelocity = rotate_by_quaternion(translational_velocity, iquaternion)
			new_translationalAcceleration = rotate_by_quaternion(translational_acceleration, iquaternion)
			new_rotationalVelocity = rotate_by_quaternion(rotational_velocity, quaternion)
			#print("vel", new_translationalVelocity)

			useful_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], motor_position, new_translationalVelocity[:], rotational_velocity, motor_velocity, new_translationalAcceleration[:], joint_position, joint_velocity]))
			#print("vel", useful_state[15]-new_translationalVelocity[0])
			return np.concatenate([useful_state, [(self.phase % 14) / 14]])
	def step_simulation(self, action):
		#self.sim.perturb_mass()
		#self.sim.get_mass()
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())


		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		#if self.phase 
		if self.phase < 14:
			target = action + self.ref_pos[self.phase]
			#print(self.phase)
			#print(self.ref_pos[self.phase], self.ref_pos[(self.phase+14)%28])
		else:
			mirror_action = np.zeros(10)
			mirror_action[0:5] = np.copy(action[5:10])
			mirror_action[5:10] = np.copy(action[0:5])
			mirror_action[0] = -mirror_action[0]
			mirror_action[1] = -mirror_action[1]
			mirror_action[5] = -mirror_action[5]
			mirror_action[6] = -mirror_action[6]
			target = mirror_action + self.ref_pos[self.phase]

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]

		self.state_buffer.append(self.sim.step_pd(self.u))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)
			#print(self.state_buffer[0].pelvis.translationalAcceleration[:])
			#print(self.state_buffer[1].pelvis.translationalAcceleration[:])

		self.cassie_state = self.state_buffer[len(self.state_buffer) - 1]

		'''if self.phase == 0 and self.time >= 50:
			self.record_state = True
		if self.record_state:
			self.leg_state.append((np.array(self.sim.qpos())[pos_index], np.array(self.sim.qvel())[vel_index]))
		if len(self.leg_state) == 1680*20 and not self.recorded:
			cdir = os.path.dirname(__file__)
			with open(os.path.join(cdir, 'trajectory', 'phase_run1'), 'wb') as fp:
				pickle.dump(self.leg_state, fp)
			self.recorded = True'''


class cassieRLEnvMirrorPhaseGym(cassieRLEnvMirrorPhase, gym.Env):
	metadata = {"render.modes": ["human"]}

	def __init__(self, render=False):
		super().__init__()
		self.vis = None
		high = np.ones(47) * np.inf
		self.observation_space = gym.spaces.Box(-high, high, dtype=np.float32)
		high = np.ones(10)
		self.action_space = gym.spaces.Box(-high, high, dtype=np.float32)

	def render(self, mode='human'):
		# TODO: add rgb_array mode
		if self.vis is None:
			self.vis = CassieVis(self.sim.c)
		self.vis.draw(self.sim)
		return np.array([])

