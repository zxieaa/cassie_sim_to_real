import pickle
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data
from model import ActorCriticNet, Shared_obs_stats
from quaternion_function import *
from cassieRLEnvMirror import *
from cassieRLEnvIKBackward import cassieRLEnvIKBackward

reorder_motor_matrix = np.zeros((10, 10))
reorder_motor_matrix[0:5, 5:10] = np.identity(5)
reorder_motor_matrix[5:10, 0:5] = np.identity(5)
reorder_motor_matrix[0, 5] = -1
reorder_motor_matrix[1, 6] = -1
reorder_motor_matrix[5, 0] = -1
reorder_motor_matrix[6, 1] = -1

reorder_joint_matrix = np.zeros((6, 6))
reorder_joint_matrix[0:3, 3:6] = np.identity(3)
reorder_joint_matrix[3:6, 0:3] = np.identity(3)

reorder_position_matrix = np.identity(3)
reorder_position_matrix[1, 1] = -1

reorder_orientation_matrix = np.identity(3) * -1
reorder_orientation_matrix[1, 1] = 1

pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])
pos_mirror_index = np.array([2,3,4,5,6,21,22,23,28,29,30,34,7,8,9,14,15,16,20])
vel_mirror_index = np.array([0,1,2,3,4,5,19,20,21,25,26,27,31,6,7,8,12,13,14,18])


def get_mirror_state(state, quaternion):
	#pelvis_euler = reorder_orientation_matrix.dot(quaternion2euler(np.copy(state.pelvis.orientation[:])))
	#pelvis_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
	iquaternion = inverse_quaternion(quaternion)
	new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
	if new_orientation[0] < 0:
		new_orientation = -new_orientation
	pelvis_euler = reorder_orientation_matrix.dot(quaternion2euler(new_orientation))
	new_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
	#print(new_orientation)

	translationalVelocity = reorder_position_matrix.dot(state.pelvis.translationalVelocity[:])
	translationalAcceleration = reorder_position_matrix.dot(state.pelvis.translationalAcceleration[:])
	rotationalVelocity = reorder_orientation_matrix.dot(state.pelvis.rotationalVelocity[:])
	motor_position = reorder_motor_matrix.dot(state.motor.position[:])
	motor_velocity = reorder_motor_matrix.dot(state.motor.velocity[:])
	joint_position = reorder_joint_matrix.dot(state.joint.position[:])
	joint_velocity = reorder_joint_matrix.dot(state.joint.velocity[:])

	new_translationalVelocity = rotate_by_quaternion(translationalVelocity, quaternion)

	print('quaternion: {}, new_orientation: {}'.format(quaternion, new_orientation))

	cassie_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], motor_position[:], new_translationalVelocity[:], rotationalVelocity[:], motor_velocity[:], translationalAcceleration[:], joint_position, joint_velocity]))
	return cassie_state

with open('robot_data/2019-06-27_13_35_log.pkl', 'rb') as f:
	data = pickle.load(f)

with open('robot_data/2019-06-30_sim_log.pkl', 'rb') as f:
	sim_data = pickle.load(f)

model = ActorCriticNet(85, 10, [64, 64])
model.load_state_dict(torch.load("expert_model/IKForwardBackwardSupervisedJune19Size64X64.pt"))
with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
	shared_obs_stats = pickle.load(input)

env = cassieRLEnvIKBackward()

reorder_motor_matrix = np.zeros((10, 10))
reorder_motor_matrix[0:5, 5:10] = np.identity(5)
reorder_motor_matrix[5:10, 0:5] = np.identity(5)
reorder_motor_matrix[0, 5] = -1
reorder_motor_matrix[1, 6] = -1
reorder_motor_matrix[5, 0] = -1
reorder_motor_matrix[6, 1] = -1

states = data['state'][:]
actions = data['target'][:]
inputs = data['input'][:]
outputs = data['output'][:]
command = data['command'][:]
targets = data['target'][:]
sim_inputs = sim_data['input'][:]
#actions = data[1][:]
hip_abduction = np.zeros(10953)
hip_action = np.zeros(10953)
x_speed = np.zeros(len(states))
sim_x_speed = np.zeros(len(states))
orientations = np.zeros(10953)
targets_data = np.zeros(len(states))
height_data = np.zeros(2614)
command_speed = np.zeros(len(states))
index = np.zeros(len(states))
left_forces = np.zeros(len(states))
right_forces = np.zeros(len(states))
j = 0
for i in range(len(states)):
	print(i)
	index[i] = j * 0.03
	j += 1
	#command_speed[i] = command[i] * 0.7
	x_speed[i] = inputs[i][15]
	#sim_x_speed[i] = sim_inputs[i][15]
	left_forces[i] = states[i].leftFoot.heelForce[2]
	right_forces[i] = states[i].rightFoot.heelForce[2]
	targets_data[i] = targets[i][3]
	'''if i % 28 < 14:
		hip_abduction[i] = (states[i][5]) * 180 / np.pi
		hip_action[i] = actions[i][0] * 180 / np.pi
	else:
		hip_abduction[i] = -states[i][10] * 180 / np.pi
		hip_action[i] = -actions[i][5] * 180 / np.pi'''
	#orientation = states[i].pelvis.orientation[:]
	#euler = quaternion2euler(orientation)
	#x_speed[i] = states[i].pelvis.position[2] - states[i].terrain.height#states[i].pelvis.rotationalVelocity[1]
	#orientations[i] = states[i].leftFoot.toeForce[2]
	#print((states[i][0]))

fig = plt.figure()
ax = fig.add_subplot(111)

#ax.plot(hip_abduction, 'b', label="hip_abduction")
#ax.plot(hip_action, 'g', label='hip_action')
#ax.plot(x_speed[1000:10953], 'g', label='x_speed')
'''ax.plot(index[0:3500], command_speed[0:3500], 'r', label="command speed")
ax.plot(index[0:3500], x_speed[0:3500], 'g', label="robot speed")
ax.plot(index[0:3500], sim_x_speed[0:3500], 'b', label="simulation speed")'''
ax.plot(targets_data, 'r')
#ax.plot(index[2000:2200], -left_forces[2000:2200], 'g', label="right forces")
#ax.plot(index[2000:2200], -right_forces[2000:2200], 'b', label="left forces")
#ax.set_xlim([0, 60])
#ax.set_ylim([-0.1, 1])
plt.legend(loc='upper left')
plt.xlabel('time(s)')
plt.ylabel('speed (m/s)')
plt.show()