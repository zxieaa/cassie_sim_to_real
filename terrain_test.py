from cassiemujoco import *
from cassieRLEnv import *
from cassieRLEnvInvariant import *
from cassieRLEnvWithMoreState import *
from cassieRLEnvAccelPenalty import *
from cassieRLEnvSimpleJump import *
from cassieRLEnvMultiSkill import *
from cassieRLEnvHorizon import *
from cassieRLEnvMultiDirection import *
from cassieRLEnvMultiTraj import *
from cassieRLEnvMirror import *
from cassieRLEnvTerrain import *
import time as t

import argparse
import os
import sys
import gym
from gym import wrappers
import random
import numpy as np

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

from params import Params

import pickle
from model import ActorCriticNet, Shared_obs_stats

#env = cassieRLEnStepInPlaceInvariant()
env = cassieRLEnvTerrain()
#env = cassieRLEnvHigherFootSlowInvariant()
#env = cassieRLEnvSpeed()

u = pd_in_t()
u.leftLeg.motorPd.torque[3] = 0 # Feedforward torque
u.leftLeg.motorPd.pTarget[3] = -2
u.leftLeg.motorPd.pGain[3] = 1000
u.leftLeg.motorPd.dTarget[3] = -2
u.leftLeg.motorPd.dGain[3] = 100
u.rightLeg.motorPd = u.leftLeg.motorPd

num_inputs = env.observation_space.shape[0]
num_outputs = env.action_space.shape[0]

model = ActorCriticNet(num_inputs, num_outputs,[1024, 512])
#model.load_state_dict(torch.load("cassie3dStepInPlaceInvariant2kHz256X256June11.pt"))
#model.load_state_dict(torch.load("cassie3dHigherFootSlowerInvariantNoisy2kHz256X256June10.pt"))
#model.load_state_dict(torch.load("cassie3dSpeedDelay2kHz256X256June04.pt"))
env.delay = False
env.noisy = False
#model.load_state_dict(torch.load("torch_model/cassie3dTerrainNov19.pt"))

residual_env = cassieRLEnvMirror()
residual_model = ActorCriticNet(residual_env.observation_space.shape[0], residual_env.action_space.shape[0],[256, 256])
residual_model.load_state_dict(torch.load("torch_model/cassie3dMirror2kHz256X256Oct09.pt"))
with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
	residual_shared_obs_stats = pickle.load(input)

with open('torch_model/cassie3dTerrainResidualNov192kHz_shared_obs_stats.pkl', 'rb') as input:
	shared_obs_stats = pickle.load(input)
#with open('cassie3dFootForcesLess2kHz_shared_obs_stats.pkl', 'rb') as input:
#	shared_obs_stats = pickle.load(input)

state_list = []
env.visualize = True

def run_test():
	t.sleep(1)
	#env.scale = 2
	#env.sim.model.hfield_size[0][2] = 0.4
	state = env.reset()
	#print(env.speed)
	env.speed = 0
	#env.speed = 0.0
	total_reward = 0
	done = False
	total_10_reward = 0
	current_scale = 0
	clock = 0
	done = False
	current_phase = 0

	for i in range(10):
		state = env.reset()
		#env.speed = -1.0
		total_reward = 0
		counter = 0
		interpolate = 1
		while counter < 100000 and not done:
			start = t.time()
			#print(env.speed)
			for j in range(1):
				counter += 1
				clock += 1
				res_state = Variable(torch.Tensor(state[0:85]).unsqueeze(0))
				state = Variable(torch.Tensor(state).unsqueeze(0))
				residual_state = residual_shared_obs_stats.normalize(res_state)
				state = shared_obs_stats.normalize(state)
				#print(state)
				mu, log_std, v = model(state)
				residual_mu, _, _ = residual_model(residual_state)

				env_action = 0.2 * mu.data.squeeze().numpy() + 1*residual_mu.data.squeeze().numpy()
				state, reward, done, _ = env.step(env_action)
				#env.speed = np.sqrt(state[15]**2+state[16]**2+state[17]**2) / 0.8
				env.vis.draw(env.sim)
				#print(reward)
				total_reward += reward
				force = np.zeros(12)
				#print(env.get_foot_forces())
				#t.sleep(1)
				#env.get_foot_height()
			while True:
				stop = t.time()
				#print(stop-start)
				if stop - start > 0.030:
					break
				#print("stop")
			#total_reward += reward
		done = False
		counter = 0
		total_10_reward += total_reward
		print("total rewards", total_reward)
	print(total_10_reward)

def play_kin():
	env.vis.draw(env.sim)
	#env.set_data()
	env.phase = 0
	#env.speed = -0.0
	env.reset()
	env.phase = 0
	t.sleep(1)
	while True:
		print(env.phase)
		start = t.time()
		while True:
			stop = t.time()
			#print(stop-start)
			#print("stop")
			if stop - start > .033:
				break
		pos, vel = env.get_kin_state()
		print(env.speed)
		env.phase += 1
		#print(env.speed)
		if env.phase >= 28:
			env.phase = 0
			env.counter += 1
			#break
		env.set_state(pos, vel)
		y = env.sim.step_pd(u)
		env.vis.draw(env.sim)
		env.get_state()

play_kin()
#run_test()