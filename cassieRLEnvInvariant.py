from cassieRLEnv import *
import math

def euler2quat(z=0, y=0, x=0):

    z = z/2.0
    y = y/2.0
    x = x/2.0
    cz = math.cos(z)
    sz = math.sin(z)
    cy = math.cos(y)
    sy = math.sin(y)
    cx = math.cos(x)
    sx = math.sin(x)
    return np.array([
             cx*cy*cz - sx*sy*sz,
             cx*sy*sz + cy*cz*sx,
             cx*cz*sy - sx*cy*sz,
             cx*cy*sz + sx*cz*sy])


class cassieEnvCrouchToStandInvariant(cassieRLEnvCrouchToStand):
	def __init__(self):
		super().__init__()
		self.orientation = 0
		self.observation_space = np.zeros(78)
		self.buffer_size = 50
		self.noisy = True
		self.delay = True

	def get_state(self):
		if len(self.state_buffer) >= self.buffer_size and self.delay:
			random_index = random.randint(0, self.buffer_size - 1)
			state = self.state_buffer[random_index]
			qpos = np.copy(state[0])
			qvel = np.copy(state[1])
			#print(random_index)
		else:
			qpos = np.copy(self.sim.qpos())
			qvel = np.copy(self.sim.qvel())

		if self.noisy:
			qpos += np.random.normal(size=35)*0.01
			qvel += np.random.normal(size=32)*0.01

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])

		return np.concatenate([qpos[pos_index], qvel[vel_index], ref_pos[pos_index], ref_vel[vel_index]])

	def get_mirror_state(self, state):
		mirror_pos_index = np.array([0, 1, 2, 3, 4, 12, 13, 14, 15, 16, 17, 18, 5, 6, 7, 8, 9, 10, 11])
		mirror_vel_index = np.array([0, 1, 2, 3, 4, 5, 13, 14, 15, 16, 17, 18, 19, 6, 7, 8, 9, 10, 11, 12])
		mirror_state = np.concatenate([state[mirror_pos_index], state[mirror_vel_index + 19], state[mirror_pos_index + 39], state[mirror_pos_index + 58]])
		return mirror_state

	def get_mirror_action(self, action):
		mirror_action_index = np.array([5, 6, 7, 8, 9, 0, 1, 2, 3, 4])
		return np.copy(action[mirror_action_index])

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0


		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		com_penalty = 50 * (self.sim.qpos()[2]-ref_pos[2])**2

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2
		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-orientation_penalty) + 0.1*np.exp(-spring_penalty)

		return total_reward

	def reset(self):
		self.phase = random.randint(0, 27)
		self.orientation = 0#random.randint(-10, 10) * np.pi / 10.0
		#self.orientation = -0.712 * np.pi
		quaternion = euler2quat(z=self.orientation, y=0, x=0)
		#self.orientation = -3.1
		self.time = 0
		self.counter = 0
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		qpos[7] += 0.1 * np.random.normal(size=1)
		qpos[8] += 0.1 * np.random.normal(size=1)
		qpos[21] += 0.1 * np.random.normal(size=1)
		qpos[22] += 0.1 * np.random.normal(size=1)
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)

		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		return self.reset()

	def get_kin_state(self):
		if self.phase < 28 and self.counter == 0:
			interpolate = self.phase / 27.0
		else:
			interpolate = 1
		vel = np.zeros(32)
		pose = self.standing_gait * interpolate + self.crouching_gait * (1 - interpolate)

		pose[0] = 0
		pose[1] = 0
		#pose[6] = self.orientation
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		if phase < 28 and self.counter == 0:
			interpolate = phase / 27.0
		else:
			interpolate = 1
		pose = self.standing_gait * interpolate + self.crouching_gait * (1 - interpolate)
		pose[0] = 0
		pose[1] = 0
		#pose[6] = self.orientation
		vel = np.zeros(32)
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		return pose, vel

class cassieEnvStandToCrouchInvariant(cassieEnvCrouchToStandInvariant):
	def __init__(self):
		super().__init__()
		self.buffer_size = 21
		self.foot_in_air = 0
		self.crouching_gait = np.array([0.022249437292217136, 0.03915589618943308, 0.7584974415213919, 1, 0, 0, 0, 0.06365968825724762, 0.027357072601438052, 0.9611546623679783, 0.8741475448475693, -0.012355694919851688, 0.05225796426302985, -0.4826826201668116, -1.762953772338173, -0.05162436071426111, 2.1291187459935697, -0.05055348649586683, -1.8293552652637257, 1.8108321335701894, -1.9345158883587112, -0.05274099460578134, 0.047203079559391216, 0.9951597010304909, 0.8745384925105277, 0.0023137929942111755, -0.046819332630169345, -0.48268521996375574, -1.7632757779215495, -0.04848941460938318, 2.1043981703118835, -0.03818448407104278, -1.826956462950988, 1.8085041395065278, -1.9337774310330218])
		self.standing_gait = np.array([-0.008835742718181341, 0.04493796413993974, 1.0420432541514535, 1.0, 0.00, 0.0, 0.0, 0.024370651231284606, -0.01629083887281643, 0.3428460780685493, 0.9920774271912584, -0.016730231599306648, 0.009856962361051665, -0.12411816185079373, -1.0112519801792446, -0.02505812646947018, 1.296752283082772, -0.01116537877354158, -1.3811497007932885, 1.3626978375540937, -1.491232377023124, -0.042477126020865834, 0.001890586100925538, 0.39768522724261957, 0.9907064109117, -0.008293631886474785, -0.007535573271668771, -0.13555529566954144, -1.0520627429472444, -0.005812225606574884, 1.3018799821685454, -0.005315626044254926, -1.4186254660102486, 1.4002532543659774, -1.5257096574289477])


	def step(self, action):
		for _ in range(60):
			self.step_simulation(action)

		height = self.sim.qpos()[2]
		self.time += 1
		self.phase += 1

		if self.phase >= 28:
			self.phase = 0
			self.counter +=1
		done = not(height > 0.4 and height < 3.0) or self.time >= self.time_limit

		reward = self.compute_reward()
		if reward < 0.3:
			done = True

		foot_forces = self.get_foot_forces()
		if foot_forces[2] < 150 or foot_forces[8] < 150:
			self.foot_in_air += 1
		
		if self.foot_in_air > 10:
			done = True

		return self.get_state(), reward, done, {}

	def get_kin_state(self):
		if self.phase < 28 and self.counter == 0:
			interpolate = self.phase / 27.0
		else:
			interpolate = 1
		interpolate *= 1
		interpolate = 1 - interpolate
		vel = np.zeros(32)
		pose = self.standing_gait * interpolate + self.crouching_gait * (1 - interpolate)

		pose[0] = 0
		pose[1] = 0
		#pose[6] = self.orientation
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		if phase < 28 and self.counter == 0:
			interpolate = phase / 27.0
		else:
			interpolate = 1
		interpolate *= 1
		interpolate = 1 - interpolate
		pose = self.standing_gait * interpolate + self.crouching_gait * (1 - interpolate)
		pose[0] = 0
		pose[1] = 0
		#pose[6] = self.orientation
		vel = np.zeros(32)
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		return pose, vel

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0

		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			if i == 7 or i == 8 or i == 21 or i == 22:
				joint_penalty += error*100
			else:
				joint_penalty += error*30

		com_penalty = (self.sim.qvel()[0])**2 + (self.sim.qvel()[1])**2 + (self.sim.qpos()[2]-ref_pos[2])**2

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2 + (self.sim.qvel()[5]**2)

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		#force penalty
		foot_forces = self.get_foot_forces()
		force_penalty = 0
		if foot_forces[2] < 150:
			force_penalty += 1000
		if foot_forces[8] < 150:
			force_penalty += 1000
		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.2*np.exp(-orientation_penalty)
		return total_reward

	def reset(self):
		self.phase = random.randint(0, 27)
		self.orientation = random.randint(-10, 10) * np.pi / 10.0
		self.foot_in_air = 0
		#self.orientation = -0.712 * np.pi
		quaternion = euler2quat(z=self.orientation, y=0, x=0)
		#self.orientation = -3.1
		self.time = 0
		self.counter = 0
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		#qpos[7] += 0.1 * np.random.normal(size=1)
		#qpos[8] += 0.1 * np.random.normal(size=1)
		#qpos[21] += 0.1 * np.random.normal(size=1)
		#qpos[22] += 0.1 * np.random.normal(size=1)
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)

		return self.get_state()

class cassieEnvCrouchStandInvariant(cassieEnvCrouchToStandInvariant):
	def __init__(self):
		super().__init__()
		self.buffer_size = 21

	def step(self, action):
		for _ in range(60):
			self.step_simulation(action)

		height = self.sim.qpos()[2]
		self.time += 1
		self.phase += 1

		if self.phase >= 56:
			self.phase = 0
			self.counter +=1
		done = not(height > 0.4 and height < 3.0) or self.time >= self.time_limit

		reward = self.compute_reward()
		if reward < 0.3:
			done = True

		return self.get_state(), reward, done, {}

	def get_kin_state(self):
		if self.phase < 27:
			interpolate = self.phase / 27.0
		else:
			interpolate = 1 - (self.phase-27)/27.0
		vel = np.zeros(32)
		pose = self.crouching_gait * interpolate + self.standing_gait * (1 - interpolate)
		pose[0] = 0
		pose[1] = 0
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		if self.phase < 27:
			vel[2] = -0.1
		else:
			vel[2] = 0.1
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 56:
			phase = 0
		if phase < 27:
			interpolate = phase / 27.0
		else:
			interpolate = 1 - (phase-27)/27.0
		pose = self.crouching_gait * interpolate + self.standing_gait * (1 - interpolate)
		pose[0] = 0
		pose[1] = 0
		vel = np.zeros(32)
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		if phase < 27:
			vel[2] = -0.1
		else:
			vel[2] = 0.1
		return pose, vel

	def reset(self):
		self.phase = random.randint(0, 55)
		self.orientation = random.randint(-10, 10) * np.pi / 10.0
		#self.orientation = 0
		quaternion = euler2quat(z=self.orientation, y=0, x=0)
		#self.orientation = -3.1
		self.time = 0
		self.counter = 0
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		qpos[7] += 0.1 * np.random.normal(size=1)
		qpos[8] += 0.1 * np.random.normal(size=1)
		qpos[21] += 0.1 * np.random.normal(size=1)
		qpos[22] += 0.1 * np.random.normal(size=1)
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		return self.reset()


class cassieRLEnvHigherFootInvariant(cassieEnvCrouchToStandInvariant):
	def __init__(self):
		super().__init__()
		self.orientation = 0
		self.observation_space = np.zeros(78)
		self.buffer_size = 50
		self.noisy = True
		self.delay = True
		self.trajectory = CassieTrajectory("trajectory/stepdata.bin")
		self.higherfootdata = np.array([0.00042345002206552217, 0.024996789026977884, 1.0091868555579184, 0.7651178930754755, -0.005108838142607584, 0.07879546091654344, -0.6390303473290821, -2.192854172592867, 0.004446333191512813, 2.3965238794838375, 0.00135515492741686, -1.5275838449602654, 1.5091577734340855, -1.6353856143984384, 0.10054080868910445, -0.014019219893468186, 1.011560313669182, 0.765106617673535, -0.0046878014529935196, -0.07061146686786338, -0.6400030537868311, -2.1929329862847458, 0.004484356343494892, 2.396601037694866, 0.0013188056358714399, -1.5275235330737484, 1.509097589697184, -1.635325258728461])
		self.higherfootdata[14:28] = np.copy(self.higherfootdata[0:14])
		self.higherfootdata[19] = -self.higherfootdata[19]
		self.foot_forces = np.ones(2) * 500
		self.max_phases = 28

	def step(self, action):
		for _ in range(60):
			self.step_simulation(action)

		height = self.sim.qpos()[2]
		self.time += 1
		self.phase += 1

		if self.phase >= self.max_phases:
			self.phase = 0
			self.counter +=1
		done = not(height > 0.6 and height < 3.0) or self.time >= self.time_limit

		reward = self.compute_reward()
		if reward < 0.3:
			done = True

		return self.get_state(), reward, done, {}

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		#original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])
		original_right_foot = np.copy(original_left_foot)
		original_right_foot[5] = -original_right_foot[5]

		if self.phase < 7:
			interpolate = self.phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif self.phase < 14:
			interpolate = (self.phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif self.phase < 21:
			interpolate = (self.phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (self.phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot

		#quaternion = euler2quat(z=self.orientation, y=0, x=0)
		#pose[3:7] = quaternion
			
		vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		vel[0] = 0
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		vel[0] = 0

		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])

		if phase < 7:
			interpolate = phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif phase < 14:
			interpolate = (phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif phase < 21:
			interpolate = (phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		return pose, vel

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0

		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			if i == 7 or i == 8 or i == 21 or i == 22:
				joint_penalty += error*100
			else:
				joint_penalty += error*30

		com_penalty = (self.sim.qvel()[0])**2 + (self.sim.qvel()[1])**2 + (self.sim.qpos()[2]-ref_pos[2])**2

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2 + (self.sim.qvel()[5] * 100)**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		#force penalty
		foot_forces = self.get_foot_forces()
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2])**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8])**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]



		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-orientation_penalty)+0.1*np.exp(-spring_penalty)

		return total_reward

class cassieRLEnvHigherFootSlowerInvariant(cassieRLEnvHigherFootInvariant):
	def __init__(self):
		super().__init__()
		self.max_phases = 40
		self.buffer_size = 20

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[0])
		pose[1] = 0
		pose[0] = 0
		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		#original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])
		original_right_foot = np.copy(original_left_foot)
		original_right_foot[5] = -original_right_foot[5]

		if self.phase < 10:
			interpolate = self.phase / 10.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif self.phase < 20:
			interpolate = (self.phase - 9.0) / 10.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif self.phase < 30:
			interpolate = (self.phase - 19) / 10.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (self.phase - 29) / 10.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot

		#quaternion = euler2quat(z=self.orientation, y=0, x=0)
		#pose[3:7] = quaternion
			
		vel = np.copy(self.trajectory.qvel[0])
		vel[0] = 0
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 40:
			phase = 0
		pose = np.copy(self.trajectory.qpos[0])
		pose[1] = 0
		pose[0] = 0
		vel = np.copy(self.trajectory.qvel[0])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		vel[0] = 0

		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])

		if phase < 10:
			interpolate = phase / 10.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif phase < 20:
			interpolate = (phase - 9.0) / 10.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif phase < 30:
			interpolate = (phase - 19) / 10.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (phase - 29) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		return pose, vel

class cassieRLEnvHigherFootSlowInvariant(cassieRLEnvHigherFootSlowerInvariant):
	def __init__(self):
		super().__init__()
		self.max_phases = 40
		self.buffer_size = 20
		with open ("step_in_place_higher_foot_slower_trajectory", 'rb') as fp:
			self.trajectory = pickle.load(fp)

	def get_kin_state(self):
		pose = np.copy(self.trajectory[self.phase][0])
		vel = np.copy(self.trajectory[self.phase][1])
		pose[0] = pose[1] = 0
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 40:
			phase = 0
		pose = np.copy(self.trajectory[self.phase][0])
		vel = np.copy(self.trajectory[self.phase][1])
		pose[0] = pose[1] = 0
		return pose, vel

class cassieRLEnStepInPlaceInvariant(cassieRLEnvHigherFootSlowInvariant):
	def __init__(self):
		super().__init__()
		self.max_phases = 28
		self.buffer_size = 20
		self.trajectory = CassieTrajectory("trajectory/stepdata.bin")

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		vel[0] = 0
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		vel[0] = 0
		return pose, vel





