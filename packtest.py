from cassiemujoco import *
import ctypes

sendbuf = (ctypes.c_ubyte * 476)()
pdin = pd_in_t()
pdin.leftLeg.motorPd.pGain[0] = 100
pdin.leftLeg.motorPd.pTarget[0] = 1
pack_pd_in_t(pdin, sendbuf)
