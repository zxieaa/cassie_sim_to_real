import argparse
import os
import sys
import gym
from gym import wrappers
import random
import numpy as np
import scipy

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

from params import Params

import pickle
import time as t

from model import ActorCriticNet, Shared_obs_stats

import statistics
import matplotlib.pyplot as plt
from operator import add, sub


import pickle
import threading
import torch.multiprocessing as mp
import queue
from random import randint

from cassieRLEnv import *
from cassiemujoco import *
from cassieRLEnvInvariant import *
from cassieRLEnvWithMoreState import *
from cassieRLEnvAccelPenalty import *
from cassieRLEnvSimpleJump import *
from cassieRLEnvMultiSkill import *
from cassieRLEnvHorizon import *
from cassieRLEnvMultiDirection import *
from cassieRLEnvMultiTraj import *
from cassieRLEnvMirror import *
from cassieRLEnvTerrain import *
from cassieRLEnvStablePelvis import *
from cassieRLEnvNoRef import *
from cassieRLEnvSlopeTerrain import *
from cassieRLEnvSideStepping import *
from cassieRLEnvStanding import *
from cassieRLEnvSym import cassieRLEnvSym
from symmetric_net import *

from utils import TrafficLight
from utils import Counter



class ReplayMemory(object):
	def __init__(self, capacity):
		self.capacity = capacity
		self.memory = []

	def push(self, events):
		for event in zip(*events):
			self.memory.append(event)
			if len(self.memory)>self.capacity:
				del self.memory[0]

	def clear(self):
		self.memory = []

	def sample(self, batch_size):
		samples = zip(*random.sample(self.memory, batch_size))
		#print(map(lambda x: np.concatenate(x, 0), samples))
		return map(lambda x: np.concatenate(x, 0), samples)

	def clean_memory(self):
		while len(self.memory) > self.capacity:
			del self.memory[0]

def normal(x, mu, log_std):
	a = (x - mu)/(log_std.exp())
	a = -0.5 * a.pow(2)
	a = torch.sum(a, dim=1)
	b = torch.sum(log_std, dim=1)
	#print(result)
	return a-b

class RL(object):
	def __init__(self, env, hidden_layer=[64, 64]):
		self.env = env
		#self.env.env.disableViewer = False
		self.num_inputs = env.observation_space.shape[0]
		self.num_outputs = env.action_space.shape[0]
		self.hidden_layer = hidden_layer

		self.params = Params()

		# self.model = ActorCriticNet(self.num_inputs, self.num_outputs,self.hidden_layer)
		# self.model = SymmetricNet(**env.mirror_counts, num_layers=2, hidden_size=64)
		self.model = self.create_net()
		self.model.share_memory()

		self.shared_obs_stats = SymmetricStats(env.mirror_counts['c_in'], env.mirror_counts['n_in'], env.mirror_counts['s_in'])
		self.best_model = self.create_net()
		self.memory = ReplayMemory(self.params.num_steps * 10000)
		self.test_mean = []
		self.test_std = []
		self.fig = plt.figure()
		#self.fig2 = plt.figure()
		self.lr = self.params.lr
		plt.show(block=False)

		self.test_list = []
		self.queue = mp.Queue()

		self.mpdone = [mp.Event(), mp.Event(), mp.Event(), mp.Event()]

		self.process = []
		self.traffic_light = TrafficLight()
		self.counter = Counter()

	def create_net(self):
		#return ActorCriticNet(self.num_inputs, self.num_outputs,self.hidden_layer)
		return SymmetricPolicyValue(**self.env.mirror_counts, num_layers=2, hidden_size=[30, 30, 98], log_std=-2, varying_std=False)

	def normalize_data(self, num_iter=50000, file='shared_obs_stats.pkl'):
		state = self.env.reset()#_for_normalization()
		state = Variable(torch.Tensor(state).unsqueeze(0))
		model_old = self.create_net()
		model_old.load_state_dict(self.model.state_dict())
		for i in range(num_iter):
			self.shared_obs_stats.observes(state)
			state = self.shared_obs_stats.normalize(state)
			mu, log_std, v = model_old(state)
			eps = torch.randn(mu.size())
			action = (mu + log_std.exp()*Variable(eps))
			env_action = action.data.squeeze().numpy()
			state, reward, done, _ = self.env.step(env_action)

			if done:
				state = self.env.reset()

			state = Variable(torch.Tensor(state).unsqueeze(0))

		with open(file, 'wb') as output:
			pickle.dump(self.shared_obs_stats, output, pickle.HIGHEST_PROTOCOL)

	def run_test(self, num_test=1):
		state = self.env.reset()#_for_test()
		state = Variable(torch.Tensor(state).unsqueeze(0))
		model_old = self.create_net()
		model_old.load_state_dict(self.model.state_dict())
		ave_test_reward = 0

		total_rewards = []
		'''self.fig2.clear()
		circle1 = plt.Circle((0, 0), 0.5, edgecolor='r', facecolor='none')
		circle2 = plt.Circle((0, 0), 0.01, edgecolor='r', facecolor='none')
		plt.axis('equal')'''
		
		for i in range(num_test):
			total_reward = 0
			while True:
				state = self.shared_obs_stats.normalize(state)
				mu, log_std, v = self.model(state)
				eps = torch.randn(mu.size())
				action = (mu+0.1*eps).data.squeeze().numpy()
				state, reward, done, _ = self.env.step(action)
				total_reward += reward
				#print(state)
				#print("done", done, "state", state)

				if done:
					state = self.env.reset()#_for_test()
					#print(self.env.position)
					#print(self.env.time)
					state = Variable(torch.Tensor(state).unsqueeze(0))
					ave_test_reward += total_reward / num_test
					total_rewards.append(total_reward)
					break
				state = Variable(torch.Tensor(state).unsqueeze(0))
		#print("avg test reward is", ave_test_reward)

		reward_mean = statistics.mean(total_rewards)
		reward_std = statistics.stdev(total_rewards)
		self.test_mean.append(reward_mean)
		self.test_std.append(reward_std)
		self.test_list.append((reward_mean, reward_std))
		#print(self.model.state_dict())

	def plot_statistics(self):
		
		ax = self.fig.add_subplot(111)
		low = []
		high = []
		index = []
		for i in range(len(self.test_mean)):
			low.append(self.test_mean[i] - self.test_std[i])
			high.append(self.test_mean[i] + self.test_std[i])
			index.append(i)
		#ax.set_xlim([0,1000])
		#ax.set_ylim([0,300])
		plt.xlabel('iterations')
		plt.ylabel('average rewards')
		ax.plot(self.test_mean, 'b')
		ax.fill_between(index, low, high, color='cyan')
		#ax.plot(map(sub, test_mean, test_std))
		self.fig.canvas.draw()

	def save_statistics(self, filename):
		statistics = [self.test_mean, self.test_std]
		with open(filename, 'wb') as output:
			pickle.dump(statistics, output, pickle.HIGHEST_PROTOCOL)


	def collect_samples(self, num_samples, start_state=None, noise=-2.0, env_index=0, random_seed=1):

		random.seed(random_seed)
		torch.manual_seed(random_seed+1)
		np.random.seed(random_seed+2)

		if start_state == None:
			start_state = self.env.reset()
		samples = 0
		done = False
		states = []
		next_states = []
		actions = []
		rewards = []
		values = []
		q_values = []
		self.model.set_noise(noise)
		model_old = self.create_net()
		model_old.load_state_dict(self.model.state_dict())
		model_old.set_noise(noise)


		state = start_state
		state = Variable(torch.Tensor(state).unsqueeze(0))
		total_reward = 0
		#q_value = Variable(torch.zeros(1, 1))
		while True:
			self.model.set_noise(-2.0)
			model_old.set_noise(-2.0)
			signal_init = self.traffic_light.get()
			while samples < num_samples and not done:
				state = self.shared_obs_stats.normalize(state)
				#print(state)
				states.append(state.data.numpy())
				mu, log_std, v = model_old(state)
				#print(mu.shape, log_std.shape, v.shape)

				eps = torch.randn(mu.size())
				#print(log_std.exp())
				action = (mu + log_std.exp()*Variable(eps))
				actions.append(action.data.numpy())
				values.append(v.data.numpy())
				env_action = action.data.squeeze().numpy()
				state, reward, done, _ = self.env.step(env_action)
				total_reward += reward
				rewards.append(Variable(reward * torch.ones(1)).data.numpy())
				#q_value = self.gamma * q_value + Variable(reward * torch.ones(1))
				state = Variable(torch.Tensor(state).unsqueeze(0))

				next_state = self.shared_obs_stats.normalize(state)
				next_states.append(next_state.data.numpy())

				samples += 1

			state = self.shared_obs_stats.normalize(state)
			#print(state)
			_,_,v = model_old(state)
			if done:
				R = torch.zeros(1, 1)
			else:
				R = v.data
			R = Variable(R)
			for i in reversed(range(len(rewards))):
				R = self.params.gamma * R + Variable(torch.from_numpy(rewards[i]))
				q_values.insert(0, R.data.numpy())

			#self.memory.push([states, actions, next_states, rewards, q_values])
			#return [states, actions, next_states, rewards, q_values]
			self.queue.put([states, actions, next_states, rewards, q_values])
			self.counter.increment()
			self.env.reset()
			while self.traffic_light.get() == signal_init:
				pass
			start_state = self.env.reset()
			state = start_state
			state = Variable(torch.Tensor(state).unsqueeze(0))
			total_reward = 0
			samples = 0
			done = False
			states = []
			next_states = []
			actions = []
			rewards = []
			values = []
			q_values = []
			model_old = self.create_net()
			model_old.load_state_dict(self.model.state_dict())
			model_old.set_noise(-2.0)
		#print(self.queue.empty())
		#print("total reward", total_reward)

	def update_critic(self, batch_size, num_epoch):
		self.model.train()
		optimizer = optim.Adam(self.model.parameters(), lr=10*self.lr)
		model_old = self.create_net()
		model_old.load_state_dict(self.model.state_dict())
		for k in range(num_epoch):
			batch_states, batch_actions, batch_next_states, batch_rewards, batch_q_values = self.memory.sample(batch_size)
			batch_states = Variable(torch.Tensor(batch_states))
			batch_q_values = Variable(torch.Tensor(batch_q_values))
			# batch_next_states = Variable(torch.Tensor(batch_next_states))
			# _, _, v_pred_next = model_old(batch_next_states)
			_, _, v_pred = self.model(batch_states)
			loss_value = (v_pred - batch_q_values)**2
			#loss_value = (v_pred_next * self.params.gamma + batch_rewards - v_pred)**2
			loss_value = 0.5*torch.mean(loss_value)
			optimizer.zero_grad()
			loss_value.backward()
			optimizer.step()
			print(loss_value)
			#av_value_loss = loss_value.data[0]
			#model_old.load_state_dict(model.state_dict())
		#print("value loss ", av_value_loss)

	def update_actor(self, batch_size, num_epoch):
		model_old = self.create_net()
		model_old.load_state_dict(self.model.state_dict())
		model_old.set_noise(self.model.noise)
		self.model.train()
		optimizer = optim.Adam(self.model.parameters(), lr=self.lr)
		for k in range(num_epoch):
			batch_states, batch_actions, batch_next_states, batch_rewards, batch_q_values = self.memory.sample(batch_size)

			batch_states = Variable(torch.Tensor(batch_states))
			batch_q_values = Variable(torch.Tensor(batch_q_values))
			batch_actions = Variable(torch.Tensor(batch_actions))
			mu_old, log_std_old, v_pred_old = model_old(batch_states)
			#mu_old_next, log_std_old_next, v_pred_old_next = model_old(batch_next_states)
			mu, log_std, v_pred = self.model(batch_states)
			batch_advantages = batch_q_values - v_pred_old
			probs_old = normal(batch_actions, mu_old, log_std_old)
			probs = normal(batch_actions, mu, log_std)
			ratio = (probs - (probs_old)).exp()
			ratio = ratio.unsqueeze(1)
			#print(model_old.noise)
			#print(ratio)
			batch_advantages = batch_q_values - v_pred_old
			surr1 = ratio * batch_advantages
			surr2 = ratio.clamp(1-self.params.clip, 1+self.params.clip) * batch_advantages
			loss_clip = -torch.mean(torch.min(surr1, surr2))


			total_loss = loss_clip# + 1*mirror_loss
			#print(loss_clip)
			optimizer.zero_grad()
			total_loss.backward(retain_graph=True)
			optimizer.step()
		if self.lr > 1e-4:
			self.lr *= 0.99

	def clear_memory(self):
		self.memory.clear()

	def save_model(self, filename):
		torch.save(self.model.state_dict(), filename)

	def save_shared_obs_stas(self, filename):
		with open(filename, 'wb') as output:
			pickle.dump(self.shared_obs_stats, output, pickle.HIGHEST_PROTOCOL)

	def collect_samples_multithread(self):
		#queue = Queue.Queue()
		self.lr = 3e-4
		seed1 = np.random.randint(0, 4294967296)
		seed2 = np.random.randint(0, 4294967296)
		seed3 = np.random.randint(0, 4294967296)
		seed4 = np.random.randint(0, 4294967296)
		seed5 = np.random.randint(0, 4294967296)
		seed6 = np.random.randint(0, 4294967296)
		seed7 = np.random.randint(0, 4294967296)
		seed8 = np.random.randint(0, 4294967296)
		seed9 = np.random.randint(0, 4294967296)
		seed10 = np.random.randint(0, 4294967296)

		t1 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':0, 'random_seed':seed1})
		t2 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':1, 'random_seed':seed2})
		t3 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':2, 'random_seed':seed3})
		t4 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':3, 'random_seed':seed4})
		t5 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':4, 'random_seed':seed5})
		t6 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':5, 'random_seed':seed6})
		t7 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':6, 'random_seed':seed7})
		t8 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':7, 'random_seed':seed8})
		t9 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':8, 'random_seed':seed9})
		t10 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':9, 'random_seed':seed10})
			
		t1.start()
		t2.start()
		t3.start()
		t4.start()
		t5.start()
		t6.start()
		t7.start()
		t8.start()
		t9.start()
		t10.start()
		self.model.set_noise(-2.0)
		while True:
			if len(self.test_mean) % 100 == 1:
			    self.save_statistics("stats/SymStatsJuly17%d_v3.stat"%(len(self.test_mean)))
			self.save_model("torch_model/cassie_sym_July17_v3.pt")
			#if len(self.test_mean) % 100 == 1 and self.test_mean[len(self.test_mean)-1] > 300:
			 #   self.save_model("torch_model/multiskill/v4_cassie3dMirrorIter%d.pt"%(len(self.test_mean),))
			while len(self.memory.memory) < 9000:
				#print(len(self.memory.memory))
				if self.counter.get() == 10:
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
					self.counter.increment()
				if len(self.memory.memory) < 9000 and self.counter.get() == 11:
					self.counter.reset()
					self.traffic_light.switch()
					#print(len(self.memory.memory))
			self.update_critic(128, 256)
			self.update_actor(128, 256)
			self.clear_memory()
			self.run_test(num_test=2)
			#print("run test")
			self.plot_statistics()
			self.traffic_light.switch()
			self.counter.reset()
			#print("done")
		'''t1.join()
		t2.join()
		t3.join()
		t4.join()
		t1.terminate()
		t2.terminate()
		t3.terminate()
		t4.terminate()'''
		
		'''L = mp.manager.list()
		with mp.Pool(processes=4) as pool:
			result1 = pool.apply_async(self.collect_samples, args=(300),kwds=('noise',-2.0, 'env_index',0))
			result2 = pool.apply_async(self.collect_samples, args=(300),kwds=('noise',-2.0, 'env_index',1))
			result3 = pool.apply_async(self.collect_samples, args=(300),kwds=('noise',-2.0, 'env_index',2))
			result4 = pool.apply_async(self.collect_samples, args=(300),kwds=('noise',-2.0, 'env_index',3))
			#print(result1.get())
			#self.memory.push(memory1)
			#self.memory.push(result2.get())
			#self.memory.push(result3.get())
			#self.memory.push(result4.get())
			pool.close()
			pool.join()'''
		#manager.terminate()

	def add_env(self, env):
		self.env_list.append(env)

def mkdir(base, name):
	path = os.path.join(base, name)
	if not os.path.exists(path):
		os.makedirs(path)
	return path

if __name__ == '__main__':
	#torch.manual_seed(1)
	#env = gym.make(sys.argv[1])
	#env.env.disableViewer = False
	#env = wrappers.Monitor(env, monitor_dir, force=True)
	#print(sys.argv[1])
	#env_name = sys.argv[1]
	#torch.manual_seed(1)
	#np.random.seed(1)
	#random.seed(1)
	#env = cassieRLEnStepInPlaceInvariant()
	torch.set_num_threads(1)
	env = cassieRLEnvSym()
	#import mocca_env
	#env_name = "mocca_envs:CassieMocapPhaseRSI2DEnv-v0"
	#env = gym.make(env_name)
	ppo = RL(env, [64, 64])
	#print(env.observation_space.shape)
	#ppo.add_env(env2)
	#ppo.add_env(env3)
	#ppo.add_env(env4)
	#ppo.normalize_data(num_iter=10000)
	#ppo.save_shared_obs_stas('torch_model/cassie3dRLEnvSym_shared_obs_stats.pkl')
	#print("done")
	with open('torch_model/cassie3dRLEnvSym_shared_obs_stats.pkl', 'rb') as input:
		ppo.shared_obs_stats = pickle.load(input)
	#ppo.model.load_state_dict(torch.load((("torch_model/cassieStandingJuly11.pt"))))
	ppo.collect_samples_multithread()

	start = t.time()

	noise = -2.0