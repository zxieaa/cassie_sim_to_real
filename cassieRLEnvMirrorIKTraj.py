from cassieRLEnvMirrorWithTransition import *
import pickle

class cassieRLEnvMirrorIKTraj(cassieRLEnvMirrorWithTransition):
	def __init__(self):
		super().__init__()
		self.step_in_place_trajectory = CassieTrajectory("trajectory/more-poses-trial.bin")
		for i in range(841):
			self.step_in_place_trajectory.qpos[i][7:21] = np.copy(self.step_in_place_trajectory.qpos[(i+841)][21:35])
			self.step_in_place_trajectory.qpos[i][12] = -self.step_in_place_trajectory.qpos[i][12]
			self.step_in_place_trajectory.qpos[i][21:35] = np.copy(self.step_in_place_trajectory.qpos[(i+841)][7:21])
			self.step_in_place_trajectory.qpos[i][26] = -self.step_in_place_trajectory.qpos[i][26]
		for i in range(1682):
			self.step_in_place_trajectory.qpos[i][2] = 1.05

	def get_kin_state(self):
		'''if self.speed < 0:
			interpolate = self.speed * -1
			phase = self.phase
			pose = np.copy(self.backward_trajectory[phase*self.control_rate][0]) * interpolate + (1 - interpolate) * np.copy(self.step_in_place_trajectory[phase*self.control_rate][0])
			pose[0] += (self.backward_trajectory[(self.max_phase-1)*self.control_rate][0][0]- self.backward_trajectory[0][0][0])* self.counter * (-self.speed)
			pose[1] = 0
			vel = np.copy(self.backward_trajectory[phase*self.control_rate][1])
			vel[0] *= (-self.speed)
		elif self.speed <= 1.0:
			interpolate = self.speed * 1
			pose = np.copy(self.trajectory.qpos[self.phase*self.control_rate]) * interpolate + (1 - interpolate) * np.copy(self.step_in_place_trajectory.qpos[self.phase*self.control_rate])
			pose[0] = self.trajectory.qpos[self.phase*self.control_rate, 0]
			pose[0] *= self.speed
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel = np.copy(self.trajectory.qvel[self.phase*self.control_rate])
			vel[0] *= self.speed
		else:
			pose = np.copy(self.trajectory.qpos[self.phase*self.control_rate])
			pose[0] = self.trajectory.qpos[self.phase*self.control_rate, 0]
			pose[0] *= self.speed
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel = np.copy(self.trajectory.qvel[self.phase*self.control_rate])
			vel[0] *= self.speed'''
		if self.speed < 0:
			phase = self.phase
			if phase >= self.max_phase:
				phase = 0
			phase = self.max_phase - phase
			pose = np.copy(self.step_in_place_trajectory.qpos[phase*self.control_rate])
			pose[0] += self.speed * 1.0 * (self.max_phase-phase) / 28
			vel = np.copy(self.step_in_place_trajectory.qvel[phase*self.control_rate])
			pose[0] += (1.0 - self.step_in_place_trajectory.qpos[0, 0])* self.counter * self.speed
			#print(pose[0])
			pose[1] = 0
			vel[0] = 0.8 * self.speed
		else:
			phase = self.phase
			if phase >= self.max_phase:
				phase = 0
			pose = np.copy(self.step_in_place_trajectory.qpos[phase*self.control_rate])
			pose[0] += self.speed * 1.0 * phase / 28
			vel = np.copy(self.step_in_place_trajectory.qvel[phase*self.control_rate])
			pose[0] += (1.0 - self.step_in_place_trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel[0] = 0.8 * self.speed
		#print(pose[0])
		pose[3] = 1
		pose[4:7] = 0
		pose[7] = 0
		pose[8] = 0
		pose[21] = 0
		pose[22] = 0
		vel[6] = 0
		vel[7] = 0
		vel[19] = 0
		vel[20] = 0
		return pose, vel
	def get_kin_next_state(self):
		if self.speed < 0:
			phase = self.phase + 1
			if phase >= self.max_phase:
				phase = 0
			phase = self.max_phase - phase
			pose = np.copy(self.step_in_place_trajectory.qpos[phase*self.control_rate])
			pose[0] += self.speed * 1.0 * (self.max_phase-phase) / 28
			vel = np.copy(self.step_in_place_trajectory.qvel[phase*self.control_rate])
			pose[0] += (1.0- self.step_in_place_trajectory.qpos[0, 0])* self.counter * self.speed
			#print(pose[0])
			pose[1] = 0
			vel[0] = 0.8 * self.speed
		else:
			phase = self.phase + 1
			if phase >= self.max_phase:
				phase = 0
			pose = np.copy(self.step_in_place_trajectory.qpos[phase*self.control_rate])
			pose[0] += self.speed * 1.0 * phase / 28
			vel = np.copy(self.step_in_place_trajectory.qvel[phase*self.control_rate])
			pose[0] += (1.0 - self.step_in_place_trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel[0] = 0.8 * self.speed
		#print(pose[0])
		pose[3] = 1
		pose[4:7] = 0
		pose[7] = 0
		pose[8] = 0
		pose[21] = 0
		pose[22] = 0
		vel[6] = 0
		vel[7] = 0
		vel[19] = 0
		vel[20] = 0
		return pose, vel
	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0
		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = np.copy(self.cassie_state.pelvis.position[:])
		pelvis_vel = np.copy(self.cassie_state.pelvis.translationalVelocity[:])
		com_penalty = (pelvis_vel[0]-ref_vel[0])**2 + (pelvis_vel[1]-ref_vel[1])**2 + (self.sim.qpos()[2]-1.05)**2

		yaw = quat2yaw(self.sim.qpos()[3:7])

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2
		#print(self.sim.qpos()[2])

		foot_forces = self.get_foot_forces()
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-150)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-150)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]

		pos = np.zeros(6)
		pos = self.sim.foot_pos(pos)
		foot_height = max(pos[2], pos[5])
		foot_penalty = 0
		if (self.phase <= 5  and self.phase >= 2) or (self.phase <= 19 and self.phase >= 16):
			desire_height = (2 - abs((self.phase % 14) - 3.5)) * 0.05 + 0.075
			#print(desire_height)
			foot_penalty = (min(foot_height - desire_height, 0))**2 * 400

		total_reward = 0.4*np.exp(-joint_penalty)+0.3*np.exp(-10*com_penalty)+0.2*np.exp(-10*orientation_penalty)+0.1*np.exp(-foot_penalty)
		#total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.2*np.exp(-10*orientation_penalty)
		#print(0.4*np.exp(-joint_penalty), 0.3*np.exp(-10*com_penalty), 0.2*np.exp(-10*orientation_penalty), 0.1*np.exp(-foot_penalty))

		return total_reward