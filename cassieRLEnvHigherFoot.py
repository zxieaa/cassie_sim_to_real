from cassiemujoco_ctypes import *
import math
from quaternion_function import *
from cassieRLEnvMirror import *
from cassieRLEnvMultiTraj import *
from model import *
import pickle

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

class cassieRLEnvHigherFoot(cassieRLEnvMirror):
	def __init__(self):
		super().__init__()
		self.buffer_size = 10

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		yaw = quaternion2euler(self.sim.qpos()[3:7])
		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(self.sim.qpos()[6])**2
		orientation_vel_penalty = (self.sim.qvel()[3])**2+(self.sim.qvel()[4])**2+(self.sim.qvel()[5])**2
		orientation_acc_penalty = (0.1*self.sim.qacc()[3])**2+(0.1*self.sim.qacc()[4])**2+(0.1*self.sim.qacc()[5])**2
		vel_penalty = (self.sim.qpos()[1]-ref_pos[1])**2+(self.sim.qvel()[2])**2+(self.sim.qpos()[0]-ref_pos[0])**2
		#acc_penalty = (0.01 * self.sim.qacc()[1])**2+(0.01* self.sim.qacc()[2])**2+(self.sim.qpos()[0]-ref_pos[0])**2
		pos = np.zeros(6)
		pos = self.sim.foot_pos(pos)
		foot_height = max(pos[2], pos[5])
		foot_penalty = orientation_vel_penalty
		if (self.phase <= 5  and self.phase >= 2) or (self.phase <= 19 and self.phase >= 16):
			desire_height = (2 - abs((self.phase % 14) - 3.5)) * 0.05 + 0.125
			#print(desire_height)
			foot_penalty = (min(foot_height - desire_height, 0))**2 * 400
			print("foot_height", foot_height, desire_height, foot_penalty)
		#self.sim.qpos()[3:7]
		#print("pos vel", self.sim.qvel()[0:3])
		#print("motor acc", self.sim.qacc()[6:9])
		#print(self.buffer_size)
		#print("acc", self.sim.qacc()[3:6])
		#print("angular vel", self.sim.qvel()[3:6])
		#print("reward", 0.5*np.exp(-1*orientation_vel_penalty), 0.5*np.exp(-2*vel_penalty))
		#return np.exp(-np.sum(action**2))
		#print(self.sim.qvel()[2])
		#print( 0.5*np.exp(-foot_penalty), 0.5*np.exp(-1*vel_penalty))
		return 0.5*np.exp(-foot_penalty)+0.5*np.exp(-1*vel_penalty)