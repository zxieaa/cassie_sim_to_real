from cassieRLEnv import *
from cassiemujoco_ctypes import *
import math
import os
from quaternion_function import *

def euler2quat(z=0, y=0, x=0):

    z = z/2.0
    y = y/2.0
    x = x/2.0
    cz = math.cos(z)
    sz = math.sin(z)
    cy = math.cos(y)
    sy = math.sin(y)
    cx = math.cos(x)
    sx = math.sin(x)
    result =  np.array([
             cx*cy*cz - sx*sy*sz,
             cx*sy*sz + cy*cz*sx,
             cx*cz*sy - sx*cy*sz,
             cx*cy*sz + sx*cz*sy])
    if result[0] < 0:
    	result = -result
    return result

def quat2yaw(quaternion):
	return math.atan2(2.0 * (quaternion[0] * quaternion[3] + quaternion[1] * quaternion[2]), 1.0 - 2.0 * (quaternion[2]**2 + quaternion[3]**2))

class cassieRLEnvStepInPlaceWithFootForces(cassieRLEnvDelay):
	def __init__(self):
		super().__init__()
		self.observation_space = np.zeros(87)
		self.action_space = np.zeros(10)
		self.trajectory = CassieTrajectory(os.path.join(os.path.dirname(__file__), "trajectory", "stepdata.bin"))
		self.delay = True
		self.buffer_size = 20
		self.noisy = True
		self.cassie_state = state_out_t()
		self.orientation = 0
		self.foot_forces = np.ones(2) * 500
		self.max_phase = 28
		self.control_rate = 60
		self.time_limit = 400 * 60 / self.control_rate

		self.record_state = False
		self.recorded = False
		self.recorded_state = []
		self.printed = True

	def step_simulation(self, action):
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())

		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		target = action + ref_pos[pos_index]

		#if self.phase == 6 and self.printed:
		#	print("target", target)
		#	print("action", action)
		#	self.printed = False
		#control = self.P*(target-self.sim.data.qpos[index])-self.D*self.sim.data.qvel[vel_index]

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]

		self.state_buffer.append(self.sim.step_pd(self.u))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)
			#print(self.state_buffer[0].pelvis.translationalAcceleration[:])
			#print(self.state_buffer[1].pelvis.translationalAcceleration[:])

		self.cassie_state = self.state_buffer[len(self.state_buffer) - 1]

		'''if self.phase == 0 and self.time >= 100:
			self.record_state = True
		if self.record_state and len(self.recorded_state) < 1682:
			self.recorded_state.append((self.sim.qpos(), self.sim.qvel()))
		if len(self.recorded_state) == 1682 and not self.recorded:
			with open('trajectory/side_stepping_trajectory_Nov', 'wb') as fp:
				pickle.dump(self.recorded_state, fp)
			self.recorded = True'''
		#print("torque",self.cassie_state.motor.torque[:])
		#for i in range(len(self.state_buffer)):
		#	print("see state", i, self.state_buffer[i].pelvis.translationalVelocity[:])

	def step(self, action):
		for _ in range(self.control_rate):
			self.step_simulation(action)

		height = self.sim.qpos()[2]
		#print("height", height)
		self.time += 1
		self.phase += 1

		if self.phase >= self.max_phase:
			self.phase = 0
			self.counter +=1
		#print("height", height)

		done = not(height > 0.4 and height < 100.0) or self.time >= self.time_limit
		yaw = quat2yaw(self.sim.qpos()[3:7])
		#print(yaw - self.orientation)
		#if (yaw-self.orientation)**2 > 0.15:
		#	done = True

		reward = self.compute_reward()
		#print(reward)
		if reward < 0.3:
			done = True

		return self.get_state(), reward, done, {}

	def get_state(self):
		#if len(self.state_buffer) >= self.buffer_size and self.delay:
		#	random_index = random.randint(0, self.buffer_size - 1)
		#	state = self.state_buffer[random_index]
			#print(random_index)
		#else:
		state = self.cassie_state

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([1,2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])


		#state.pelvis.orientation[3] -= np.sin(self.orientation/2)
		quaternion = euler2quat(z=self.orientation, y=0, x=0)
		iquaternion = inverse_quaternion(quaternion)
		new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
		#print(new_orientation)
		new_translationalVelocity = rotate_by_quaternion(state.pelvis.translationalVelocity[:], iquaternion)
		#print(new_translationalVelocity)
		new_translationalAcceleration = rotate_by_quaternion(state.pelvis.translationalAcceleration[:], iquaternion)
		new_rotationalVelocity = rotate_by_quaternion(state.pelvis.rotationalVelocity[:], quaternion)
		#print(state.pelvis.rotationalVelocity[:])
		#print(new_rotationalVelocity, state.pelvis.rotationalVelocity[:])
		#print(state.rightFoot.toeForce[:])
		'''new_lefttoeForce = rotate_by_quaternion(state.leftFoot.toeForce[:], iquaternion)
		new_leftFootheelForce = rotate_by_quaternion(state.leftFoot.heelForce[:], iquaternion)
		new_rightFootheelForce = rotate_by_quaternion(state.rightFoot.heelForce[:], iquaternion)
		new_rightFoottoeForce = rotate_by_quaternion(state.rightFoot.toeForce[:], iquaternion)'''

		useful_state = np.copy(np.concatenate([0 * np.ones(1), [state.pelvis.position[2] - state.terrain.height], new_orientation[:], state.motor.position[:], new_translationalVelocity[:], state.pelvis.rotationalVelocity[:], state.motor.velocity[:], new_translationalAcceleration[:], state.leftFoot.toeForce[:], state.leftFoot.heelForce[:], state.rightFoot.toeForce[:], state.rightFoot.heelForce[:]]))
		#print(useful_state[1])

		return np.concatenate([useful_state, ref_pos[pos_index], ref_vel[vel_index]])

	def get_mirror_state(self, state):
		orientation = np.copy(state[0:1])
		height = np.copy(state[1:2])
		pelvis_orientation = np.copy(state[2:6])
		euler = quaternion2euler(pelvis_orientation)
		euler[0] = -euler[0]
		euler[2] = -euler[2]
		pelvis_orientation = euler2quat(z=euler[2],y=euler[1],x=euler[0])
		

		left_motor_p = np.copy(state[6:11])
		left_motor_p[0:2] *= -1
		right_motor_p = np.copy(state[11:16])
		right_motor_p[0:2] *= -1
		pelvis_vel = np.copy(state[16:19])
		pelvis_vel[1] = -pelvis_vel[1]
		pelvis_rot_vel =np.copy(state[19:22])
		pelvis_rot_vel[0] = -pelvis_rot_vel[0]
		pelvis_rot_vel[2] = -pelvis_rot_vel[2]
		left_motor_v = np.copy(state[22:27])
		right_motor_v = np.copy(state[27:32])
		left_motor_v[0:2] *= -1
		right_motor_v[0:2] *= -1
		pelvis_acc = np.copy(state[32:35])
		pelvis_acc[1] = -pelvis_acc[1]
		left_toe_force = np.copy(state[35:38])
		left_toe_force[1] = -left_toe_force[1]
		left_heel_force = np.copy(state[38:41])
		left_heel_force[1] = -left_heel_force[1]
		right_toe_force = np.copy(state[41:44])
		right_toe_force[1] = -right_toe_force[1]
		right_heel_force = np.copy(state[44:47])
		right_heel_force[1] = -right_heel_force[1]
		ref_pelvis_p = np.copy(state[47:53])
		ref_pelvis_p[0] = -ref_pelvis_p[0]
		ref_pelvis_p[3] = -ref_pelvis_p[3]
		ref_pelvis_p[5] = -ref_pelvis_p[5]
		ref_left_motor_p = np.copy(state[53:60])
		ref_right_motor_p = np.copy(state[60:67])
		ref_left_motor_p[0:2] *= -1
		ref_right_motor_p[0:2] *= -1
		ref_vel_p = np.copy(state[67:73])
		ref_vel_p[1] = -ref_vel_p[1]
		ref_vel_p[3] = -ref_vel_p[3]
		ref_vel_p[5] = -ref_vel_p[5]
		ref_left_motor_v = np.copy(state[73:80])
		ref_right_motor_v = np.copy(state[80:87])
		ref_left_motor_v[0:2] *= -1
		ref_right_motor_v[0:2] *= -1
		return np.concatenate([np.copy(orientation), np.copy(height), np.copy(pelvis_orientation), np.copy(right_motor_p), np.copy(left_motor_p), np.copy(pelvis_vel), np.copy(pelvis_rot_vel), np.copy(right_motor_v), np.copy(left_motor_v), np.copy(pelvis_acc), np.copy(right_toe_force), np.copy(right_heel_force), np.copy(left_toe_force), np.copy(left_heel_force), np.copy(ref_pelvis_p), np.copy(ref_right_motor_p), np.copy(ref_left_motor_p), np.copy(ref_vel_p), np.copy(ref_right_motor_v), np.copy(ref_left_motor_v)])

	def get_mirror_action(self, action):
		left_action = np.copy(action[0:5])
		right_action = np.copy(action[5:10])
		return np.concatenate([right_action, left_action])

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*self.control_rate])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		vel = np.copy(self.trajectory.qvel[self.phase*self.control_rate])
		vel[0] = 0
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= self.max_phase:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*self.control_rate])
		vel = np.copy(self.trajectory.qvel[phase*self.control_rate])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		vel[0] = 0
		return pose, vel

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0


		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = self.cassie_state.pelvis.position[:]

		com_penalty = (pelvis_pos[0])**2 + (pelvis_pos[1])**2 + (pelvis_pos[2]-ref_pos[2])**2

		yaw = quat2yaw(self.sim.qpos()[3:7])

		orientation_penalty = (self.sim.qpos()[4]*20)**2+(self.sim.qpos()[5]*20)**2+(yaw - self.orientation)**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		foot_forces = self.get_foot_forces()
		#print(foot_forces)
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-150)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-150)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]

		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-orientation_penalty)+0.1*np.exp(-spring_penalty)

		return total_reward

	def reset(self):
		self.orientation = 0 * np.pi#0.5*np.pi#0.5*np.pi#random.randint(-4, 4) * np.pi / 4.0
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation + random.randint(-20, 20) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		#yaw = quat2yaw(quaternion)
		#print(yaw - self.orientation)
		#print(np.arcsin(quaternion[3])-self.orientation/2)
		self.phase = 0#random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		self.orientation = 0.0*np.pi#random.randint(-10, 10) * np.pi / 10.0
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation + random.randint(-20, 20) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		#print(quaternion[3]-np.sin(self.orientation/2))
		self.phase = random.randint(0, self.max_phase-1)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def set_state(self, qpos, qvel):
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)

class cassieRLEnvWithFootForces(cassieRLEnvStepInPlaceWithFootForces):
	def __init__(self):
		super().__init__()
		self.speed = 1.0
		self.rest_pose = np.array([0, 0, 1.05, 1, 0, 0, 0, 0.0045, 0, 0.4973, 0.9785, -0.0164, 0.01787, -0.2049,
         -1.1997, 0, 1.4267, 0, -1.5244, 1.5244, -1.5968,
         -0.0045, 0, 0.4973, 0.9786, 0.00386, -0.01524, -0.2051,
         -1.1997, 0, 1.4267, 0, -1.5244, 1.5244, -1.5968])
		#self.observation_space = np.zeros(87)

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*self.control_rate])
		pose[0] *= self.speed
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
		pose[1] = 0
		vel = np.copy(self.trajectory.qvel[self.phase*self.control_rate])
		vel[0] *= self.speed
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*self.control_rate])
		pose[0] *= self.speed
		vel = np.copy(self.trajectory.qvel[phase*self.control_rate])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
		pose[1] = 0
		vel[0] *= self.speed
		return pose, vel

	def reset(self):
		#self.speed = 1.0
		self.orientation = 0#random.randint(-4, 4) * np.pi / 4.0
		self.speed = random.randint(0, 10) / 10
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		#self.speed = 1.0
		self.speed = random.randint(0, 10) / 10
		self.orientation = 0#random.randint(-10, 10) * np.pi / 10.0
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, self.max_phase-1)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0


		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = self.cassie_state.pelvis.position[:]

		desired_x = ref_pos[0]*np.cos(self.orientation)
		desired_y = ref_pos[1]*np.sin(self.orientation)

		com_penalty = (pelvis_pos[0]-desired_x)**2 + (pelvis_pos[1]-desired_y)**2 + (pelvis_pos[2]-ref_pos[2])**2
		#print("x", pelvis_pos[0]-desired_x, "y", pelvis_pos[1]-desired_y)

		yaw = quat2yaw(self.sim.qpos()[3:7])

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		foot_forces = self.get_foot_forces()
		#print(foot_forces)
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-200)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-200)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]
		#print(0.5*np.exp(-joint_penalty), 0.3*np.exp(-com_penalty), 0.2*np.exp(-30*orientation_penalty))

		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-30*orientation_penalty)+0.1*np.exp(-force_penalty)#0.1*np.exp(-force_penalty)

		return total_reward


class cassieRLEnvBackwardWithFootForces(cassieRLEnvStepInPlaceWithFootForces):
	def __init__(self):
		super().__init__()
		self.speed = -1.0

	def get_kin_state(self):
		phase = 28 - self.phase
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		pose[0] = self.trajectory.qpos[self.phase*2*30, 0]
		pose[0] = pose[0] * self.speed
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
		pose[1] = 0
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		vel[0] *= self.speed
		#print(pose[0])
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		phase = 28 - phase
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
		#print(pose[0])
		pose[1] = 0
		vel[0] *= self.speed
		return pose, vel

	def reset(self):
		#self.speed = 1.0
		self.orientation = 0#random.randint(-4, 4) * np.pi / 4.0
		self.speed = -1.0*random.randint(0, 10) / 10
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		#self.speed = 1.0
		self.speed = -1.0*random.randint(0, 10) / 10
		self.orientation = 0#random.randint(-10, 10) * np.pi / 10.0
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0


		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = self.cassie_state.pelvis.position[:]

		desired_x = ref_pos[0]*np.cos(self.orientation)
		desired_y = ref_pos[1]*np.sin(self.orientation)

		com_penalty = (pelvis_pos[0]-desired_x)**2 + (pelvis_pos[1]-desired_y)**2 + (pelvis_pos[2]-ref_pos[2])**2

		yaw = quat2yaw(self.sim.qpos()[3:7])

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		foot_forces = self.get_foot_forces()
		#print(foot_forces)
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-200)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-200)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]

		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-30*orientation_penalty)+0.1*np.exp(-force_penalty)

		return total_reward

class cassieRLEnvSideSteppingWithFootForces(cassieRLEnvStepInPlaceWithFootForces):
	def __init__(self):
		super().__init__()
		self.speed = 1.0
		self.speed_y = 0.4
		self.observation_space = np.zeros(86)

	def get_state(self):
		#if len(self.state_buffer) >= self.buffer_size and self.delay:
		#	random_index = random.randint(0, self.buffer_size - 1)
		#	state = self.state_buffer[random_index]
			#print(random_index)
		#else:
		state = self.cassie_state

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])


		#state.pelvis.orientation[3] -= np.sin(self.orientation/2)
		quaternion = euler2quat(z=self.orientation, y=0, x=0)
		iquaternion = inverse_quaternion(quaternion)
		new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
		#print(new_orientation)
		new_translationalVelocity = rotate_by_quaternion(state.pelvis.translationalVelocity[:], iquaternion)
		#print(new_translationalVelocity)
		new_translationalAcceleration = rotate_by_quaternion(state.pelvis.translationalAcceleration[:], iquaternion)
		new_rotationalVelocity = rotate_by_quaternion(state.pelvis.rotationalVelocity[:], quaternion)
		useful_state = np.copy(np.concatenate([0 * np.ones(1), state.pelvis.position[2:3], new_orientation[:], state.motor.position[:], new_translationalVelocity[:], state.pelvis.rotationalVelocity[:], state.motor.velocity[:], new_translationalAcceleration[:], state.leftFoot.toeForce[:], state.leftFoot.heelForce[:], state.rightFoot.toeForce[:], state.rightFoot.heelForce[:]]))
		#print(useful_state[1])

		return np.concatenate([useful_state, ref_pos[pos_index], ref_vel[vel_index]])

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
		pose[1] = pose[0] * self.speed_y
		vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		vel[0] *= self.speed
		vel[1] = self.speed_y
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
		pose[1] = pose[0] * self.speed_y
		vel[0] *= self.speed
		vel[1] = self.speed_y
		return pose, vel

	def reset(self):
		#self.speed = 1.0
		self.orientation = 0#random.randint(-4, 4) * np.pi / 4.0
		#self.speed = -1.0*random.randint(0, 10) / 10
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation #+ random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		#self.speed = 1.0
		#self.speed = -1.0*random.randint(0, 10) / 10
		self.orientation = 0#random.randint(-10, 10) * np.pi / 10.0
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation# + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.05, 0.05, 0.1, 0.05, 0.05, 0.05, 0.05, 0.1, 0.05, 0.05]
		joint_penalty = 0


		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = self.cassie_state.pelvis.position[:]

		desired_x = ref_pos[0]#*np.cos(self.orientation)
		desired_y = ref_pos[1]#*np.sin(self.orientation)

		com_penalty = (pelvis_pos[0]-desired_x)**2 + (pelvis_pos[1]-desired_y)**2 + (pelvis_pos[2]-ref_pos[2])**2

		print("x", pelvis_pos[0]-desired_x, "y", pelvis_pos[1]-desired_y)

		yaw = quat2yaw(self.sim.qpos()[3:7])
		print(yaw, self.orientation)

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		foot_forces = self.get_foot_forces()
		#print(foot_forces)
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-200)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-200)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]

		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.2*np.exp(-30*orientation_penalty)#+0.1*np.exp(-force_penalty)

		return total_reward

class cassieRLEnvWithFootForcesLess(cassieRLEnvWithFootForces):
	def __init__(self):
		super().__init__()
		self.speed = 1.0
		self.observation_space = np.zeros(85)
		self.trajectory = CassieTrajectory("trajectory/stepdata.bin")
		for i in range(14):
			self.trajectory.qpos[(i)*2*30, 7] = 0
			self.trajectory.qpos[(i)*2*30, 21] = 0
			self.trajectory.qpos[(i)*2*30, 8] = 0
			self.trajectory.qpos[(i)*2*30, 22] = 0
			self.trajectory.qpos[(i+14)*2*30, 7:21] = np.copy(self.trajectory.qpos[(i)*2*30, 21:35])
			self.trajectory.qpos[(i+14)*2*30, 21:35] = np.copy(self.trajectory.qpos[(i)*2*30, 7:21])
			self.trajectory.qpos[(i+14)*2*30, 12] = -self.trajectory.qpos[(i+14)*2*30, 12]
			self.trajectory.qpos[(i+14)*2*30, 26] = -self.trajectory.qpos[(i+14)*2*30, 26]
		for i in range(28):
			self.trajectory.qpos[(i)*2*30, 3:7] = np.array([1, 0, 0, 0])
			#self.trajectory.qvel[(i+14)*2*30, 6:] = np.copy(self.trajectory.qvel[(i)*2*30])

	def get_state(self):
		state = self.cassie_state

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])

		#state.pelvis.orientation[3] -= np.sin(self.orientation/2)
		quaternion = euler2quat(z=self.orientation, y=0, x=0)
		iquaternion = inverse_quaternion(quaternion)
		new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
		#print(new_orientation)
		new_translationalVelocity = rotate_by_quaternion(state.pelvis.translationalVelocity[:], iquaternion)
		#print(new_translationalVelocity)
		new_translationalAcceleration = rotate_by_quaternion(state.pelvis.translationalAcceleration[:], iquaternion)
		new_rotationalVelocity = rotate_by_quaternion(state.pelvis.rotationalVelocity[:], quaternion)
		useful_state = np.copy(np.concatenate([state.pelvis.position[2:3], new_orientation[:], state.motor.position[:], new_translationalVelocity[:], state.pelvis.rotationalVelocity[:], state.motor.velocity[:], new_translationalAcceleration[:], state.leftFoot.toeForce[:], state.leftFoot.heelForce[:], state.rightFoot.toeForce[:], state.rightFoot.heelForce[:]]))
		#print(useful_state[1])

		#print("sensor", state.pelvis.position[0:3], "true pos", self.sim.qpos()[0:3])

		return np.concatenate([useful_state, ref_pos[pos_index], ref_vel[vel_index]])

	def get_kin_state(self):
		if self.speed >= 0:
			pose = np.copy(self.trajectory.qpos[self.phase*2*30])
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
			pose[1] = 0
			pose[0] = 0
			vel = np.copy(self.trajectory.qvel[self.phase*2*30])
			vel[0] = 0
		else:
			phase = 28 - self.phase
			pose = np.copy(self.trajectory.qpos[phase*2*30])
			pose[0] = self.trajectory.qpos[self.phase*2*30, 0]
			pose[0] = pose[0] * self.speed
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel = np.copy(self.trajectory.qvel[phase*2*30])
			vel[0] *= self.speed
		return pose, vel

	def get_kin_next_state(self):
		if self.speed >= 0:
			phase = self.phase + 1
			if phase >= 28:
				phase = 0
			pose = np.copy(self.trajectory.qpos[phase*2*30])
			vel = np.copy(self.trajectory.qvel[phase*2*30])
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
			pose[1] = 0
			pose[0] = 0
			vel[0] = 0
		else:
			phase = self.phase + 1
			if phase >= 28:
				phase = 0
			phase = 28 - phase
			pose = np.copy(self.trajectory.qpos[phase*2*30])
			vel = np.copy(self.trajectory.qvel[phase*2*30])
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			#print(pose[0])
			pose[1] = 0
			vel[0] *= self.speed
		return pose, vel

	def get_mirror_state(self, state):
		#orientation = np.copy(state[0:1])
		height = np.copy(state[0:1])
		pelvis_orientation = np.copy(state[1:5])
		euler = quaternion2euler(pelvis_orientation)
		euler[0] = -euler[0]
		euler[2] = -euler[2]
		pelvis_orientation = euler2quat(z=euler[2],y=euler[1],x=euler[0])

		left_motor_p = np.copy(state[5:10])
		left_motor_p[0:2] *= -1
		right_motor_p = np.copy(state[10:15])
		right_motor_p[0:2] *= -1
		pelvis_vel = np.copy(state[15:18])
		pelvis_vel[1] = -pelvis_vel[1]
		pelvis_rot_vel =np.copy(state[18:21])
		pelvis_rot_vel[0] = -pelvis_rot_vel[0]
		pelvis_rot_vel[2] = -pelvis_rot_vel[2]
		left_motor_v = np.copy(state[21:26])
		left_motor_v[0:2] *= -1
		right_motor_v = np.copy(state[26:31])
		right_motor_v[0:2] *= -1
		pelvis_acc = np.copy(state[31:34])
		pelvis_acc[1] = -pelvis_acc[1]
		left_toe_force = np.copy(state[34:37])
		left_toe_force[1] = -left_toe_force[1]
		left_heel_force = np.copy(state[37:40])
		left_heel_force[1] = -left_heel_force[1]
		right_toe_force = np.copy(state[40:43])
		right_toe_force[1] = -right_toe_force[1]
		right_heel_force = np.copy(state[43:46])
		right_heel_force[1] = -right_heel_force[1]
		ref_pelvis_p = np.copy(state[46:51])
		#ref_pelvis_p[0] = -ref_pelvis_p[0]
		ref_pelvis_p[2] = -ref_pelvis_p[2]
		ref_pelvis_p[3] = -ref_pelvis_p[3]
		ref_left_motor_p = np.copy(state[51:58])
		ref_left_motor_p[0:2] *= -1
		ref_right_motor_p = np.copy(state[58:65])
		ref_right_motor_p[0:2] *= -1
		ref_vel_p = np.copy(state[65:71])
		ref_vel_p[1] = -ref_vel_p[1]
		ref_vel_p[3] = -ref_vel_p[3]
		ref_vel_p[5] = -ref_vel_p[5]
		ref_left_motor_v = np.copy(state[71:78])
		ref_left_motor_v[0:2] *= -1
		ref_right_motor_v = np.copy(state[78:85])
		ref_right_motor_v[0:2] *= -1
		return np.concatenate([np.copy(height), np.copy(pelvis_orientation), np.copy(right_motor_p), np.copy(left_motor_p), np.copy(pelvis_vel), np.copy(pelvis_rot_vel), np.copy(right_motor_v), np.copy(left_motor_v), np.copy(pelvis_acc), np.copy(right_toe_force), np.copy(right_heel_force), np.copy(left_toe_force), np.copy(left_heel_force), np.copy(ref_pelvis_p), np.copy(ref_right_motor_p), np.copy(ref_left_motor_p), np.copy(ref_vel_p), np.copy(ref_right_motor_v), np.copy(ref_left_motor_v)])

	def reset(self):
		#self.speed = 1.0
		self.orientation = 0#random.randint(-4, 4) * np.pi / 4.0
		self.speed = -1 * random.randint(0, 10) / 10
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		#self.speed = 1.0
		self.speed = -1 * random.randint(0, 10) / 10
		self.orientation = 0#random.randint(-10, 10) * np.pi / 10.0
		#self.orientation = -0.712 * np.pi
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0


		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = self.cassie_state.pelvis.position[:]

		desired_x = ref_pos[0]*np.cos(self.orientation)
		desired_y = ref_pos[1]*np.sin(self.orientation)

		com_penalty = (pelvis_pos[0]-desired_x)**2 + (pelvis_pos[1]-desired_y)**2 + (pelvis_pos[2]-ref_pos[2])**2
		#print("x", pelvis_pos[0]-desired_x, "y", pelvis_pos[1]-desired_y)

		yaw = quat2yaw(self.sim.qpos()[3:7])

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		foot_forces = self.get_foot_forces()
		#print(foot_forces)
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-200)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-200)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]

		speed_penalty = (self.sim.qvel()[0] - ref_vel[0])**2 + (self.sim.qvel()[1] - ref_vel[1])**2
		#print(speed_penalty)
		#print(0.5*np.exp(-joint_penalty), 0.3*np.exp(-com_penalty), 0.2*np.exp(-30*orientation_penalty))
		#total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-orientation_penalty)+0.1*np.exp(-spring_penalty)

		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-10*orientation_penalty)+0.1*np.exp(-5*speed_penalty)#+0.1*np.exp(-spring_penalty)#+0.1*np.exp(-force_penalty)#0.1*np.exp(-force_penalty)

		return total_reward

class cassieRLEnvWithFootForcesLessHigherFoot(cassieRLEnvWithFootForcesLess):
	def __init__(self):
		super().__init__()
		self.higherfootdata = np.array([0.00042345002206552217, 0.024996789026977884, 1.0091868555579184, 0.7651178930754755, -0.005108838142607584, 0.07879546091654344, -0.6390303473290821, -2.192854172592867, 0.004446333191512813, 2.3965238794838375, 0.00135515492741686, -1.5275838449602654, 1.5091577734340855, -1.6353856143984384, 0.10054080868910445, -0.014019219893468186, 1.011560313669182, 0.765106617673535, -0.0046878014529935196, -0.07061146686786338, -0.6400030537868311, -2.1929329862847458, 0.004484356343494892, 2.396601037694866, 0.0013188056358714399, -1.5275235330737484, 1.509097589697184, -1.635325258728461])
		self.higherfootdata[14:28] = np.copy(self.higherfootdata[0:14])
		self.higherfootdata[19] = -self.higherfootdata[19]
		#self.higherfootdata[5] *= 0.5
		#self.higherfootdata[19] *= 0.5

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		original_right_foot = np.copy(original_left_foot)
		original_right_foot[5] = -original_right_foot[5]

		if self.phase < 7:
			interpolate = self.phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif self.phase < 14:
			interpolate = (self.phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif self.phase < 21:
			interpolate = (self.phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (self.phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
			
		#pose[9] += 0.5
		#pose[23] += 0.5
		vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		vel[0] = 0
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		vel[0] = 0

		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])

		if phase < 7:
			interpolate = phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif phase < 14:
			interpolate = (phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif phase < 21:
			interpolate = (phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		return pose, vel