from RL_Multiprocessing import *

class RL_Residual(RL):
	def __init__(self, env, hidden_layer=[64, 64]):
		super().__init__(env, hidden_layer)
		with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
			self.residual_shared_obs_stats = pickle.load(input)
		self.residual_env = cassieRLEnvMirror()
		self.residual_model = ActorCriticNet(self.residual_env.observation_space.shape[0], self.residual_env.action_space.shape[0],[256, 256])
		#self.residual_model.load_state_dict(torch.load("torch_model/cassie3dMirror2kHz256X256Oct09.pt"))
		self.residual_model.load_state_dict(torch.load("torch_model/cassie3dMirror2kHz256X256Oct09.pt"))
		self.residual_weight = 1

	def run_test(self, num_test=1):
		state = self.env.reset_for_test()
		res_state = Variable(torch.Tensor(state[0:85]).unsqueeze(0))
		state = Variable(torch.Tensor(state).unsqueeze(0))
		model_old = ActorCriticNet(self.num_inputs, self.num_outputs,self.hidden_layer)
		model_old.load_state_dict(self.model.state_dict())
		ave_test_reward = 0

		total_rewards = []
		'''self.fig2.clear()
		circle1 = plt.Circle((0, 0), 0.5, edgecolor='r', facecolor='none')
		circle2 = plt.Circle((0, 0), 0.01, edgecolor='r', facecolor='none')
		plt.axis('equal')'''
		
		for i in range(num_test):
			total_reward = 0
			while True:
				residual_state = self.residual_shared_obs_stats.normalize(res_state)
				residual_action, _, _ = self.residual_model(residual_state)
				state = self.shared_obs_stats.normalize(state)
				mu, log_std, v = self.model(state)
				action = 0.2 * mu.data.squeeze().numpy() + self.residual_weight * residual_action.data.squeeze().numpy()
				state, reward, done, _ = self.env.step(action)
				total_reward += reward
				#print(state)
				#print("done", done, "state", state)

				if done:
					state = self.env.reset_for_test()
					res_state = Variable(torch.Tensor(state[0:85]).unsqueeze(0))
					state = Variable(torch.Tensor(state).unsqueeze(0))
					ave_test_reward += total_reward / num_test
					total_rewards.append(total_reward)
					break
				res_state = Variable(torch.Tensor(state[0:85]).unsqueeze(0))
				state = Variable(torch.Tensor(state).unsqueeze(0))
		#print("avg test reward is", ave_test_reward)

		reward_mean = statistics.mean(total_rewards)
		reward_std = statistics.stdev(total_rewards)
		self.test_mean.append(reward_mean)
		self.test_std.append(reward_std)
		self.test_list.append((reward_mean, reward_std))
	def collect_samples(self, num_samples, start_state=None, noise=-2.0, env_index=0, random_seed=1):

		random.seed(random_seed)
		torch.manual_seed(random_seed+1)
		np.random.seed(random_seed+2)

		if start_state == None:
			start_state = self.env.reset()
		samples = 0
		done = False
		states = []
		next_states = []
		actions = []
		rewards = []
		values = []
		q_values = []
		self.model.set_noise(noise)
		model_old = ActorCriticNet(self.num_inputs, self.num_outputs, self.hidden_layer)
		model_old.load_state_dict(self.model.state_dict())
		model_old.set_noise(noise)


		state = start_state
		res_state = Variable(torch.Tensor(state[0:85]).unsqueeze(0))
		state = Variable(torch.Tensor(state).unsqueeze(0))
		total_reward = 0
		#q_value = Variable(torch.zeros(1, 1))
		while True:
			self.model.set_noise(-2.0)
			model_old.set_noise(-2.0)
			signal_init = self.traffic_light.get()
			while samples < num_samples and not done:
				residual_state = self.residual_shared_obs_stats.normalize(res_state)
				residual_action, _, _ = self.residual_model(residual_state)
				state = self.shared_obs_stats.normalize(state)
				states.append(state.data.numpy())
				mu, log_std, v = model_old(state)
				eps = torch.randn(mu.size())
				action = (mu + log_std.exp()*Variable(eps))
				actions.append(action.data.numpy())
				values.append(v.data.numpy())
				env_action = 0.2 * action.data.squeeze().numpy() + self.residual_weight * residual_action.data.squeeze().numpy()
				state, reward, done, _ = self.env.step(env_action)
				total_reward += reward
				reward = reward
				rewards.append(Variable(reward * torch.ones(1)).data.numpy())
				#q_value = self.gamma * q_value + Variable(reward * torch.ones(1))
				res_state = Variable(torch.Tensor(state[0:85]).unsqueeze(0))
				state = Variable(torch.Tensor(state).unsqueeze(0))

				next_state = self.shared_obs_stats.normalize(state)
				next_states.append(next_state.data.numpy())

				samples += 1

			state = self.shared_obs_stats.normalize(state)
			_,_,v = model_old(state)
			if done:
				R = torch.zeros(1, 1)
			else:
				R = v.data
			R = Variable(R)
			for i in reversed(range(len(rewards))):
				R = self.params.gamma * R + Variable(torch.from_numpy(rewards[i]))
				q_values.insert(0, R.data.numpy())

			#self.memory.push([states, actions, next_states, rewards, q_values])
			#return [states, actions, next_states, rewards, q_values]
			self.queue.put([states, actions, next_states, rewards, q_values])
			self.counter.increment()
			self.env.reset()
			while self.traffic_light.get() == signal_init:
				pass
			start_state = self.env.reset()
			state = start_state
			state = Variable(torch.Tensor(state).unsqueeze(0))
			total_reward = 0
			samples = 0
			done = False
			states = []
			next_states = []
			actions = []
			rewards = []
			values = []
			q_values = []
			model_old = ActorCriticNet(self.num_inputs, self.num_outputs, self.hidden_layer)
			model_old.load_state_dict(self.model.state_dict())
			model_old.set_noise(-2.0)
	def collect_samples_multithread(self):
		self.lr = 3e-4
		seed1 = np.random.randint(0, 4294967296)
		seed2 = np.random.randint(0, 4294967296)
		seed3 = np.random.randint(0, 4294967296)
		seed4 = np.random.randint(0, 4294967296)
		seed5 = np.random.randint(0, 4294967296)
		seed6 = np.random.randint(0, 4294967296)
		seed7 = np.random.randint(0, 4294967296)
		seed8 = np.random.randint(0, 4294967296)
		seed9 = np.random.randint(0, 4294967296)
		seed10 = np.random.randint(0, 4294967296)

		t1 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':0, 'random_seed':seed1})
		t2 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':1, 'random_seed':seed2})
		t3 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':2, 'random_seed':seed3})
		t4 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':3, 'random_seed':seed4})
		t5 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':4, 'random_seed':seed5})
		t6 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':5, 'random_seed':seed6})
		t7 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':6, 'random_seed':seed7})
		t8 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':7, 'random_seed':seed8})
		t9 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':8, 'random_seed':seed9})
		t10 = mp.Process(target=self.collect_samples,args=(300,), kwargs={'noise':-2.0, 'env_index':9, 'random_seed':seed10})
			
		t1.start()
		t2.start()
		t3.start()
		t4.start()
		t5.start()
		t6.start()
		t7.start()
		t8.start()
		t9.start()
		t10.start()
		self.model.set_noise(-2.0)
		while True:
			self.save_model("torch_model/cassie3dTerrainNov19.pt")
			while len(self.memory.memory) < 9000:
				if self.counter.get() == 10:
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
					self.counter.increment()
				if len(self.memory.memory) < 9000 and self.counter.get() == 11:
					self.counter.reset()
					self.traffic_light.switch()
					#print(len(self.memory.memory))
			self.update_critic(128, 256)
			self.update_actor(128, 256)
			self.clear_memory()
			self.run_test(num_test=2)
			self.plot_statistics()
			self.traffic_light.switch()
			self.counter.reset()

if __name__ == '__main__':
	env = cassieRLEnvTerrain()

	ppo = RL_Residual(env, [1024, 512, 256, 128])
	ppo.normalize_data(num_iter=10000)
	ppo.save_shared_obs_stas('torch_model/cassie3dTerrainResidualNov192kHz_shared_obs_stats.pkl')
	#with open('torch_model/cassie3dTerrainResidualNov192kHz_shared_obs_stats.pkl', 'rb') as input:
	#	ppo.shared_obs_stats = pickle.load(input)
	#ppo.model.load_state_dict(torch.load("torch_model/cassie3dTerrainNov06.pt"))
	ppo.collect_samples_multithread()
	

	start = t.time()

	noise = -2.0
