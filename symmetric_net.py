import math
import torch as th
import torch.nn as nn
from torch.nn.parameter import Parameter
from torch.nn import init
from torch.nn import functional as F
from model import Shared_obs_stats

class SymmetricLayer(nn.Module):
	__constants__ = ["sbias", "cbias"]
	__weights__ = ["c2s", "n2s", "c2c", "s2c", "s1s", "s2s", "n2n", "s2n"]

	def __init__(self, in_const, in_neg, in_side, out_const, out_neg, out_side, wmag=1):
		"""
		"""
		super().__init__()

		self.in_const = in_const
		self.in_neg = in_neg
		self.in_side = in_side
		self.out_const = out_const
		self.out_neg = out_neg
		self.out_side = out_side

		# self.c2s = Parameter(th.Tensor(out_side, in_const))
		# self.s1s = Parameter(th.Tensor(out_side, in_side))
		# self.s2s = Parameter(th.Tensor(out_side, in_side))
		# self.n2s = Parameter(th.Tensor(out_side, in_neg))
		# self.sbias = Parameter(th.Tensor(out_side))

		# self.s2c = Parameter(th.Tensor(out_const, in_side))
		# self.c2c = Parameter(th.Tensor(out_const, in_const))
		# self.cbias = Parameter(th.Tensor(out_const))

		# self.n2n = Parameter(th.Tensor(out_neg, in_neg))
		# self.s2n = Parameter(th.Tensor(out_neg, in_side))

		self.c2s = nn.Linear(in_const, out_side)
		self.c2s.weight.data /= (256/out_side)
		self.s1s = nn.Linear(in_side, out_side, bias=False)
		self.s1s.weight.data /= (256/out_side)
		self.s2s = nn.Linear(in_side, out_side, bias=False)
		self.s2s.weight.data /= (256/out_side)
		self.n2s = nn.Linear(in_neg, out_side, bias=False)
		self.n2s.weight.data /= (256/out_side)
		self.s2c = nn.Linear(in_side, out_const)
		self.s2c.weight.data /= (256/out_const)
		self.c2c = nn.Linear(in_const, out_const, bias=False)
		self.c2c.weight.data /= (256/out_const)
		self.n2n = nn.Linear(in_neg, out_neg, bias=False)
		self.n2n.weight.data /= (256/out_neg)
		self.s2n = nn.Linear(in_side, out_neg, bias=False)
		self.s2n.weight.data /= (256/out_neg)
		#self.reset_parameters(wmag)

	def forward(self, c, n, l, r):
		# c2s = F.linear(c, self.c2s, self.sbias)
		# n2s = F.linear(n, self.n2s)
		# s2c = F.linear((l + r) / 2, self.s2c, self.cbias)
		# s2n = F.linear(l - r, self.s2n)
		# return (
		# 	s2c + F.linear(c, self.c2c),  # constant part
		# 	s2n + F.linear(n, self.n2n),  # negative part
		# 	c2s + n2s + F.linear(l, self.s1s) + F.linear(r, self.s2s),  # left
		# 	c2s - n2s + F.linear(r, self.s1s) + F.linear(l, self.s2s),  # right
		# )
		c2s = self.c2s(c)
		n2s = self.n2s(n)
		s2c = self.s2c((l + r) / 2)
		s2n = self.s2n(l-r)
		return ((s2c + self.c2c(c)), (s2n + self.n2n(n)),
			(c2s + n2s + self.s1s(l) + self.s2s(r)), 
			(c2s - n2s + self.s1s(r) + self.s2s(l)))


	def reset_parameters(self, wmag):
		for wname in self.__weights__:
			#init.xavier_normal_(getattr(self, wname), gain=1 * wmag)
			init.kaiming_uniform_(getattr(self, wname), a=math.sqrt(5) * wmag)

		bound = 1 / math.sqrt(self.in_const + self.in_neg + 2 * self.in_side)
		self.sbias.data.fill_(0)
		self.cbias.data.fill_(0)
		#init.uniform_(self.sbias, -bound, bound)
		#init.uniform_(self.cbias, -bound, bound)


#         if self.bias is not None:
#             fan_in, _ = init._calculate_fan_in_and_fan_out(self.weight)
#             bound = 1 / math.sqrt(fan_in)
#             init.uniform_(self.bias, -bound, bound)

#     @weak_script_method
#     def forward(self, input):
#         return F.linear(input, self.weight, self.bias)

#     def extra_repr(self):
#         return 'in_features={}, out_features={}, bias={}'.format(
#             self.in_features, self.out_features, self.bias is not None
#         )


class SymmetricNet(nn.Module):
	def __init__(
		self,
		c_in,
		n_in,
		s_in,
		c_out,
		n_out,
		s_out,
		num_layers=3,
		hidden_size=16,
		tanh_finish=True,
		varying_std=False,
		log_std=-1,
		deterministic=False,
	):
		super().__init__()
		self.c_in = c_in
		self.s_in = s_in
		self.n_in = n_in
		self.tanh_finish = tanh_finish
		self.deterministic = deterministic

		self.layers = []
		last_cin = c_in
		last_nin = n_in
		last_sin = s_in
		for i in range(num_layers - 1):
			self.layers.append(
				SymmetricLayer(
					last_cin, last_nin, last_sin, hidden_size[0], hidden_size[1], hidden_size[2]
				)
			)
			self.add_module("layer%d" % i, self.layers[i])
			last_cin, last_nin, last_sin = hidden_size[0], hidden_size[1], hidden_size[2]
		self.layers.append(
			SymmetricLayer(last_cin, last_nin, last_sin, c_out, n_out, s_out, wmag=3e-3)
		)
		self.add_module("final", self.layers[-1])

		if not self.deterministic:
			action_size = c_out + n_out + 2 * s_out
			if varying_std:
				self.log_std_param = nn.Parameter(
					th.randn(action_size) * 1e-10 + log_std
				)
			else:
				self.log_std_param = log_std * th.ones(action_size)

	def forward(self, obs):
		# TODO: better fix than transpose?
		obs = obs.transpose(0, -1)
		c = obs[: self.c_in].transpose(0, -1)
		n = obs[self.c_in : self.c_in + self.n_in].transpose(0, -1)
		l = obs[self.c_in + self.n_in : self.c_in + self.n_in + self.s_in].transpose(
			0, -1
		)
		r = obs[-self.s_in :].transpose(0, -1)

		for i, layer in enumerate(self.layers):
			if i != 0:
				n = th.tanh(n)  # TODO
				c = th.relu(c)
				r = th.relu(r)
				l = th.relu(l)

			c, n, l, r = layer(c, n, l, r)
			#print(c, n, l, r)

		mean = th.cat([c, n, l, r], -1)
		if self.tanh_finish:
			mean = th.tanh(mean)

		if not self.deterministic:
			log_std = self.log_std_param.expand_as(mean)
			return mean, log_std
		else:
			return mean

	def set_noise(self, noise):
		pass
		# self.log_std_param = self.log_std_param * 0 + noise


class SymmetricPolicyValue(nn.Module):
	def __init__(self, *args, **kwargs):
		super().__init__()
		self.pnet = SymmetricNet(deterministic=False, tanh_finish=True, *args, **kwargs)
		self.noise = kwargs["log_std"]
		kwargs["s_out"] = 1
		kwargs["n_out"] = 1
		kwargs["c_out"] = 1
		#self.vnet = SymmetricNet(deterministic=True, tanh_finish=False, *args, **kwargs)

		self.v_fcs = nn.ModuleList()
		v_fc = nn.Linear(48, 256)
		self.v_fcs.append(v_fc)
		for i in range(len([256, 256])-1):
			v_fc = nn.Linear(256, 256)
			self.v_fcs.append(v_fc)
		self.v = nn.Linear(256, 1)

	def forward(self, obs):
		mean, log_std = self.pnet(obs)
		#state_value = self.vnet(obs)
		#print(obs, mean, log_std, state_value[:, -1:])
		x = F.relu(self.v_fcs[0](obs))
		for i in range(1):
			x = F.relu(self.v_fcs[i+1](x))
		v = self.v(x)

		return mean, log_std, v#(state_value[:,0:1])

	def set_noise(self, noise):
		pass
	#     self.policy.set_noise(noise)

class SymmetricStats(Shared_obs_stats):
	def __init__(self, c_in, n_in, s_in, *args, **kwargs):
		input_size = c_in + n_in + 2 * s_in
		super().__init__(input_size)
		self.zeros_inds = th.arange(c_in, c_in + n_in)
		self.shared_inds = th.stack(
			[
				th.arange(c_in + n_in, c_in + n_in + s_in),
				th.arange(c_in + n_in + s_in, c_in + n_in + 2 * s_in),
			]
		)

	def observes(self, obs):
		"""
		@brief update observation mean & stdev
		# @param obs: the observation. assuming NxS where N is the batch-size and S is the input-size
		@param obs: the observation (can be 1D or 2D in which case the first dimension is the batch-size)
		"""
		super().observes(obs)

		self.mean[self.zeros_inds] = 0

		shared_mean = self.mean[self.shared_inds].mean(0)
		shared_std = self.std[self.shared_inds].max(0)[0]

		for inds in self.shared_inds:
			self.mean[inds] = shared_mean
			self.std[inds] = shared_std