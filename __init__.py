import gym
import os


def register(id, **kvargs):
    if id in gym.envs.registration.registry.env_specs:
        return
    else:
        return gym.envs.registration.register(id, **kvargs)


# fixing package path
current_dir = os.path.dirname(os.path.realpath(__file__))
os.sys.path.append(current_dir)


register(
    id="CassieMJCEnv-v0",
    entry_point="cassie_sim_to_real.cassieRLPhase:cassieRLEnvPhase",
    max_episode_steps=300,
)

register(
    id="CassieMJCPhaseMirrorEnv-v0",
    entry_point="cassie_sim_to_real.cassieRLEnvMirrorPhase:cassieRLEnvMirrorPhaseGym",
    max_episode_steps=300,
)
