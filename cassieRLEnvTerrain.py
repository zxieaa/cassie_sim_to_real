from cassiemujoco_ctypes import *
import math
from quaternion_function import *
from cassieRLEnvMirror import *
import matplotlib.pyplot as plt

class cassieRLEnvTerrain(cassieRLEnvMirror):
	def __init__(self):
		super().__init__()
		self.speed = 0
		self.y_speed = 1
		self.observation_space = np.zeros(110)
		self.height_field = np.zeros(512)
		self.generate_terrain()
		self.visualize = False


	def step(self, action):
		for _ in range(60):
			self.step_simulation(action)

		height = self.cassie_state.pelvis.position[2] - self.cassie_state.terrain.height
		#print("height", height)
		self.time += 1
		self.phase += 1

		if self.phase >= 28:
			self.phase = 0
			self.counter +=1
		#print("height", height)

		done = not(height > 0.2 and height < 100.0) or self.time >= self.time_limit
		if self.sim.qpos()[0] < -1.0 or self.sim.qvel()[0] < 0.3:
			done = True
		yaw = quat2yaw(self.sim.qpos()[3:7])
		#print(yaw - self.orientation)
		#if (yaw-self.orientation)**2 > 0.15:
		#	done = True

		reward = self.compute_reward()
		#print("reward", reward)
		if reward < 0.3:
			done = True

		return self.get_state(), reward, done, {}

	def generate_terrain(self):
		height_field_size = np.zeros(4)
		slope = np.pi * 0.1
		for i in range(4):
			height_field_size[i] = self.sim.height_field_size()[i]
		height_field_size[2] = 12.8 * np.tan(slope)
		#fig = plt.figure()
		#ax = fig.add_subplot(111)
		#ax.plot(self.sim.height_field()[0:512])
		#plt.show()
		self.sim.set_height_field_size(height_field_size)
		data = np.zeros(512*512)
		for i in range(512):
			for j in range(512):
				if j == 0:
					data[i*512 + j] = float(0)
				else:
					data[i*512 + j] = float(np.tan(slope)*0.025+data[i*512 + j-1])
		data /= height_field_size[2]
		self.sim.set_height_field_data(data)

	def generate_hole_terrain(self):
		height_field_size = np.zeros(4)
		for i in range(4):
			height_field_size[i] = self.sim.height_field_size()[i]
		height_field_size[2] = 8
		self.sim.set_height_field_size(height_field_size)
		data = np.zeros(512*512)
		current_height = 0.5
		gap_counter = random.randint(7, 10)
		step_height = random.randint(-25, 25) / 1000.0
		for i in range(100):
			for j in range(512):
				data[i + 512*j] = current_height
				self.height_field[i] = current_height
		for i in range(100, 512):
			gap_counter -= 1
			if gap_counter <= 3:
				if gap_counter == 3:
					current_height = current_height + step_height
					if current_height < 0:
						current_height = 0
					if current_height >= 1:
						current_height = 1
				for j in range(512):
					data[i + 512*j] = current_height
					self.height_field[i] = current_height
			if gap_counter == 0:
				gap_counter = random.randint(7, 10)
				step_height = random.randint(-25, 25) / 1000.0
		self.sim.set_height_field_data(data)

	def generate_stairs(self):
		height_field_size = np.zeros(4)
		for i in range(4):
			height_field_size[i] = self.sim.height_field_size()[i]
		slope = np.pi * 0.1
		height_field_size[2] = 0.5
		self.sim.set_height_field_size(height_field_size)
		data = np.zeros(512*512)
		self.height_field = np.zeros(512)
		for i in range(100):
			for j in range(512):
				data[i + 512*j] = 0
		flat_counter = 0
		current_height = 0
		for i in range(100, 512):
			if flat_counter == 0:
				current_height += 0.1
				if current_height > 1.0:
					current_height = 1.0
				flat_counter = 10
			flat_counter -= 1
			for j in range(512):
				data[i + 512*j] = current_height
			self.height_field[i] = current_height
		self.sim.set_height_field_data(data)


	def get_state(self):
		state = super().get_state()
		ref_pos, ref_vel = np.copy(self.get_kin_state())
		x_pos = self.sim.qpos()[0]
		y_pos = self.sim.qpos()[1]
		height_field_index_x = int((x_pos + 3.8) // 0.05)

		#print(height_field_index_y)
		heights = self.height_field[height_field_index_x-5:height_field_index_x+20]
		#print(height_field[height_field_index_y-10:height_field_index_y+10,height_field_index_x-5:height_field_index_x+30])
		#heights -= self.height_field[height_field_index]
		heights *= 8.0
		heights -= 4.0
		heights -= self.sim.qpos()[2]
		for j in range(25):
			if heights[j] < -3:
				heights[j] = -3
		#print(heights)
		#print(heights.shape)
		return np.concatenate([state, heights])

	def reset(self):
		state = super().reset()
		self.height_field = np.zeros(512)
		self.generate_hole_terrain()
		if self.visualize:
			cassie_vis_free(self.vis.v)
			self.vis.v = cassie_vis_init(self.sim.c)
		return self.get_state()

	def reset_for_test(self):
		return self.reset()

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0
		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = np.copy(self.cassie_state.pelvis.position[:])
		com_penalty = (pelvis_pos[0]-ref_pos[0])**2 + (pelvis_pos[1]-ref_pos[1])**2 + (pelvis_pos[2]-ref_pos[2])**2
		#print(ref_vel[1])

		yaw = quat2yaw(self.sim.qpos()[3:7])

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		speed_penalty = (self.sim.qvel()[0] - ref_vel[0])**2 + (self.sim.qvel()[1] - ref_vel[1])**2

		foot_forces = self.get_foot_forces()
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-150)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-150)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]
		#print(force_penalty/1000)
		pelvis_vel = np.copy(self.cassie_state.pelvis.translationalVelocity[:])
		com_velocity_penalty = np.sqrt((pelvis_vel[0]-ref_vel[0]/0.8)**2) #+ (pelvis_vel[1]-ref_vel[1])**2 + (pelvis_vel[2]-ref_vel[2])**2
		#vel = np.sqrt(pelvis_vel[0]**2+pelvis_vel[2]**2)
		#com_velocity_penalty = np.sqrt((vel-ref_vel[0])**2)
		#print(self.phase, ref_pos[0])

		#total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.2*np.exp(-10*orientation_penalty)#+0.1*np.exp(-spring_penalty)
		total_reward = 1*np.exp(-com_velocity_penalty)#+0.1*np.exp(-spring_penalty)
		#print(total_reward)

		return total_reward
