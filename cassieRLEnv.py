from cassiemujoco import *
from loadstep import CassieTrajectory
import numpy as np
import os
import random
import pickle

class cassieRLEnv:
	def __init__(self):
		self.sim = CassieSim()
		self.vis = CassieVis(self.sim.c)
		self.observation_space = np.zeros(80)
		self.action_space = np.zeros(10)
		self.trajectory = CassieTrajectory(os.path.join(os.path.dirname(__file__), "trajectory", "stepdata.bin"))
		self.P = np.array([100, 100, 88, 96, 50, 100, 100, 88, 96, 50]) / 2.0
		self.D = self.P / 10
		#self.D = np.array([10.0, 10.0, 8.0, 9.6, 5.0, 10.0, 10.0, 8.0, 9.6, 5.0])
		self.u = pd_in_t()
		self.time = 0
		self.phase = 0
		self.counter = 0
		self.time_limit = 400

	def get_state(self):
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([1,2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])

		return np.concatenate([qpos[pos_index], qvel[vel_index], ref_pos[pos_index], ref_vel[vel_index]])

	def step_simulation(self, action):
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())

		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		target = action + ref_pos[pos_index]
		#control = self.P*(target-self.sim.data.qpos[index])-self.D*self.sim.data.qvel[vel_index]

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]
		self.sim.step_pd(self.u)

	def ref_action(self):
		ref_pos, ref_vel = self.get_kin_next_state()
		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		return ref_pos[pos_index]

	def step(self, action):
		for _ in range(60):
			self.step_simulation(action)

		height = self.sim.qpos()[2]
		self.time += 1
		self.phase += 1

		if self.phase >= 28:
			self.phase = 0
			self.counter +=1
		#print("height", height)

		done = not(height > 0.4 and height < 3.0) or self.time >= self.time_limit

		reward = self.compute_reward()
		if reward < 0.3:
			done = True

		return self.get_state(), reward, done, {}

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		return pose, vel

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0


		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		com_penalty = (ref_pos[0] - self.sim.qpos()[0])**2 + (self.sim.qpos()[1])**2 + (self.sim.qpos()[2]-ref_pos[2])**2

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(self.sim.qpos()[6])**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-orientation_penalty)+0.1*np.exp(-spring_penalty)

		return total_reward

	def reset(self):
		self.phase = 0#random.randint(0, 27)
		self.time = 0
		self.counter = 0
		qpos, qvel = self.get_kin_state()
		qvel[0] = 0
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		return self.reset()

	def set_state(self, qpos, qvel):
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)

	def record_state(self, state_list):
		state_list.append((self.sim.qpos(), self.sim.qvel()))

	def get_foot_forces(self):
		forces = np.zeros(12)
		self.sim.foot_force(forces)
		return forces

	#def get_foot_height(self):
	#	s = self.sim.get_state()
	#	print(s.s.leftFoot.position[:])

class cassieRLEnvDelay(cassieRLEnv):
	def __init__(self):
		self.sim = CassieSim()
		#self.vis = CassieVis(self.sim.c)
		self.observation_space = np.zeros(80)
		self.action_space = np.zeros(10)
		self.trajectory = CassieTrajectory(os.path.join(os.path.dirname(__file__), "trajectory", "stepdata.bin"))
		self.P = np.array([100, 100, 88, 96, 50, 100, 100, 88, 96, 50])
		self.D = np.array([10.0, 10.0, 8.0, 9.6, 5.0, 10.0, 10.0, 8.0, 9.6, 5.0])
		self.u = pd_in_t()
		self.time = 0
		self.phase = 0
		self.counter = 0
		self.time_limit = 400
		self.state_buffer = []
		self.delay = True
		self.buffer_size = 120
		self.noisy = True

	def step_simulation(self, action):
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())

		self.state_buffer.append((qpos, qvel))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)

		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		target = action + ref_pos[pos_index]
		#control = self.P*(target-self.sim.data.qpos[index])-self.D*self.sim.data.qvel[vel_index]

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]
		self.sim.step_pd(self.u)

	def get_state(self):
		if len(self.state_buffer) >= self.buffer_size and self.delay:
			random_index = random.randint(0, 20)
			state = self.state_buffer[random_index]
			qpos = np.copy(state[0])
			qvel = np.copy(state[1])
			#print(random_index)
		else:
			qpos = np.copy(self.sim.qpos())
			qvel = np.copy(self.sim.qvel())

		if self.noisy:
			qpos += np.random.normal(size=35)*0.01
			qvel += np.random.normal(size=32)*0.01

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([1,2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])

		return np.concatenate([qpos[pos_index], qvel[vel_index], ref_pos[pos_index], ref_vel[vel_index]])


class cassieRLEnvStepInPlace(cassieRLEnvDelay):
	def __init__(self):
		self.sim = CassieSim()
		self.vis = CassieVis(self.sim.c)
		self.observation_space = np.zeros(80)
		self.action_space = np.zeros(10)
		#self.trajectory = CassieTrajectory("trajectory/stepdata.bin")
		#self.trajectory = "step_in_place_trajectory"
		with open ("step_in_place_trajectory", 'rb') as fp:
			self.trajectory = pickle.load(fp)
		self.P = np.array([100, 100, 88, 96, 50, 100, 100, 88, 96, 50])
		self.D = np.array([10.0, 10.0, 8.0, 9.6, 5.0, 10.0, 10.0, 8.0, 9.6, 5.0])
		self.u = pd_in_t()
		self.time = 0
		self.phase = 0
		self.counter = 0
		self.time_limit = 400
		self.state_buffer = []
		self.buffer_size = 150
		self.delay = True

	def step_simulation(self, action):
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())

		self.state_buffer.append((qpos, qvel))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)

		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		target = action + ref_pos[pos_index]
		#control = self.P*(target-self.sim.data.qpos[index])-self.D*self.sim.data.qvel[vel_index]

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]
		self.sim.step_pd(self.u)

	def get_state(self):
		if len(self.state_buffer) >= 80 and self.delay:
			random_index = random.randint(0, 20)
			state = self.state_buffer[random_index]
			qpos = np.copy(state[0])
			qvel = np.copy(state[1])
			#print(random_index)
		else:
			qpos = np.copy(self.sim.qpos())
			qvel = np.copy(self.sim.qvel())

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([1,2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])

		return np.concatenate([qpos[pos_index], qvel[vel_index], ref_pos[pos_index], ref_vel[vel_index]])

	def get_kin_state(self):
		pose = np.copy(self.trajectory[self.phase][0])
		vel = np.copy(self.trajectory[self.phase][1])
		#pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		#pose[1] = 0
		#pose[0] = 0
		#vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory[self.phase][0])
		vel = np.copy(self.trajectory[self.phase][1])
		#pose = np.copy(self.trajectory.qpos[phase*2*30])
		#vel = np.copy(self.trajectory.qvel[phase*2*30])
		#pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		#pose[1] = 0
		#pose[0] = 0
		#vel[0] = 0
		return pose, vel

class cassieRLEnvHigherFoot(cassieRLEnvDelay):
	def __init__(self):
		self.sim = CassieSim()
		self.vis = CassieVis()
		self.observation_space = np.zeros(80)
		self.action_space = np.zeros(10)
		self.trajectory = CassieTrajectory("trajectory/stepdata.bin")
		self.P = np.array([100, 100, 88, 96, 50, 100, 100, 88, 96, 50])
		self.D = np.array([10.0, 10.0, 8.0, 9.6, 5.0, 10.0, 10.0, 8.0, 9.6, 5.0])
		self.u = pd_in_t()
		self.time = 0
		self.phase = 0
		self.counter = 0
		self.time_limit = 400
		self.higherfootdata = np.array([0.00042345002206552217, 0.024996789026977884, 1.0091868555579184, 0.7651178930754755, -0.005108838142607584, 0.07879546091654344, -0.6390303473290821, -2.192854172592867, 0.004446333191512813, 2.3965238794838375, 0.00135515492741686, -1.5275838449602654, 1.5091577734340855, -1.6353856143984384, 0.10054080868910445, -0.014019219893468186, 1.011560313669182, 0.765106617673535, -0.0046878014529935196, -0.07061146686786338, -0.6400030537868311, -2.1929329862847458, 0.004484356343494892, 2.396601037694866, 0.0013188056358714399, -1.5275235330737484, 1.509097589697184, -1.635325258728461])
		self.higherfootdata[14:28] = np.copy(self.higherfootdata[0:14])
		self.higherfootdata[19] = -self.higherfootdata[19]
		self.state_buffer = []
		self.buffer_size = 50
		self.delay = True

	def get_state(self):
		if len(self.state_buffer) >= 50 and self.delay:
			random_index = random.randint(0, 20)
			state = self.state_buffer[random_index]
			qpos = np.copy(state[0])
			qvel = np.copy(state[1])
			#print(random_index)
		else:
			qpos = np.copy(self.sim.qpos())
			qvel = np.copy(self.sim.qvel())
		#qpos = np.copy(self.sim.qpos())
		#qvel = np.copy(self.sim.qvel())

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([1,2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])

		return np.concatenate([qpos[pos_index], qvel[vel_index], ref_pos[pos_index], ref_vel[vel_index]])

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0


		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			if i == 7 or i == 8 or i == 21 or i == 22:
				joint_penalty += error*100
			else:
				joint_penalty += error*30

		com_penalty = (ref_pos[0] - self.sim.qpos()[0])**2 + (self.sim.qpos()[1])**2 + (self.sim.qpos()[2]-ref_pos[2])**2

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(self.sim.qpos()[6])**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		total_reward = 0.4*np.exp(-joint_penalty)+0.4*np.exp(-com_penalty)+0.1*np.exp(-orientation_penalty)+0.1*np.exp(-spring_penalty)

		return total_reward

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		#original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])
		original_right_foot = np.copy(original_left_foot)
		original_right_foot[5] = -original_right_foot[5]

		if self.phase < 7:
			interpolate = self.phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif self.phase < 14:
			interpolate = (self.phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif self.phase < 21:
			interpolate = (self.phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (self.phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
			
		#pose[9] += 0.5
		#pose[23] += 0.5
		vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		vel[0] = 0
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		pose[0] = 0
		vel[0] = 0
		#pose[9] += 0.5
		#pose[23] += 0.5

		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])

		if phase < 7:
			interpolate = phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif phase < 14:
			interpolate = (phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif phase < 21:
			interpolate = (phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		return pose, vel

class cassieRLEnvHigherFootForward(cassieRLEnvHigherFoot):
	def __init__(self):
		super().__init__()

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])

		if self.phase < 7:
			interpolate = self.phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif self.phase < 14:
			interpolate = (self.phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif self.phase < 21:
			interpolate = (self.phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (self.phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
			
		#pose[9] += 0.5
		#pose[23] += 0.5
		vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		#pose[9] += 0.5
		#pose[23] += 0.5

		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])

		if phase < 7:
			interpolate = phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif phase < 14:
			interpolate = (phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif phase < 21:
			interpolate = (phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		return pose, vel

class cassieRLEnvSpeed(cassieRLEnvDelay):
	def __init__(self):
		super().__init__()
		self.speed = 0
		self.observation_space = np.zeros(81)
		self.delay = True
		self.buffer_size = 10
		self.noisy = True

	def set_speed(self, speed):
		self.speed = speed

	def get_state(self):
		if len(self.state_buffer) >= self.buffer_size and self.delay:
			random_index = random.randint(0, 5)
			state = self.state_buffer[random_index]
			qpos = np.copy(state[0])
			qvel = np.copy(state[1])
			#print(random_index)
		else:
			qpos = np.copy(self.sim.qpos())
			qvel = np.copy(self.sim.qvel())

		if self.noisy:
			qpos += np.random.normal(size=35)*0.01
			qvel += np.random.normal(size=32)*0.01
		#qpos = np.copy(self.sim.qpos())
		#qvel = np.copy(self.sim.qvel())

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([1,2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])

		return np.concatenate([self.speed*np.ones(1), qpos[pos_index], qvel[vel_index], ref_pos[pos_index], ref_vel[vel_index]])

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*2*30])
		pose[0] *= self.speed
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
		pose[1] = 0

		vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		vel[0] * self.speed
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		pose[0] *= self.speed
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
		pose[1] = 0
		vel[0] *= self.speed
		return pose, vel

	def reset(self):
		self.phase = random.randint(0, 27)
		self.speed = random.randint(0, 10) / 10.0
		self.speed = 0
		self.time = 0
		self.counter = 0
		qpos, qvel = self.get_kin_state()
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		return self.reset()

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0


		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			if i == 7 or i == 8 or i == 21 or i == 22:
				joint_penalty += error*100
			else:
				joint_penalty += error*30

		com_penalty = (ref_pos[0] - self.sim.qpos()[0])**2 + (self.sim.qpos()[1])**2 + (self.sim.qpos()[2]-ref_pos[2])**2

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(self.sim.qpos()[6])**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		total_reward = 0.3*np.exp(-joint_penalty)+0.5*np.exp(-com_penalty)+0.1*np.exp(-orientation_penalty)+0.1*np.exp(-spring_penalty)

		return total_reward

class cassieRLEnvHigherFootSpeed(cassieRLEnvHigherFootForward):
	def __init__(self):
		super().__init__()
		self.speed = 0
		self.observation_space = np.zeros(81)
		self.buffer_size = 50

	def get_state(self):
		if len(self.state_buffer) >= self.buffer_size and self.delay:
			random_index = random.randint(0, 20)
			state = self.state_buffer[random_index]
			qpos = np.copy(state[0])
			qvel = np.copy(state[1])
			#print(random_index)
		else:
			qpos = np.copy(self.sim.qpos())
			qvel = np.copy(self.sim.qvel())

		ref_pos, ref_vel = self.get_kin_next_state()

		pos_index = np.array([1,2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])

		return np.concatenate([self.speed*np.ones(1), qpos[pos_index], qvel[vel_index], ref_pos[pos_index], ref_vel[vel_index]])

	def get_kin_state(self):
		pose = np.copy(self.trajectory.qpos[self.phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])

		if self.phase < 7:
			interpolate = self.phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif self.phase < 14:
			interpolate = (self.phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif self.phase < 21:
			interpolate = (self.phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (self.phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
			
		vel = np.copy(self.trajectory.qvel[self.phase*2*30])
		pose[0] *= self.speed
		vel[0] *= self.speed
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		pose = np.copy(self.trajectory.qpos[phase*2*30])
		vel = np.copy(self.trajectory.qvel[phase*2*30])
		pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter
		pose[1] = 0
		#pose[9] += 0.5
		#pose[23] += 0.5

		original_left_foot = np.copy(self.trajectory.qpos[16*2*30, 7:21])
		original_right_foot = np.copy(self.trajectory.qpos[7*2*30, 21:35])

		if phase < 7:
			interpolate = phase / 7.0
			pose[7:21] = original_left_foot * interpolate + (1-interpolate)*self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		elif phase < 14:
			interpolate = (phase - 6.0) / 7.0
			pose[21:35] = original_right_foot * (1 - interpolate) + interpolate*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		elif phase < 21:
			interpolate = (phase - 13) / 7.0
			pose[21:35] = original_right_foot * interpolate + (1 - interpolate)*self.higherfootdata[14:28]
			pose[7:21] = original_left_foot
		else:
			interpolate = (phase - 20) / 7.0
			pose[7:21] = original_left_foot * (1 - interpolate) + interpolate *self.higherfootdata[0:14]
			pose[21:35] = original_right_foot
		pose[0] *= self.speed
		vel[0] *= self.speed
		return pose, vel

	def reset(self):
		self.phase = random.randint(0, 27)
		self.speed = random.randint(0, 10) / 10.0
		self.time = 0
		self.counter = 0
		qpos, qvel = self.get_kin_state()
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		return self.get_state()

	def reset_for_normalization(self):
		return self.reset()

	def reset_for_test(self):
		return self.reset()


class cassieRLEnvCrouchStand(cassieRLEnvDelay):
	def __init__(self):
		super().__init__()
		self.crouching_gait = np.array([-2.026061164079969e-06, -1.0074387105244617e-06, 0.7067320535788225, 0.9999571927726693, -0.0026085787569872926, -0.00887738355211205, 5.834211252965799e-07, -0.0013879084036283866, 0.05107822304228246, 1.017030291014589, 0.7650923595855996, -0.0051079374606224776, 0.07880162651624044, -0.6390601645674943, -2.1929618912521502, 0.004479528550847322, 2.396598857512702, 0.0013344411210153473, -1.535654049818323, 1.5172271643280384, -1.6433895653965314, 0.10699094728669176, -0.026012740304504723, 1.0186751625550925, 0.7650781737126235, -0.004692712589926546, -0.07061483025803576, -0.640036649188727, -2.1929834715328895, 0.004441801371880326, 2.396671497185963, 0.0013384000219548516, -1.5355597192682269, 1.5171329121988897, -1.6432956018245928])
		self.standing_gait = np.copy(self.trajectory.qpos[16*2*30])
		self.buffer_size = 120

	def step(self, action):
		for _ in range(60):
			self.step_simulation(action)

		height = self.sim.qpos()[2]
		self.time += 1
		self.phase += 1

		if self.phase >= 56:
			self.phase = 0
			self.counter +=1
		done = not(height > 0.4 and height < 3.0) or self.time >= self.time_limit

		reward = self.compute_reward()
		if reward < 0.3:
			done = True

		return self.get_state(), reward, done, {}

	def get_kin_state(self):
		if self.phase < 27:
			interpolate = self.phase / 27.0
		else:
			interpolate = 1 - (self.phase-28)/27.0
		vel = np.zeros(32)
		pose = self.crouching_gait * interpolate + self.standing_gait * (1 - interpolate)
		pose[0] = 0
		pose[1] = 0
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 56:
			phase = 0
		if phase < 27:
			interpolate = phase / 27.0
		else:
			interpolate = 1 - (phase-27)/27.0
		pose = self.crouching_gait * interpolate + self.standing_gait * (1 - interpolate)
		pose[0] = 0
		pose[1] = 0
		vel = np.zeros(32)
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		return pose, vel

class cassieRLEnvCrouchToStand(cassieRLEnvCrouchStand):
	def __init__(self):
		super().__init__()

	def step(self, action):
		for _ in range(60):
			self.step_simulation(action)

		height = self.sim.qpos()[2]
		self.time += 1
		self.phase += 1

		if self.phase >= 28:
			self.phase = 0
			self.counter +=1
		done = not(height > 0.4 and height < 3.0) or self.time >= self.time_limit

		reward = self.compute_reward()
		if reward < 0.3:
			done = True

		return self.get_state(), reward, done, {}

	def get_kin_state(self):
		if self.phase < 28 and self.counter == 0:
			interpolate = self.phase / 27.0
		else:
			interpolate = 1
		vel = np.zeros(32)
		pose = self.standing_gait * interpolate + self.crouching_gait * (1 - interpolate)
		pose[0] = 0
		pose[1] = 0
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		return pose, vel

	def get_kin_next_state(self):
		phase = self.phase + 1
		if phase >= 28:
			phase = 0
		if phase < 28 and self.counter == 0:
			interpolate = phase / 27.0
		else:
			interpolate = 1
		pose = self.standing_gait * interpolate + self.crouching_gait * (1 - interpolate)
		pose[0] = 0
		pose[1] = 0
		vel = np.zeros(32)
		pose[21:35] = np.copy(pose[7:21])
		pose[26] = -pose[26]
		return pose, vel


	