from cassiemujoco_ctypes import *
import math
from quaternion_function import *
from cassieRLEnvMirror import *
from cassieRLEnvMultiTraj import *
from cassieRLEnvMirrorIKTraj import *
from model import *
import pickle

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

class cassieRLEnvCleanMotor(cassieRLEnvMirrorIKTraj):
	def __init__(self):
		super().__init__()
		self.buffer_size = 10
		self.motor_velocity_last = np.zeros(10)
		self.motor_acceleration_sq = np.zeros(10)
		self.motor_acc_tol = 700

	def step_simulation(self, action):
		super().step_simulation(action)
		self.motor_acceleration_sq += ((np.array(self.cassie_state.motor.velocity[:]) - self.motor_velocity_last) / 5e-4)**2
		self.motor_velocity_last = np.array(self.cassie_state.motor.velocity[:])
		#print(self.cassie_state.pelvis.translationalVelocity[:])
		#print(self.cassie_state.motor.position[:])

	def reset(self):
		self.motor_velocity_last = np.zeros(10)
		self.motor_acceleration_sq = np.zeros(10)
		return super().reset()

	def reset_for_test(self):
		self.motor_velocity_last = np.zeros(10)
		self.motor_acceleration_sq = np.zeros(10)
		return super().reset_for_test()

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		#print(ref_vel[0])
		yaw = quaternion2euler(self.sim.qpos()[3:7])
		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(self.sim.qpos()[6])**2
		orientation_vel_penalty = (self.sim.qvel()[3])**2+(self.sim.qvel()[4])**2+(self.sim.qvel()[5])**2
		orientation_acc_penalty = (0.1*self.sim.qacc()[3])**2+(0.1*self.sim.qacc()[4])**2+(0.1*self.sim.qacc()[5])**2
		vel_penalty = (self.sim.qpos()[1]-ref_pos[1])**2+(self.sim.qvel()[2])**2+(self.sim.qpos()[0]-ref_pos[0])**2

		#print(self.sim.qvel()[0])
		
		rms_motor_acceleration = np.sum(np.sqrt(self.motor_acceleration_sq / 60))
		#print(self.motor_acceleration_sq)
		self.motor_acceleration_sq = np.zeros(10)
		rms_motor_acceleration = max(self.motor_acc_tol, rms_motor_acceleration)
		if rms_motor_acceleration <= self.motor_acc_tol + 1:
			penalty = orientation_vel_penalty
		else:
			penalty = max(rms_motor_acceleration-self.motor_acc_tol, 0)/1000

		total_reward = 0.5*np.exp(-penalty) + 0.5*np.exp(-1*vel_penalty)
		#print(rms_motor_acceleration, 0.5*np.exp(-penalty))
		#print(0.5*np.exp(-rms_motor_acceleration/2000), 0.5*np.exp(-1*vel_penalty))

		return total_reward