from cassiemujoco import *
from cassiemujoco_ctypes import *
from cassieRLEnvMultiSkill import *
from cassieRLEnvMirror import *
import time
import numpy as np
import torch
import pickle
from model import ActorCriticNet, Shared_obs_stats
import platform
import quaternion_function


reorder_motor_matrix = np.zeros((10, 10))
reorder_motor_matrix[0:5, 5:10] = np.identity(5)
reorder_motor_matrix[5:10, 0:5] = np.identity(5)
reorder_motor_matrix[0, 5] = -1
reorder_motor_matrix[1, 6] = -1
reorder_motor_matrix[5, 0] = -1
reorder_motor_matrix[6, 1] = -1

reorder_joint_matrix = np.zeros((6, 6))
reorder_joint_matrix[0:3, 3:6] = np.identity(3)
reorder_joint_matrix[3:6, 0:3] = np.identity(3)

reorder_position_matrix = np.identity(3)
reorder_position_matrix[1, 1] = -1

reorder_orientation_matrix = np.identity(3) * -1
reorder_orientation_matrix[1, 1] = 1


def get_mirror_state(state, quaternion):
	#pelvis_euler = reorder_orientation_matrix.dot(quaternion2euler(np.copy(state.pelvis.orientation[:])))
	#pelvis_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
	iquaternion = inverse_quaternion(quaternion)
	new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
	if new_orientation[0] < 0:
		new_orientation = -new_orientation
	pelvis_euler = reorder_orientation_matrix.dot(quaternion2euler(new_orientation))
	new_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
	#print(new_orientation)

	translationalVelocity = reorder_position_matrix.dot(state.pelvis.translationalVelocity[:])
	translationalAcceleration = reorder_position_matrix.dot(state.pelvis.translationalAcceleration[:])
	rotationalVelocity = reorder_orientation_matrix.dot(state.pelvis.rotationalVelocity[:])
	motor_position = reorder_motor_matrix.dot(state.motor.position[:])
	motor_velocity = reorder_motor_matrix.dot(state.motor.velocity[:])
	joint_position = reorder_joint_matrix.dot(state.joint.position[:])
	joint_velocity = reorder_joint_matrix.dot(state.joint.velocity[:])

	new_translationalVelocity = rotate_by_quaternion(translationalVelocity, quaternion)

	print('quaternion: {}, new_orientation: {}'.format(quaternion, new_orientation))

	cassie_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], motor_position[:], new_translationalVelocity[:], rotationalVelocity[:], motor_velocity[:], translationalAcceleration[:], joint_position, joint_velocity]))
	return cassie_state


# Prevent latency issues by disabling multithreading in pytorch
torch.set_num_threads(1)

# Prepare model
env = cassieRLEnvMirror()
env.phase = 0
env.counter = 0
num_inputs = env.observation_space.shape[0]
num_outputs = env.action_space.shape[0]
model = ActorCriticNet(num_inputs, num_outputs, [256, 256])
model.load_state_dict(torch.load("torch_model/MirrorNov09.pt"))

with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
	shared_obs_stats = pickle.load(input)

# Initialize control structure with gains
P = np.array([100, 100, 88, 96, 50, 100, 100, 88, 96, 50])
D = np.array([10.0, 10.0, 8.0, 9.6, 5.0, 10.0, 10.0, 8.0, 9.6, 5.0])
u = pd_in_t()
for i in range(5):
	u.leftLeg.motorPd.pGain[i] = P[i]
	u.leftLeg.motorPd.dGain[i] = D[i]
	u.rightLeg.motorPd.pGain[i] = P[i+5]
	u.rightLeg.motorPd.dGain[i] = D[i+5]

pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])
pos_mirror_index = np.array([2,3,4,5,6,21,22,23,28,29,30,34,7,8,9,14,15,16,20])
vel_mirror_index = np.array([0,1,2,3,4,5,19,20,21,25,26,27,31,6,7,8,12,13,14,18])

# Determine whether running in simulation or on the robot
if platform.node() == 'cassie':
	cassie = CassieUdp(remote_addr='10.10.10.3', remote_port='25010',
					   local_addr='10.10.10.100', local_port='25011')
else:
	cassie = CassieUdp() # local testing

# Connect to the simulator or robot
print('Connecting...')
y = None
while y is None:
	cassie.send_pd(pd_in_t())
	time.sleep(0.001)
	y = cassie.recv_newest_pd()
received_data = True
print('Connected!\n')

# Record time
t = time.monotonic()

while True:
	# Wait until next cycle time
	while time.monotonic() - t < 60/2000:
		time.sleep(0.001)
	t = time.monotonic()

	# Get newest state
	state = cassie.recv_newest_pd()
	if state is None:
		print('Missed a cycle')
		continue

	env.orientation -= state.radio.channel[3] / 60.0
	env.speed = max(0, state.radio.channel[0])
	print('orientation: {}, speed: {}'.format(env.orientation, env.speed))

	if env.phase < 14:
		quaternion = euler2quat(z=env.orientation, y=0, x=0)
		iquaternion = inverse_quaternion(quaternion)
		new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
		if new_orientation[0] < 0:
			new_orientation = -new_orientation
		new_translationalVelocity = rotate_by_quaternion(state.pelvis.translationalVelocity[:], iquaternion)

		print('quaternion: {}, new_orientation: {}'.format(quaternion, new_orientation))

		# Construct input vector
		cassie_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], state.motor.position[:], new_translationalVelocity[:], state.pelvis.rotationalVelocity[:], state.motor.velocity[:], state.pelvis.translationalAcceleration[:], state.joint.position[:], state.joint.velocity[:]]))
		ref_pos, ref_vel = env.get_kin_next_state()
		RL_state = np.concatenate([cassie_state, ref_pos[pos_index], ref_vel[vel_index]])
	else:
		quaternion = euler2quat(z=env.orientation, y=0, x=0)
		cassie_state = get_mirror_state(state, quaternion)
		ref_pos, ref_vel = env.get_kin_next_state()
		ref_vel[1] = -ref_vel[1]
		euler = quaternion2euler(ref_pos[3:7])
		euler[0] = -euler[0]
		euler[2] = -euler[2]
		ref_pos[3:7] = euler2quat(z=euler[2],y=euler[1],x=euler[0])
		RL_state = np.concatenate([cassie_state, ref_pos[pos_mirror_index], ref_vel[vel_mirror_index]])

	# Construct input vector
	RL_state = torch.Tensor(RL_state).unsqueeze(0)
	RL_state = shared_obs_stats.normalize(RL_state)

	# Get action
	mu, log_std, v = model(RL_state)
	env_action = mu.data.squeeze().numpy()
	if env.phase >= 14:
		env_action = reorder_motor_matrix.dot(env_action)
	ref_action = env.ref_action()
	target = ref_action + env_action

	# Send action
	for i in range(5):
		u.leftLeg.motorPd.pTarget[i] = target[i]
		u.rightLeg.motorPd.pTarget[i] = target[i+5]
	cassie.send_pd(u)

	# Measure delay
	print('delay: {:6.1f} ms'.format((time.monotonic() - t) * 1000))

	# Track phase
	env.phase += 1
	if env.phase >= 28:
		env.phase = 0
		env.counter += 1
