from cassiemujoco_ctypes import *
import math
import pickle
from quaternion_function import *
from cassieRLEnvMirrorWithTransition import *
from cassieRLEnvMultiDirection import *
from model import ActorCriticNet, Shared_obs_stats, ActorCriticNetWithPhase
import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

class cassieRLEnvSideStepping(cassieRLEnvMultiDirection):
	def __init__(self):
		super().__init__()
		self.based_env = cassieRLEnvMirrorWithTransition()
		self.based_policy = ActorCriticNet(85, 10, [256, 256])
		self.based_policy.load_state_dict(torch.load("expert_model/StablePelvisWithransitionForwardBackwardMar20Size256X256.pt"))
		with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
			self.based_shared_obs_stats = pickle.load(input)
		self.action_space = np.zeros(2)
		self.reorder_motor_matrix = np.zeros((10, 10))
		self.reorder_motor_matrix[0:5, 5:10] = np.identity(5)
		self.reorder_motor_matrix[5:10, 0:5] = np.identity(5)
		self.reorder_motor_matrix[0, 5] = -1
		self.reorder_motor_matrix[1, 6] = -1
		self.reorder_motor_matrix[5, 0] = -1
		self.reorder_motor_matrix[6, 1] = -1

	def step(self, action):
		self.based_env.phase = self.phase
		self.based_env.cassie_state = self.cassie_state
		mirror_state = self.based_env.get_state()
		mirror_state[5] = 0
		mirror_state[10] = 0
		mirror_state[21] = 0.3
		mirror_state[26] = -0.3
		mirror_state[16] = 0#np.zeros(3)
		mirror_state = Variable(torch.Tensor(mirror_state).unsqueeze(0))
		mirror_state = self.based_shared_obs_stats.normalize(mirror_state)
		mirror_action, _, _ = self.based_policy(mirror_state)
		mirror_action = mirror_action.squeeze(0).data.numpy()
		if self.phase >= self.max_phase / 2:
			mirror_action = self.reorder_motor_matrix.dot(mirror_action)
		mirror_action[0] = action[0]
		mirror_action[5] = action[1]
		return super().step(mirror_action)

	def reset(self):
		state = super().reset()
		self.based_env.speed = self.speed
		self.based_env.y_speed = 0
		return state

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		yaw = quaternion2euler(self.sim.qpos()[3:7])
		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(self.sim.qpos()[6])**2
		orientation_vel_penalty = (self.sim.qvel()[3])**2+(self.sim.qvel()[4])**2+(self.sim.qvel()[5])**2
		orientation_acc_penalty = (0.1*self.sim.qacc()[3])**2+(0.1*self.sim.qacc()[4])**2+(0.1*self.sim.qacc()[5])**2
		vel_penalty = (self.sim.qpos()[1]-ref_pos[1])**2+(self.sim.qvel()[2])**2+(self.sim.qpos()[0]-ref_pos[0])**2

		pos = np.zeros(6)
		pos = self.sim.foot_pos(pos)
		foot_height = max(pos[2], pos[5])
		foot_penalty = 0
		if (self.phase <= 5  and self.phase >= 2) or (self.phase <= 19 and self.phase >= 16):
			desire_height = (2 - abs((self.phase % 14) - 3.5)) * 0.05 + 0.075
			#print(desire_height)
			foot_penalty = (min(foot_height - desire_height, 0))**2 * 400
		#print("orientation vel", self.sim.qvel()[3:6], "vel", self.sim.qvel()[0:3])
		#print(0.5*np.exp(-1*orientation_vel_penalty), 0.5*np.exp(-1*vel_penalty))
		return 0.5*np.exp(-1*orientation_vel_penalty)+0.4*np.exp(-1*vel_penalty)+0.1*np.exp(-foot_penalty)

	def get_mirror_state(self, state, quaternion):
		pos_mirror_index = np.array([2,3,4,5,6,21,22,23,28,29,30,34,7,8,9,14,15,16,20])
		vel_mirror_index = np.array([0,1,2,3,4,5,19,20,21,25,26,27,31,6,7,8,12,13,14,18])
		reorder_motor_matrix = np.zeros((10, 10))
		reorder_motor_matrix[0:5, 5:10] = np.identity(5)
		reorder_motor_matrix[5:10, 0:5] = np.identity(5)
		reorder_motor_matrix[0, 5] = -1
		reorder_motor_matrix[1, 6] = -1
		reorder_motor_matrix[5, 0] = -1
		reorder_motor_matrix[6, 1] = -1

		reorder_joint_matrix = np.zeros((6, 6))
		reorder_joint_matrix[0:3, 3:6] = np.identity(3)
		reorder_joint_matrix[3:6, 0:3] = np.identity(3)

		reorder_position_matrix = np.identity(3)
		reorder_position_matrix[1, 1] = -1

		reorder_orientation_matrix = np.identity(3) * -1
		reorder_orientation_matrix[1, 1] = 1

		#pelvis_euler = reorder_orientation_matrix.dot(quaternion2euler(np.copy(state.pelvis.orientation[:])))
		#pelvis_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
		iquaternion = inverse_quaternion(quaternion)
		new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
		if new_orientation[0] < 0:
			new_orientation = -new_orientation
		pelvis_euler = reorder_orientation_matrix.dot(quaternion2euler(new_orientation))
		new_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
		#print(new_orientation)

		translationalVelocity = reorder_position_matrix.dot(state.pelvis.translationalVelocity[:])
		translationalAcceleration = reorder_position_matrix.dot(state.pelvis.translationalAcceleration[:])
		rotationalVelocity = reorder_orientation_matrix.dot(state.pelvis.rotationalVelocity[:])
		motor_position = reorder_motor_matrix.dot(state.motor.position[:])
		motor_velocity = reorder_motor_matrix.dot(state.motor.velocity[:])
		joint_position = reorder_joint_matrix.dot(state.joint.position[:])
		joint_velocity = reorder_joint_matrix.dot(state.joint.velocity[:])

		left_toe_force = reorder_position_matrix.dot(state.rightFoot.toeForce[:])
		left_heel_force = reorder_position_matrix.dot(state.rightFoot.heelForce[:])
		right_toe_force = reorder_position_matrix.dot(state.leftFoot.toeForce[:])
		right_heel_force = reorder_position_matrix.dot(state.leftFoot.heelForce[:])

		new_translationalVelocity = rotate_by_quaternion(translationalVelocity, quaternion)

		cassie_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], motor_position[:], new_translationalVelocity[:], rotationalVelocity[:], motor_velocity[:], translationalAcceleration[:], left_toe_force, left_heel_force, right_toe_force, right_heel_force]))
		ref_pos, ref_vel = self.get_kin_next_state()
		ref_vel[1] = -ref_vel[1]
		euler = quaternion2euler(ref_pos[3:7])
		euler[0] = -euler[0]
		euler[2] = -euler[2]
		ref_pos[3:7] = euler2quat(z=euler[2],y=euler[1],x=euler[0])
		RL_state = np.concatenate([cassie_state, ref_pos[pos_mirror_index], ref_vel[vel_mirror_index]])
		return RL_state