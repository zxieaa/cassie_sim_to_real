from cassieRLEnvMultiSkill import *
from cassiemujoco_ctypes import *
import math
from quaternion_function import *

class cassieRLEnvHorizon(cassieRLEnvMultiSkill):
	def __init__(self):
		super().__init__()
		self.speed = 0
		self.horizon_limit = 28
		self.horizon = 0
		self.observation_space = np.zeros(85+39*self.horizon_limit)
		self.ref_state = None
	def step_simulation(self, action):
		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]
		ref_pos, ref_vel = self.get_kin_state(horizon=self.horizon+1)
		target = action + ref_pos[pos_index]
		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]
		self.state_buffer.append(self.sim.step_pd(self.u))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)
		self.cassie_state = self.state_buffer[len(self.state_buffer) - 1]
	def step(self, action):
		for _ in range(60):
			self.step_simulation(action)
		height = self.sim.qpos()[2]
		self.time += 1
		self.horizon += 1
		if self.horizon >= self.horizon_limit:
			self.horizon = 0
			self.counter +=1
			self.ref_state = self.get_kin_state()
		done = not(height > 0.4 and height < 3.0) or self.time >= self.time_limit
		reward = self.compute_reward()
		if reward < 0.3:
			done = True
		return self.get_state(), reward, done, {}
	def get_state(self):
		state = self.cassie_state
		if self.horizon >= self.horizon_limit:
			self.horizon = 0
		quaternion = euler2quat(z=self.orientation, y=0, x=0)
		iquaternion = inverse_quaternion(quaternion)
		new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
		new_translationalVelocity = rotate_by_quaternion(state.pelvis.translationalVelocity[:], iquaternion)
		new_translationalAcceleration = rotate_by_quaternion(state.pelvis.translationalAcceleration[:], iquaternion)
		new_rotationalVelocity = rotate_by_quaternion(state.pelvis.rotationalVelocity[:], quaternion)
		useful_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], state.motor.position[:], new_translationalVelocity[:], state.pelvis.rotationalVelocity[:], state.motor.velocity[:], new_translationalAcceleration[:], state.leftFoot.toeForce[:], state.leftFoot.heelForce[:], state.rightFoot.toeForce[:], state.rightFoot.heelForce[:]]))
		return np.concatenate([np.copy(self.ref_state[39*self.horizon:39*(self.horizon+1)]), useful_state, np.copy(self.ref_state)])
	def get_kin_state(self, horizon=None):
		if horizon is None:
			pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
			vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])
			result = None
			self.speed = 1.0
			for i in range(int(self.horizon_limit / 2)):
				pose = np.copy(self.trajectory.qpos[i*2*30])
				pose[0] *= self.speed
				pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
				pose[1] = 0
				vel = np.copy(self.trajectory.qvel[i*2*30])
				vel[0] *= self.speed
				if result is None:
					result = np.concatenate([pose[pos_index], vel[vel_index]])
				else:
					result = np.concatenate([result, pose[pos_index], vel[vel_index]])
			self.speed += random.randint(-5, 5) / 10
			if self.speed < 0:
				self.speed = 0
			elif self.speed > 1.0:
				self.speed = 1.0
			self.speed = 1.0
			for i in range(int(self.horizon_limit / 2), self.horizon_limit):
				pose = np.copy(self.trajectory.qpos[i*2*30])
				pose[0] *= self.speed
				pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
				pose[1] = 0
				vel = np.copy(self.trajectory.qvel[i*2*30])
				vel[0] *= self.speed
				if result is None:
					result = np.concatenate([pose[pos_index], vel[vel_index]])
				else:
					result = np.concatenate([result, pose[pos_index], vel[vel_index]])
			return result
		else:
			if horizon >= self.horizon_limit:
				horizon = 0
			pose = np.copy(self.trajectory.qpos[horizon*2*30])
			pose[0] *= self.speed
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel = np.copy(self.trajectory.qvel[horizon*2*30])
			vel[0] *= self.speed
			return pose, vel
	def reset(self):
		self.orientation = 0
		self.speed = 1.0#random.randint(0, 4) / 4
		self.horizon = random.randint(0, 27)
		self.ref_state = self.get_kin_state()
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state(horizon=self.horizon)
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		state = self.get_state()
		return state
	def reset_for_normalization(self):
		return self.reset()
	def reset_for_test(self):
		return self.reset()
	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state(horizon=self.horizon)
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0
		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]
		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = self.cassie_state.pelvis.position[:]
		com_penalty = (pelvis_pos[0]-ref_pos[0])**2 + (pelvis_pos[1])**2 + (pelvis_pos[2]-ref_pos[2])**2
		yaw = quat2yaw(self.sim.qpos()[3:7])
		orientation_penalty = (self.sim.qpos()[4]*20)**2+(self.sim.qpos()[5]*20)**2+(yaw - self.orientation)**2
		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000
		speed_penalty = (self.sim.qvel()[0] - self.speed)**2 + (self.sim.qvel()[1] - 0)**2
		#print(self.speed)
		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.1*np.exp(-orientation_penalty)+0.1*np.exp(-spring_penalty)
		return total_reward