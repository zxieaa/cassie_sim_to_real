from cassiemujoco_ctypes import *
import math
import pickle
from quaternion_function import *
from cassieRLEnvMultiTraj import *

class cassieRLEnvStanding(cassieRLEnvMultiTraj):
	def __init__(self):
		super().__init__()
		self.standing_pose = np.array([0, 0, 1.05, 1, 0, 0, 0, 0.0045, 0, 0.4973, 0.9785, -0.0164, 0.01787, -0.2049,
         -1.1997, 0, 1.4267, 0, -1.5244, 1.5244, -1.5968,
         -0.0045, 0, 0.4973, 0.9786, 0.00386, -0.01524, -0.2051,
         -1.1997, 0, 1.4267, 0, -1.5244, 1.5244, -1.5968])
		self.perturb_counter = 0
		self.perturb_force = random.randint(-100, 100) / 10
		self.max_perturb_time = 400
		self.control_rate = 30
	def get_kin_state(self):
		pos = np.copy(self.standing_pose)
		vel = np.zeros(32)
		return pos, vel
	def get_kin_next_state(self):
		return self.get_kin_state()
	
	def step_simulation(self, action):
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())

		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		action[[0, 1, 5, 6]] *= 0.1
		target = action + ref_pos[pos_index]

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]

		self.state_buffer.append(self.sim.step_pd(self.u))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)

		self.cassie_state = self.state_buffer[len(self.state_buffer) - 1]

		if self.perturb_counter > self.max_perturb_time:
			if abs(self.perturb_force) > 0:
				self.perturb_force = 0
				self.max_perturb_time = 2000
			else:
				self.perturb_force = random.randint(-1, 1) * 10
				self.max_perturb_time = 400
			self.perturb_counter = 0
		else:
			self.perturb_counter += 1
		
		self.sim.apply_force(np.array([self.perturb_force,0,0,0,0,0]))

	def compute_reward(self):
		pelvis_pos = self.sim.qpos()
		ref_pos, ref_vel = self.get_kin_state()
		com_penalty = (pelvis_pos[0])**2 + (pelvis_pos[1])**2 + (pelvis_pos[2]-ref_pos[2])**2
		orientation_penalty = (self.sim.qpos()[4]*20)**2+(self.sim.qpos()[5]*20)**2+(self.sim.qpos()[6]*20)**2
		return 0.5*np.exp(-com_penalty) + 0.5*np.exp(-orientation_penalty)