import pickle
import numpy as np
import matplotlib.pyplot as plt

with open ('trajectory/pelvis_angular_vel_Jan', 'rb') as fp:
	angular_vel = pickle.load(fp)

with open ('trajectory/pelvis_angular_vel_Jan_noisy', 'rb') as fp:
	angular_vel_noisy = pickle.load(fp)

angular_vel_norm = np.zeros((5*1682))
angular_vel_norm_noisy = np.zeros((5*1682))
for i in range(5*1682):
	angular_vel_norm[i] = np.sqrt(angular_vel[i][0]**2 + angular_vel[i][1]**2 + angular_vel[i][2]**2)
	angular_vel_norm_noisy[i] = np.sqrt(angular_vel_noisy[i][0]**2 + angular_vel_noisy[i][1]**2 + angular_vel_noisy[i][2]**2)


plt.xlabel('time(ms)')
plt.ylabel('rad/s')
plt.plot(angular_vel_norm_noisy[::2], 'b', label='before optimization')
plt.plot(angular_vel_norm[::2], 'g', label='after optimization')
plt.legend(loc='upper right')
plt.show()