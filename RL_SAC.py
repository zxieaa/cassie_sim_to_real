from RL_Mirror_Supervised import *
from model import *
import pybullet_envs
from cassieRLEnvMirrorPhase import *

class RL_SAC(RL):
	def __init__(self, env, hidden_layer=[64, 64]):
		super().__init__(env, hidden_layer=hidden_layer)
		self.env = env
		self.num_inputs = env.observation_space.shape[0]
		self.num_outputs = env.action_space.shape[0]
		self.hidden_layer = hidden_layer
		self.params = Params()
		self.actor = ActorNet(self.num_inputs, self.num_outputs,self.hidden_layer)
		self.critic = ValueNet(self.num_inputs, self.hidden_layer)
		self.q_function = QNet(self.num_inputs, self.num_outputs,self.hidden_layer)
		self.q_function_target = QNet(self.num_inputs, self.num_outputs,self.hidden_layer)
		self.q_function_target.load_state_dict(self.q_function.state_dict())
		self.q_function.share_memory()
		self.critic.share_memory()
		self.actor.share_memory()
		self.q_function_target.share_memory()
		self.shared_obs_stats = Shared_obs_stats(self.num_inputs)
		self.memory = ReplayMemory(self.params.num_steps * 10000)
		self.test_mean = []
		self.test_std = []
		self.lr = 3e-4
		plt.show(block=False)
		self.test_list = []
		
		#for multiprocessing queue
		self.queue = mp.Queue()
		self.process = []
		self.traffic_light = TrafficLight()
		self.counter = Counter()

		self.off_policy_memory = ReplayMemory(300000)

		self.q_function_optimizer = optim.Adam(self.q_function.parameters(), lr=self.lr, weight_decay=0e-3)
		self.actor_optimizer = optim.Adam(self.actor.parameters(), lr=self.lr, weight_decay=0e-3)
		self.actor.train()
		self.q_function.train()

		self.q_fucntion_scheduler = optim.lr_scheduler.ExponentialLR(self.q_function_optimizer, gamma=0.99)
		self.actor_scheduler = optim.lr_scheduler.ExponentialLR(self.actor_optimizer, gamma=0.99)

		self.actor_target = ActorNet(self.num_inputs, self.num_outputs, self.hidden_layer)
		self.actor_target.load_state_dict(self.actor.state_dict())

		self.off_policy_queue = mp.Queue()

		self.log_alpha = torch.zeros(1, requires_grad=True)
		self.alpha_optim = optim.Adam([self.log_alpha], lr=0.03)
		self.alpha = self.log_alpha.exp()
		self.reward_scale = 0.01
		#self.actor_target.share_memory()

	def run_test(self, num_test=1):
		state = self.env.reset()#_for_test()
		state = Variable(torch.Tensor(state).unsqueeze(0))
		ave_test_reward = 0

		total_rewards = []
		actor_old = ActorNet(self.num_inputs, self.num_outputs, self.hidden_layer)
		actor_old.load_state_dict(self.actor.state_dict())
		
		for i in range(num_test):
			total_reward = 0
			while True:
				start = t.time()
				state = self.shared_obs_stats.normalize(state)
				action, _, mean, log_std = actor_old.sample(state)
				print("log std", log_std)
				env_action = mean.data.squeeze().numpy()
				state, reward, done, _ = self.env.step(env_action)
				total_reward += reward
				#self.env.vis.draw(self.env.sim)
				#print(state)
				#print("done", done, "state", state)
				'''self.env.render()
				while True:
					stop = t.time()
					if stop - start > 0.01:
						break'''

				if done:
					state = self.env.reset()#_for_test()
					#print(self.env.position)
					#print(self.env.time)
					state = Variable(torch.Tensor(state).unsqueeze(0))
					ave_test_reward += total_reward / num_test
					total_rewards.append(total_reward)
					break
				state = Variable(torch.Tensor(state).unsqueeze(0))

		reward_mean = statistics.mean(total_rewards)
		reward_std = statistics.stdev(total_rewards)
		self.test_mean.append(reward_mean)
		self.test_std.append(reward_std)
		self.test_list.append((reward_mean, reward_std))

	def plot_statistics(self): 
		ax = self.fig.add_subplot(111)
		low = []
		high = []
		index = []
		for i in range(len(self.test_mean)):
			low.append(self.test_mean[i] - self.test_std[i])
			high.append(self.test_mean[i] + self.test_std[i])
			index.append(i)
		#ax.set_xlim([0,1000])
		#ax.set_ylim([0,300])
		plt.xlabel('iterations')
		plt.ylabel('average rewards')
		ax.plot(self.test_mean, 'b')
		ax.fill_between(index, low, high, color='cyan')
		#ax.plot(map(sub, test_mean, test_std))
		self.fig.canvas.draw()

	def collect_samples(self, num_samples, noise=-2.0, random_seed=1):
		#random seed is used to make sure different thread generate different trajectories
		random.seed(random_seed)
		torch.manual_seed(random_seed+1)
		np.random.seed(random_seed+2)
		start_state = self.env.reset()
		samples = 0
		done = False
		states = []
		next_states = []
		actions = []
		rewards = []
		q_values = []
		dones = []
		self.actor.set_noise(noise)
		state = start_state
		state = Variable(torch.Tensor(state).unsqueeze(0))
		total_reward = 0
		start = t.time()
		while True:
			actor_old = ActorNet(self.num_inputs, self.num_outputs, self.hidden_layer)
			actor_old.load_state_dict(self.actor.state_dict())
			signal_init = self.traffic_light.get()
			#while samples < num_samples and not done:
			state = self.shared_obs_stats.normalize(state)
			states.append(state.data.numpy())
			if self.traffic_light.explore.value == False:# and random.randint(0,90)%100 > 0:
				action, _, mean, _ = self.actor.sample(state)
				action.detach()
				mean.detach()
			else:
				action = torch.randint(-100, 100, (1,self.num_outputs))/100.0

			actions.append(action.data.numpy())
			env_action = action.data.squeeze().numpy()

			# mirror_state = self.env.get_mirror_state()
			# mirror_state = Variable(torch.Tensor(mirror_state).unsqueeze(0))
			# mirror_state = self.shared_obs_stats.normalize(mirror_state)
			# states.append(mirror_state.data.numpy())
			# mirror_action = self.env.get_mirror_action(action.data.numpy())
			# actions.append(mirror_action)

			state, reward, done, _ = self.env.step(env_action)
			#print(samples)
			total_reward += reward
			rewards.append(Variable(reward * torch.ones(1, 1)).data.numpy())
			state = Variable(torch.Tensor(state).unsqueeze(0))
			next_state = self.shared_obs_stats.normalize(state)
			next_states.append(next_state.data.numpy())
			dones.append(Variable((1 - done) * torch.ones(1, 1)).data.numpy())
			samples += 1
			done = (done or samples > num_samples)

			# rewards.append(Variable(reward * torch.ones(1, 1)).data.numpy())
			# next_mirror_state = self.env.get_mirror_state()
			# next_mirror_state = Variable(torch.Tensor(next_mirror_state).unsqueeze(0))
			# next_mirror_state = self.shared_obs_stats.normalize(next_mirror_state)
			# next_states.append(next_mirror_state.data.numpy())
			self.queue.put([states, actions, next_states, rewards, dones])
			#self.off_policy_queue.put([states, actions, next_states, rewards, dones])
			self.counter.increment()
			#print("waiting sim time passed", t.time() - start)
			start = t.time()
			while self.traffic_light.get() == signal_init:
				pass
			#print("waiting memory collectd time passed", t.time() - start)
			start = t.time()
			if done:
				print(total_reward)
				state = self.env.reset()
				state = Variable(torch.Tensor(state).unsqueeze(0))
				samples = 0
				total_reward = 0
				#self.update_actor_target(1)
			done = False
			states = []
			next_states = []
			actions = []
			rewards = []
			values = []
			q_values = []
			dones = []

	def update_critic(self, batch_size, num_epoch):
		self.model.train()
		optimizer = optim.Adam(self.critic.parameters(), lr=self.lr*10)
		critic_old = ValueNet(self.num_inputs, self.hidden_layer)
		critic_old.load_state_dict(self.critic.state_dict())
		for k in range(num_epoch):
			batch_states, batch_actions, batch_next_states, _, _, batch_q_values = self.memory.sample(batch_size)
			batch_states = Variable(torch.Tensor(batch_states))
			batch_q_values = Variable(torch.Tensor(batch_q_values))
			batch_next_states = Variable(torch.Tensor(batch_next_states))
			v_pred_next = critic_old(batch_next_states)
			v_pred = self.critic(batch_states)
			loss_value = (v_pred - batch_q_values)**2
			loss_value = 0.5*torch.mean(loss_value)
			optimizer.zero_grad()
			loss_value.backward(retain_graph=True)
			optimizer.step()
			av_value_loss = loss_value.data[0]

	def update_q_function(self, batch_size, num_epoch, update_actor=False):
		#self.q_function.train()
		#self.q_function_optimizer = optim.Adam(self.q_function.parameters(), lr=self.lr)
		#self.actor_optimizer = optim.Adam(self.actor.parameters(), lr=self.lr)
		#self.alpha_optim = optim.Adam([self.log_alpha], lr=self.lr)
		for k in range(num_epoch):
			batch_states, batch_actions, batch_next_states, batch_rewards, batch_dones = self.off_policy_memory.sample(batch_size)
			batch_states2, batch_actions2, batch_next_states2, batch_rewards2, batch_dones2 = self.memory.sample(10)

			batch_states = Variable(torch.Tensor(batch_states))
			batch_next_states = Variable(torch.Tensor(batch_next_states))
			batch_actions = Variable(torch.Tensor(batch_actions))
			batch_rewards = Variable(torch.Tensor(batch_rewards * self.reward_scale))
			batch_dones = Variable(torch.Tensor(batch_dones))

			batch_states2 = Variable(torch.Tensor(batch_states2))
			batch_next_states2 = Variable(torch.Tensor(batch_next_states2))
			batch_actions2 = Variable(torch.Tensor(batch_actions2))
			batch_rewards2 = Variable(torch.Tensor(batch_rewards2 * self.reward_scale))
			batch_dones2 = Variable(torch.Tensor(batch_dones2))

			batch_states = torch.cat([batch_states, batch_states2], 0)
			batch_next_states = torch.cat([batch_next_states, batch_next_states2], 0)
			batch_actions = torch.cat([batch_actions, batch_actions2], 0)
			batch_rewards = torch.cat([batch_rewards, batch_rewards2], 0)
			batch_dones = torch.cat([batch_dones, batch_dones2], 0)

			#compute on policy actions for next state
			batch_next_state_action, batch_next_log_prob,  batch_next_state_action_mean, _, = self.actor.sample(batch_next_states)
			#compute q value for these actions
			q_next_1_target, q_next_2_target = self.q_function_target(batch_next_states, batch_next_state_action)
			#q_next_1, q_next_2 = q_function_old(batch_next_states, batch_next_state_action)
			#q_next_1 = temperture * q_next_1_target + (1 - temperture) * q_next_1
			#q_next_2 = temperture * q_next_2_target + (1 - temperture) * q_next_2
			q = torch.min(q_next_1_target - 0.0 * batch_next_log_prob, q_next_2_target - 0.0 * batch_next_log_prob)
			#q = self.critic(batch_next_states)
			#value functions estimate of the batch_states
			value = batch_rewards + batch_dones * self.params.gamma * q
			

			#q value estimate
			q1, q2 = self.q_function(batch_states, batch_actions)
			q1_value_loss = F.mse_loss(q1, value)
			q2_value_loss = F.mse_loss(q2, value)
			q_value_loss = q1_value_loss + q2_value_loss

			self.q_function_optimizer.zero_grad()
			q_value_loss.backward()
			self.q_function_optimizer.step()

			if update_actor is False:
				continue

			new_action, log_prob, mean_action, log_std = self.actor.sample(batch_states)
			q1_new, q2_new = self.q_function(batch_states, new_action)
			new_q_value = torch.min(q1_new, q2_new)# - self.critic(batch_states)
			policy_loss = (-new_q_value + 0.0 * log_prob).mean() + 0.01 *(mean_action**2).mean()
			#print("log_prob", log_prob.shape, new_q_value.shape)
			
			self.actor_optimizer.zero_grad()
			policy_loss.backward()
			self.actor_optimizer.step()

			#alpha loss
			self.alpha = self.log_alpha.exp()
			alpha_loss = -(self.alpha * (log_prob - 3).detach()).mean()
			self.alpha_optim.zero_grad()
			alpha_loss.backward()
			self.alpha_optim.step()
			self.alpha = self.log_alpha.exp()
			#self.alpha = 0
			
			#print("alpha", self.alpha, "log prob", (log_prob).mean().data.numpy())
			print(k, "q value loss", q1_value_loss, q2_value_loss)
			#print("log_prob", log_std)
		#self.q_function_target.load_state_dict(q_function_old.state_dict())

	def update_actor(self, batch_size, num_epoch):
		#actor_old = ActorNet(self.num_inputs, self.num_outputs, self.hidden_layer)
		#actor_old.load_state_dict(self.actor.state_dict())
		#actor_old.set_noise(self.actor.noise)
		#self.actor.train()
		for k in range(num_epoch):
			batch_states, batch_actions, batch_next_states, batch_rewards, batch_dones = self.off_policy_memory.sample(batch_size)
			batch_states2, batch_actions2, batch_next_states2, batch_rewards2, batch_dones2 = self.memory.sample(10)

			batch_states = Variable(torch.Tensor(batch_states))
			batch_next_states = Variable(torch.Tensor(batch_next_states))
			batch_actions = Variable(torch.Tensor(batch_actions))
			batch_rewards = Variable(torch.Tensor(batch_rewards))
			batch_dones = Variable(torch.Tensor(batch_dones))

			batch_states2 = Variable(torch.Tensor(batch_states2))
			batch_next_states2 = Variable(torch.Tensor(batch_next_states2))
			batch_actions2 = Variable(torch.Tensor(batch_actions2))
			batch_rewards2 = Variable(torch.Tensor(batch_rewards2))
			batch_dones2 = Variable(torch.Tensor(batch_dones2))

			batch_states = torch.cat([batch_states, batch_states2], 0)
			batch_next_states = torch.cat([batch_next_states, batch_next_states2], 0)
			batch_actions = torch.cat([batch_actions, batch_actions2], 0)
			batch_rewards = torch.cat([batch_rewards, batch_rewards2], 0)
			batch_dones = torch.cat([batch_dones, batch_dones2], 0)

			new_action, log_prob, mean, log_std = self.actor.sample(batch_states)
			q1_new, q2_new = self.q_function(batch_states, mean)
			new_q_value = torch.min(q1_new, q2_new)# - self.critic(batch_states)
			policy_loss = (-new_q_value).mean()

			#print(k, "policy loss", policy_loss)
			
			self.actor_optimizer.zero_grad()
			policy_loss.backward(retain_graph=True)
			self.actor_optimizer.step()
		if self.lr > 1e-4:
			self.lr *= 0.99

	def update_q_target(self, tau):
		for target_param, param in zip(self.q_function_target.parameters(), self.q_function.parameters()):
			#print(target_param.data)
			target_param.data.copy_(target_param.data * (1.0 - tau) + param.data * tau)
	def update_actor_target(self, tau):
		for target_param, param in zip(self.actor_target.parameters(), self.actor.parameters()):
			target_param.data.copy_(target_param.data * (1.0 - tau) + param.data * tau)
	def save_actor(self, filename):
		torch.save(self.actor.state_dict(), filename)

	def collect_samples_multithread(self):
		#queue = Queue.Queue()
		num_threads = 10
		self.lr = 3e-4
		self.traffic_light.explore.value = False
		seeds = [
			np.random.randint(0, 4294967296) for _ in range(num_threads)
		]

		ts = [
			mp.Process(target=self.collect_samples,args=(400,), kwargs={'noise':-2.0, 'random_seed':seed})
			for seed in seeds
		]
		for t in ts:
			t.start()
			
		'''t1.start()
		t2.start()
		t3.start()
		t4.start()
		t5.start()
		t6.start()
		t7.start()
		t8.start()
		t9.start()
		t10.start()'''
		self.model.set_noise(-2.0)
		for iter in range(1000000):
			while len(self.memory.memory) < 10:
				if self.counter.get() == num_threads:
					'''self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
			
					self.memory.push(self.queue.get())
					self.memory.push(self.queue.get())'''
					for i in range(num_threads):
						#self.off_policy_memory.push(self.off_policy_queue.get())
						self.memory.push(self.queue.get())
					self.counter.increment()
				if len(self.memory.memory) < 10 and self.counter.get() == num_threads + 1:
					self.counter.reset()
					self.traffic_light.switch()
			#print(len(self.memory.memory))
			#self.off_policy_memory.memory = self.off_policy_memory.memory + self.memory.memory
			off_policy_memory_len = len(self.off_policy_memory.memory)
			#print(off_policy_memory_len)
			memory_len = len(self.memory.memory)
			#self.update_critic(128, 640 * int(memory_len/3000))
			if off_policy_memory_len >= 256:
				if off_policy_memory_len > 10000:
					self.traffic_light.explore.value = False
				#self.update_critic(128, 64)
				temperture = 1
				for i in range(1):
					self.update_q_function(256, 1)
					self.update_q_function(256, 1, update_actor=True)
					self.update_q_target(0.005)
					#self.update_actor(256, 1)
					#temperture *= 0.5
				'''if iter % 1000 == 0:
					self.update_q_target(1)
					self.update_actor_target(1)'''
				if iter % 900 == 0:
					self.run_test(num_test=2)
					self.plot_statistics()
					self.save_actor("torch_model/nonMirrorSAC.pt")
					self.actor_scheduler.step()
					self.q_fucntion_scheduler.step()
					#if self.reward_scale > 0.01:
					#	self.reward_scale *= 0.95
					#if self.actor.noise > -1.0:
					#	self.actor.noise *= 1.01
			self.off_policy_memory.memory = self.off_policy_memory.memory + self.memory.memory
			self.clear_memory()
			self.off_policy_memory.clean_memory()
			#start = t.time()
			#print("waiting memory collectd time passed", t.time() - start)
			self.traffic_light.switch()
			self.counter.reset()

if __name__ == '__main__':
	torch.set_num_threads(1)
	env = cassieRLEnvMirrorPhase()
	env.delay = False
	env.noisy = False
	sac = RL_SAC(env, [256, 256])
	with open('torch_model/cassie3dNonMirrorPhase_2kHz_shared_obs_stats.pkl', 'rb') as input:
		sac.shared_obs_stats = pickle.load(input)
	#sac.normalize_data(num_iter=10000)
	#sac.save_shared_obs_stas('torch_model/humanoid_shared_obs_stats.pkl')
	sac.collect_samples_multithread()

	start = t.time()

	noise = -2.0