from cassiemujoco_ctypes import *
import math
from quaternion_function import *
from cassieRLEnvMirrorWithTransition import *
from cassieRLEnvMultiTraj import *
from cassieRLEnvMultiDirection import *
from cassieRLEnvMirrorIKTraj import *
from cassieRLEnvMirrorBackward import *
from cassieRLEnvIKBackward import *
from model import *
import pickle

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

class cassieRLEnvStablePelvis(cassieRLEnvIKBackward):
	def __init__(self):
		super().__init__()
		'''self.based_controller = ActorCriticNet(self.observation_space.shape[0], self.action_space.shape[0],[256, 256])
		self.based_controller.load_state_dict(torch.load("torch_model/MirrorDec05.pt"))
		with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
			self.based_shared_obs_stats = pickle.load(input)'''
		self.buffer_size = 10
		
	def step(self, action):
		'''current_state = self.get_state()
		current_state = Variable(torch.Tensor(current_state).unsqueeze(0))
		current_state = self.based_shared_obs_stats.normalize(current_state)
		based_action, _, based_v = self.based_controller(current_state) 
		based_action = based_action.data.squeeze().numpy()'''
		#print(based_action, action)
		for i in range(self.control_rate):
			self.step_simulation(1*action)
		#print("phase", self.phase)
		#print("action", based_action+action)

		height = self.sim.qpos()[2]
		#print("height", height)
		self.time += 1
		self.phase += 1

		if self.phase >= self.max_phase:
			self.phase = 0
			self.counter +=1
		#print("height", height)

		done = not(height > 0.4 and height < 100.0) or self.time >= self.time_limit
		#print("base value", based_v)
		#if based_v < 60:
		#	done = True
		#	print("value low")
		yaw = quat2yaw(self.sim.qpos()[3:7])
		#print(yaw - self.orientation)
		#if (yaw-self.orientation)**2 > 0.15:
		#	done = True

		reward = self.compute_reward(action)
		#print("action", action)
		#print(reward)
		#if reward < 0.3:
		#	done = True

		return self.get_state(), reward, done, {}
	def compute_reward(self, action=None):
		ref_pos, ref_vel = self.get_kin_state()
		yaw = quaternion2euler(self.sim.qpos()[3:7])
		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(self.sim.qpos()[6])**2
		orientation_vel_penalty = (self.sim.qvel()[3])**2+(self.sim.qvel()[4])**2+(self.sim.qvel()[5])**2
		orientation_acc_penalty = (0.1*self.sim.qacc()[3])**2+(0.1*self.sim.qacc()[4])**2+(0.1*self.sim.qacc()[5])**2
		#vel_penalty = (self.sim.qvel()[1]-ref_vel[1])**2+(self.sim.qvel()[2])**2+(self.sim.qvel()[0]-ref_vel[0])**2
		vel_penalty = (self.sim.qvel()[1]-ref_vel[1])**2 + (self.sim.qvel()[0]-ref_vel[0])**2 + (self.sim.qpos()[2]-1.05)**2 + (self.sim.qvel()[2])**2
		pos_penalty = (self.sim.qpos()[1]-ref_pos[1])**2 + (self.sim.qpos()[0]-ref_pos[0])**2 + (self.sim.qpos()[2]-1.05)**2 + (self.sim.qvel()[2])**2
		pos = np.zeros(6)
		pos = self.sim.foot_pos(pos)
		foot_height = max(pos[2], pos[5])
		foot_penalty = 0
		if (self.phase <= 5  and self.phase >= 2) or (self.phase <= 19 and self.phase >= 16):
			desire_height = (2 - abs((self.phase % 14) - 3.5)) * 0.05 + 0.075
			#print(desire_height)
			foot_penalty = (min(foot_height - desire_height, 0))**2 * 400
		#return 0.5*np.exp(-1*orientation_vel_penalty)+0.5*np.exp(-10*vel_penalty)

		foot_forces = self.get_foot_forces()
		force_penalty = 0
		#print(foot_forces)
		'''if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			#print(foot_forces[2])
			force_penalty += (max(foot_forces[2]-100, 0))**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			#print(foot_forces[8])
			force_penalty += (max(foot_forces[8]-100, 0))**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]'''
		#print(foot_forces)
		#if force_penalty > 0:
		#	print(foot_forces)
		#	print(np.exp(-force_penalty))
		toe_torque_penalty = self.toe_torque
		self.toe_torque = 0
		#print(np.exp(-toe_torque_penalty/60), toe_torque_penalty, 0.2*np.exp(-1*orientation_vel_penalty) + 0.1 * np.exp(-10*orientation_penalty) + 0.4*np.exp(-10*vel_penalty)+0.3*np.exp(-toe_torque_penalty/60))
		#return 0.2*np.exp(-1*orientation_vel_penalty) + 0.1 * np.exp(-10*orientation_penalty) + 0.4*np.exp(-10*vel_penalty)+0.3*np.exp(-toe_torque_penalty/60)

		force_penalty = (max(self.foot_impact_penalty, 0))**2
		#print(0.5 * np.exp(-force_penalty/10))
		#print(np.exp(-force_penalty/5))
		#print(np.exp(-force_penalty/10000))
		#print(0.5*np.exp(-5*orientation_vel_penalty), 0.4*np.exp(-10*pos_penalty), 0.1*np.exp(-foot_penalty))
		self.foot_impact_penalty = 0
		return 0.4*np.exp(-1*orientation_vel_penalty)+0.4*np.exp(-10*vel_penalty) + 0.1*np.exp(-foot_penalty) + 0.1 * np.exp(-force_penalty/10)
		#print(0.4*np.exp(-1*orientation_vel_penalty), 0.1 * np.exp(-10*orientation_penalty), 0.4*np.exp(-10*vel_penalty), 0.1*np.exp(-foot_penalty))
		#return 0.5 * (0.4*np.exp(-5*orientation_vel_penalty) + 0.0 * np.exp(-10*orientation_penalty) + 0.4*np.exp(-10*pos_penalty)+0.1*np.exp(-foot_penalty)) + 0.5 * np.exp(-force_penalty/10)

		'''if self.foot_impact_penalty < 300:
			#print(ref_pos[0], self.sim.qpos()[0], pos_penalty)
			return np.exp(-pos_penalty) * 0.5 + 0.4*np.exp(-1*orientation_vel_penalty) + 0.1 * np.exp(-10*orientation_penalty)
			#return 0.4*np.exp(-1*orientation_vel_penalty) + 0.1 * np.exp(-10*orientation_penalty) + 0.4*np.exp(-10*vel_penalty)+0.1*np.exp(-foot_penalty)
		else:
			#print(np.exp(-force_penalty/10000), self.foot_forces)
			force_penalty = (max(self.foot_impact_penalty-400, 0))**2
			#print(np.exp(-force_penalty/10000/5))
			self.foot_impact_penalty = 0
			return np.exp(-force_penalty/10000/5) #* 0.5 + 0.5*np.exp(-10*vel_penalty)'''
		#return 1.0/4*np.exp(-orientation_penalty)+1.0/4*np.exp(-orientation_acc_penalty)+1.0/4*np.exp(-vel_penalty)+1.0/4*np.exp(-orientation_vel_penalty)

	'''def step_simulation(self, action, debug=False):
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())

		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		if self.phase < 28:
			target = action + ref_pos[pos_index]
		else:
			mirror_action = np.zeros(10)
			mirror_action[0:5] = np.copy(action[5:10])
			mirror_action[5:10] = np.copy(action[0:5])
			mirror_action[0] = -mirror_action[0]
			mirror_action[1] = -mirror_action[1]
			mirror_action[5] = -mirror_action[5]
			mirror_action[6] = -mirror_action[6]
			target = mirror_action + ref_pos[pos_index]
		#if debug:
		#	print(qpos[7])
		#control = self.P*(target-self.sim.data.qpos[index])-self.D*self.sim.data.qvel[vel_index]
		#print(ref_pos[pos_index])

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]

		self.state_buffer.append(self.sim.step_pd(self.u))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)

		self.cassie_state = self.state_buffer[len(self.state_buffer) - 1]
		#print(self.cassie_state.motor.torque[:])'''