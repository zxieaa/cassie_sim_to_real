from cassiemujoco import *
from cassieRLEnv import *
from cassieRLEnvInvariant import *
from cassieRLEnvWithMoreState import *
from cassieRLEnvAccelPenalty import *
from cassieRLEnvSimpleJump import *
from cassieRLEnvMultiSkill import *
from cassieRLEnvHorizon import *
from cassieRLEnvMultiDirection import *
from cassieRLEnvMultiTraj import *
from cassieRLEnvMirror import *
from cassieRLEnvTerrain import *
from cassieRLEnvStablePelvis import * 
from cassieRLEnvNoRef import *
from cassieRLEnvSlopeTerrain import *
from cassieRLEnvHigherFoot import *
from cassieRLEnvCleanMotor import *
import time as t

import statistics

import argparse
import os
import sys
import gym
from gym import wrappers
import random
import numpy as np

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

from params import Params

import pickle
from model import ActorCriticNet, Shared_obs_stats, ActorCriticNetWithMultiRobot

env = cassieRLEnvMirror()
env2 = cassieRLEnvMirror()

u = pd_in_t()
u.leftLeg.motorPd.torque[3] = 0 # Feedforward torque
u.leftLeg.motorPd.pTarget[3] = -2
u.leftLeg.motorPd.pGain[3] = 1000
u.leftLeg.motorPd.dTarget[3] = -2
u.leftLeg.motorPd.dGain[3] = 100
u.rightLeg.motorPd = u.leftLeg.motorPd

num_inputs = env.observation_space.shape[0]
num_outputs = env.action_space.shape[0]

model = ActorCriticNetWithMultiRobot(num_inputs, num_outputs, [256, 256], num_robots=3)
model.load_state_dict(torch.load("torch_model/MultiRobotMar7.pt"))

#model = ActorCriticNet(num_inputs, num_outputs, [64, 64])
#model.load_state_dict(torch.load("expert_model/StablePelvis64X64Jan12.pt"))

expert_model = ActorCriticNet(num_inputs, num_outputs,[256, 256])
expert_model.load_state_dict(torch.load(("torch_model/MirrorNov11.pt")))
residual_model = ActorCriticNet(num_inputs, num_outputs, [256, 256])
residual_model.load_state_dict(torch.load("torch_model/StablePelvisNov14_v2.pt"))
#model.load_state_dict(torch.load("expert_model/MultiTrajBackwardExpertDec03.pt"))

with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
	shared_obs_stats = pickle.load(input)
#with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
#	shared_obs_stats = pickle.load(input)

print(model.robot_policy_input_modules[0].weight.data-model.robot_policy_input_modules[1].weight.data)
#print(model.robot_value_input_modules[1].weight.data)

state_list = []
print(env.D)
env.visualize = True
env2.visualize = True

def run_test():
	t.sleep(1)
	#env.scale = 2
	#env.sim.model.hfield_size[0][2] = 0.4
	state = env.reset()
	#print(env.speed)
	#env.speed = 0
	#env.speed = 0.0
	total_reward = 0
	done = False
	total_10_reward = 0
	current_scale = 0
	clock = 0
	done = False
	current_phase = 0
	reward_list = []

	for i in range(1):
		state = env.reset_by_robot(0)
		state2 = env2.reset_by_robot(1)
		total_reward = 0
		counter = 0
		interpolate = 1
		while counter < 100000:
			start = t.time()
			#print(env.speed)
			if counter % 100 == 50:
				force = (random.randint(-50, 80))
				print(force)
				env.sim.apply_force(np.array([0,0,0,0,0,0]))
			elif counter % 100 == 57:
				env.sim.apply_force(np.zeros(6)*100)
			for j in range(1):
				counter += 1
				clock += 1
				#print(state - state2)
				state = Variable(torch.Tensor(state).unsqueeze(0))
				state = shared_obs_stats.normalize(state)
				state2 = Variable(torch.Tensor(state2).unsqueeze(0))
				state2 = shared_obs_stats.normalize(state2)
				mu, log_std, v = model.compute_robot_action(state, robot_id=0)
				mu_other, _, _ = model.compute_robot_action(state2, robot_id=1)

				x_policy, x_value = model.one_step_inference(state, robot_id=0)
				x_policy2, x_value2 = model.one_step_inference(state, robot_id=1)
				#print(x_policy)
				#print(x_policy2)
				#print(v)
				print("action diff", mu-mu_other)
				#print("robot2 action", mu_other)
				eps = torch.randn(mu.size())
				mu_expert, _, _ = expert_model(state)
				mu_residual, _, _ = residual_model(state)

				mu = (mu + 0.0*1*Variable(eps))
				#print(mu.data)

				env_action = mu.data.squeeze().numpy() * 1
				env_action2 = mu_other.data.squeeze().numpy()
				state, reward, done, _ = env.step(env_action)
				state2, reward, done, _ = env2.step(env_action2)
				done = True
				#print(state)
				#print(reward)
				#print(state.shape)
				#env.sim.get_foot_forces()
				#t.sleep(1)
				env.vis.draw(env.sim)
				env2.vis.draw(env2.sim)
				#print(reward)
				total_reward += reward
				force = np.zeros(12)
				pos = np.zeros(6)
				#print(env.phase, env.sim.foot_pos(pos))
				#t.sleep(1)
				#env.get_foot_height()
			while True:
				stop = t.time()
				#print(stop-start)
				if stop - start > 0.03 * env.control_rate / 60:
					break
				#print("stop")
			#total_reward += reward
		done = False
		counter = 0
		reward_list.append(total_reward)
		total_10_reward += total_reward
		print("total rewards", total_reward)
	print(total_10_reward)
	print(statistics.mean(reward_list))
	print(statistics.stdev(reward_list))

def play_kin():
	env.vis.draw(env.sim)
	#env.set_data()
	env.phase = 0
	#env.speed = -0.0
	env.reset()
	env.phase = 0
	t.sleep(1)
	while True:
		print(env.phase)
		start = t.time()
		while True:
			stop = t.time()
			#print(stop-start)
			#print("stop")
			if stop - start > 1.33:
				break
		pos, vel = env.get_kin_next_state()
		env.phase += 1
		#print(env.speed)
		if env.phase >= env.max_phase:
			env.phase = 0
			env.counter += 1
			#break
		env.set_state(pos, vel)
		y = env.sim.step_pd(u)
		env.vis.draw(env.sim)
#play_kin()
run_test()