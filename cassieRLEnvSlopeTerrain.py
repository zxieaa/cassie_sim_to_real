from cassiemujoco_ctypes import *
import math
from quaternion_function import *
from cassieRLEnvMirror import *
import matplotlib.pyplot as plt

class cassieRLEnvSlopeTerrain(cassieRLEnvMirror):
	def __init__(self):
		super().__init__()
		self.speed = 0
		self.y_speed = 1
		self.observation_space = np.zeros(86)
		self.height_field = np.zeros(512)
		self.slope = np.pi * 0.1
		self.generate_terrain()
		self.visualize = False
		self.data = np.zeros(512*512)


	def step(self, action):
		for _ in range(60):
			self.step_simulation(action)

		height = self.cassie_state.pelvis.position[2] - self.cassie_state.terrain.height
		#print("height, ", height)
		self.time += 1
		self.phase += 1

		if self.phase >= 28:
			self.phase = 0
			self.counter +=1
		#print("height", height)

		done = not(height > 0.2 and height < 100.0) or self.time >= self.time_limit
		if self.sim.qpos()[0] < -0.5:
			done = True

		euler = quaternion2euler(self.sim.qpos()[3:7])
		if abs(euler[1]) > 0.5:
			done = True
		#print(yaw - self.orientation)
		#if (yaw-self.orientation)**2 > 0.15:
		#	done = True

		reward = self.compute_reward()
		#print("reward", reward)
		if reward < 0.3:
			done = True

		return self.get_state(), reward, done, {}

	def generate_terrain(self):
		height_field_size = np.zeros(4)
		for i in range(4):
			height_field_size[i] = self.sim.height_field_size()[i]
		if self.slope >= 0:
			height_field_size[2] = 25.6 * np.tan(self.slope)
			self.sim.set_height_field_size(height_field_size)
			self.data = np.zeros(512*512)
			for i in range(512):
				for j in range(512):
					if j == 0:
						self.data[i*512 + j] = float(0)
					else:
						self.data[i*512 + j] = float(np.tan(self.slope)*0.05+self.data[i*512 + j-1])
			if height_field_size[2] > 0.001:
				self.data /= height_field_size[2]
		else:
			height_field_size[2] = 25.6 * np.tan(-self.slope)
			self.sim.set_height_field_size(height_field_size)
			self.data = np.zeros(512*512)
			for i in range(512):
				for j in range(512):
					if j == 0:
						self.data[i*512 + j] = height_field_size[2]
					else:
						self.data[i*512 + j] = float(np.tan(self.slope)*0.05+self.data[i*512 + j-1])
			self.data /= height_field_size[2]
		#print(np.max(self.data))
		self.sim.set_height_field_data(self.data)

	def get_state(self):
		state = super().get_state()
		return np.concatenate([state, np.ones(1)*self.slope])


	def reset(self):
		self.orientation = 0
		self.speed = 1
		self.y_speed = 0
		orientation = self.orientation #+ random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = 0#random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		

		#slope related update
		self.slope = np.pi / 180.0 * 30#random.randint(0, 3) * 10
		qpos[2] = 25.6 * abs(np.tan(self.slope)) / 2.0 - 7.4 + 1.05
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		self.height_field = np.zeros(512)
		self.generate_terrain()
		if self.visualize:
			cassie_vis_free(self.vis.v)
			self.vis.v = cassie_vis_init(self.sim.c)
		return self.get_state()

	def reset_by_slope(self, slope):
		self.orientation = 0
		self.speed = 1
		self.y_speed = 0
		orientation = self.orientation #+ random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = 0#random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		
		#slope related update
		self.slope = np.pi / 180.0 * slope
		qpos[2] = 25.6 * abs(np.tan(self.slope)) / 2.0 - 7.4 + 1.05
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		self.height_field = np.zeros(512)
		self.generate_terrain()
		if self.visualize:
			cassie_vis_free(self.vis.v)
			self.vis.v = cassie_vis_init(self.sim.c)
		return self.get_state()

	def reset_for_test(self):
		return self.reset_by_slope(random.randint(0, 3) * 10)

		return np.concatenate([useful_state, ref_pos[pos_index], ref_vel[vel_index]])
	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0
		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = np.copy(self.cassie_state.pelvis.position[:])
		ref_pos[0] = ref_pos[0] * np.cos(self.slope)
		com_penalty = (pelvis_pos[0]-ref_pos[0])**2 + (pelvis_pos[1]-ref_pos[1])**2 + (self.sim.qvel()[2])**2
		#print("ref", ref_pos[2])
		#print(ref_vel[1])

		yaw = quat2yaw(self.sim.qpos()[3:7])

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2

		spring_penalty = (self.sim.qpos()[15])**2+(self.sim.qpos()[29])**2
		spring_penalty *= 1000

		speed_penalty = (self.sim.qvel()[0] - ref_vel[0])**2 + (self.sim.qvel()[1] - ref_vel[1])**2

		foot_forces = self.get_foot_forces()
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-150)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-150)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]
		#print(force_penalty/1000)
		#print(0.5*np.exp(-joint_penalty), 0.3*np.exp(-com_penalty), 0.2*np.exp(-10*orientation_penalty))
		#print(pelvis_pos[0], ref_pos[0])
		total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.2*np.exp(-10*orientation_penalty)#+0.1*np.exp(-spring_penalty)

		return total_reward

