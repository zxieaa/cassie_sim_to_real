import pickle
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d

with open ('trajectory/base_run1', 'rb') as fp:
	state = pickle.load(fp)

clen = 1680
steps = 20

motor_pos = np.zeros((steps*clen, 10))
motor_vel = np.zeros((steps*clen, 10))

for i in range(steps*clen):
	for j in range(10):
		motor_pos[i][j] = state[i][0][j]
		motor_vel[i][j] = state[i][1][j]

motor_pos = gaussian_filter1d(motor_pos.T, sigma=40, axis=1)
motor_vel = gaussian_filter1d(motor_vel.T, sigma=40, axis=1)

qr = qdotr = ql = qdotl = 0

for t in range(0, steps):

	maskr = abs(motor_vel[3][t*clen:(t+1)*clen]) > 0
	maskl = abs(motor_vel[8][t*clen:(t+1)*clen]) > 0

	qr += motor_pos[3][t*clen:(t+1)*clen][maskr] / steps
	qdotr += motor_vel[3][t*clen:(t+1)*clen][maskr] / steps
	ql += motor_pos[8][t*clen:(t+1)*clen][maskl] / steps 
	qdotl += motor_vel[8][t*clen:(t+1)*clen][maskl] / steps

plt.plot(qr, qdotr, 'ro', markersize=1)
plt.plot(ql, qdotl, 'go', markersize=1)
# plt.plot(qr[:10], qdotr[:10], 'ro', markersize=10)
# plt.plot(ql[:10], qdotl[:10], 'go', markersize=10)
# plt.plot(qr[-10:], qdotr[-10:], 'yo', markersize=10)
# plt.plot(ql[-10:], qdotl[-10:], 'bo', markersize=10)
plt.show()