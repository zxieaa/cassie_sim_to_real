from cassiemujoco_ctypes import *
import math
import pickle
from quaternion_function import *
from cassieRLEnvMirror import *

class cassieRLEnvMirrorWithTransition(cassieRLEnvMirror):
	def __init__(self):
		super().__init__()

	def reset(self):
		self.orientation = 0
		self.speed = 1#random.randint(0, 10) / 10.0#random.randint(-5, 5) / 10.0
		#print(self.speed)
		#print("init speed", self.speed)
		self.y_speed = 0
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		#qpos[7] = 0.2
		#qpos[21] = -0.2
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		#self.speed = random.randint(-5, 10) / 10.0
		#self.sim.perturb_body_mass(bodyid=1, percentage=-0.9)
		return self.get_state()


	def reset_by_speed(self, speed, y_speed=0, phase=None):
		self.orientation = 0
		self.speed = speed#(random.randint(-5, 10)) / 10.0
		self.y_speed = 0
		orientation = self.orientation +  random.randint(-10, 10) * np.pi / 100#(random.randint(0, 1) * 2 - 1) * np.pi / 10
		quaternion = euler2quat(z=orientation, y=0, x=0)
		if phase is None:
			self.phase = random.randint(0, 27)
		else:
			self.phase = phase
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		self.speed = speed
		self.y_speed = y_speed
		return self.get_state()

	def reset_for_test(self):
		self.orientation = 0
		self.speed = 1#random.randint(0, 10) / 10.0
		#print("init speed", self.speed)
		self.y_speed = 0
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		#qpos[7] = 0.2
		#qpos[21] = -0.2
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		#self.speed = random.randint(-5, 10) / 10.0
		#self.sim.perturb_body_mass(bodyid=1, percentage=-0.9)
		return self.get_state()

	def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0
		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = np.copy(self.cassie_state.pelvis.position[:])
		pelvis_vel = np.copy(self.cassie_state.pelvis.translationalVelocity[:])
		com_penalty = (pelvis_vel[0]-ref_vel[0])**2 + (pelvis_vel[1]-ref_vel[1])**2 + (self.sim.qvel()[2])**2
		#com_penalty = (pelvis_vel[0]-ref_vel[0])**2 + (pelvis_vel[1]-ref_vel[1])**2 + (self.sim.qpos()[2]-1.05)**2
		#print(self.sim.qpos()[2])
		#print("ref", ref_pos[2])
		#print(ref_vel[1])

		yaw = quat2yaw(self.sim.qpos()[3:7])

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2
		#print(self.sim.qvel()[3])

		foot_forces = self.get_foot_forces()
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-150)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-150)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]
		#print(force_penalty/1000)
		#print(0.5*np.exp(-joint_penalty), 0.3*np.exp(-com_penalty), 0.2*np.exp(-10*orientation_penalty))
		#print(pelvis_pos[0], ref_pos[0])
		pos = np.zeros(6)
		pos = self.sim.foot_pos(pos)
		foot_height = max(pos[2], pos[5])
		foot_penalty = 0
		if (self.phase <= 5  and self.phase >= 2) or (self.phase <= 19 and self.phase >= 16):
			desire_height = (2 - abs((self.phase % 14) - 3.5)) * 0.05 + 0.075
			#print(desire_height)
			foot_penalty = (min(foot_height - desire_height, 0))**2 * 400

		#total_reward = 0.4*np.exp(-joint_penalty)+0.3*np.exp(-10*com_penalty)+0.2*np.exp(-10*orientation_penalty)+0.1*np.exp(-foot_penalty)#tracking reward
		#print(ref_vel[0])
		total_reward = np.exp(-30 * (ref_pos[joint_index[3]]-self.sim.qpos()[joint_index[3]])**2 - 30 *(ref_pos[joint_index[8]]-self.sim.qpos()[joint_index[8]])**2) * 0.25 + 0.25*np.exp(-com_penalty) + 0.25*np.exp(-10*orientation_penalty) + 0.25 * np.exp(-30 * (ref_pos[joint_index[2]]-self.sim.qpos()[joint_index[2]])**2 - 30 *(ref_pos[joint_index[7]]-self.sim.qpos()[joint_index[7]])**2)

		return total_reward