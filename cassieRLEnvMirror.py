from cassiemujoco_ctypes import *
import math
import pickle
from quaternion_function import *
from cassieRLEnvMultiTraj import *

class cassieRLEnvMirror(cassieRLEnvMultiTraj):
	def __init__(self):
		super().__init__()
		self.record_state = False
		self.recorded = False
		self.recorded_state = []
		self.max_phase = 28
		self.control_rate = 60
		self.time_limit = 400
		self.recorded_torque = []
		self.recorded_target = []
		self.recorded_motor_pos = []
		self.recorded_motor_vel = []
		self.recorded_ref = []
		self.recorded_angular_vel = []
		#self.recorded_swing_foot_pos = []
		self.recorded_pelvis_pos = []
		self.recorded_pelvis_vel = []
		self.recorded_left_foot_pos = []
		self.recorded_right_foot_pos = []
		self.recorded_stance_foot_pos = []

		self.P = np.array([100, 100, 88, 96, 50, 100, 100, 88, 96, 50])
		self.D = np.array([10.0, 10.0, 8.0, 9.6, 5.0, 10.0, 10.0, 8.0, 9.6, 5.0])

		self.mass_perturbations_list = [-0.4, 0, 0.4]
		self.robot_id = 0

		self.toe_torque = 0

		self.foot_impact_penalty = 0

		self.impact_time = 0

		self.swing_foot_pos = np.zeros(3)

		self.left_stance = True

		self.double_stance_time = 0

		self.control_rate = 60

		self.control_frequency = 60

	def step(self, action):
		for _ in range(self.control_frequency):
			self.step_simulation(action)

		height = self.sim.qpos()[2]
		#print("height", height)
		self.time += 1
		self.phase += 1

		if self.phase >= self.max_phase:
			self.phase = 0
			self.counter +=1
		#print("height", height)

		done = not(height > 0.4 and height < 100.0) or self.time >= self.time_limit
		yaw = quat2yaw(self.sim.qpos()[3:7])
		#print(yaw - self.orientation)
		#if (yaw-self.orientation)**2 > 0.15:
		#	done = True

		reward = self.compute_reward()
		#print(reward)
		if reward < 0.3:
			done = True

		return self.get_state(), reward, done, {}

	def get_mirror_state(self, state):
		reorder_motor_matrix = np.zeros((10, 10))
		reorder_motor_matrix[0:5, 5:10] = np.identity(5)
		reorder_motor_matrix[5:10, 0:5] = np.identity(5)
		reorder_motor_matrix[0, 5] = -1
		reorder_motor_matrix[1, 6] = -1
		reorder_motor_matrix[5, 0] = -1
		reorder_motor_matrix[6, 1] = -1

		reorder_joint_matrix = np.zeros((6, 6))
		reorder_joint_matrix[0:3, 3:6] = np.identity(3)
		reorder_joint_matrix[3:6, 0:3] = np.identity(3)

		reorder_position_matrix = np.identity(3)
		reorder_position_matrix[1, 1] = -1

		reorder_orientation_matrix = np.identity(3) * -1
		reorder_orientation_matrix[1, 1] = 1

		height = state[0:1]
		orientation = state[1:5]
		motor_position = state[5:15]
		translational_velocity = state[15:18]
		rotational_velocity = state[18:21]
		motor_velocity = state[21:31]
		translational_acceleration = state[31:34]
		joint_position = state[34:40]
		joint_velocity = state[40:46]

		translational_velocity = reorder_position_matrix.dot(translational_velocity)
		translational_acceleration = reorder_position_matrix.dot(translational_acceleration)
		pelvis_euler = reorder_orientation_matrix.dot(quaternion2euler(orientation))
		orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
		rotational_velocity = reorder_orientation_matrix.dot(rotational_velocity)
		motor_position = reorder_motor_matrix.dot(motor_position)
		motor_velocity = reorder_motor_matrix.dot(motor_velocity)
		joint_position = reorder_joint_matrix.dot(joint_position)
		joint_velocity = reorder_joint_matrix.dot(joint_velocity)
		return np.concatenate([height, orientation, motor_position, translational_velocity, rotational_velocity, motor_velocity, translational_acceleration, joint_position, joint_velocity])

	def get_mirror_action(self, action):
		reorder_motor_matrix = np.zeros((10, 10))
		reorder_motor_matrix[0:5, 5:10] = np.identity(5)
		reorder_motor_matrix[5:10, 0:5] = np.identity(5)
		reorder_motor_matrix[0, 5] = -1
		reorder_motor_matrix[1, 6] = -1
		reorder_motor_matrix[5, 0] = -1
		reorder_motor_matrix[6, 1] = -1
		mirror_action = reorder_motor_matrix.dot(action)
		return mirror_action

	def get_state(self):
		if len(self.state_buffer) > 0:
			random_index = random.randint(0, len(self.state_buffer)-1)
			state = self.state_buffer[random_index]
		else:
			state = self.cassie_state
		state = self.cassie_state
		ref_pos, ref_vel = np.copy(self.get_kin_next_state())
		#print(state.motor.position[0])
		#state = self.cassie_state
		#print(ref_pos[3:7])

		if self.phase < self.max_phase / 2:
			pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
			vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])
			quaternion = euler2quat(z=self.orientation, y=0, x=0)
			iquaternion = inverse_quaternion(quaternion)
			new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
			#print(new_orientation)
			new_translationalVelocity = rotate_by_quaternion(state.pelvis.translationalVelocity[:], iquaternion)
			#print(new_translationalVelocity)
			new_translationalAcceleration = rotate_by_quaternion(state.pelvis.translationalAcceleration[:], iquaternion)
			new_rotationalVelocity = rotate_by_quaternion(state.pelvis.rotationalVelocity[:], quaternion)
			useful_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], state.motor.position[:], new_translationalVelocity[:], state.pelvis.rotationalVelocity[:], state.motor.velocity[:], new_translationalAcceleration[:], state.joint.position[:], state.joint.velocity[:]]))
			return np.concatenate([useful_state, ref_pos[pos_index], ref_vel[vel_index]])
		else:
			pos_index = np.array([2,3,4,5,6,21,22,23,28,29,30,34,7,8,9,14,15,16,20])
			vel_index = np.array([0,1,2,3,4,5,19,20,21,25,26,27,31,6,7,8,12,13,14,18])
			ref_vel[1] = -ref_vel[1]
			euler = quaternion2euler(ref_pos[3:7])
			euler[0] = -euler[0]
			euler[2] = -euler[2]
			ref_pos[3:7] = euler2quat(z=euler[2],y=euler[1],x=euler[0])
			#print(ref_pos[3:7])
			quaternion = euler2quat(z=-self.orientation, y=0, x=0)
			iquaternion = inverse_quaternion(quaternion)

			pelvis_euler = quaternion2euler(np.copy(state.pelvis.orientation[:]))
			pelvis_euler[0] = -pelvis_euler[0]
			pelvis_euler[2] = -pelvis_euler[2]
			pelvis_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
			#print(pelvis_orientation, state.pelvis.orientation[:])

			translational_velocity = np.copy(state.pelvis.translationalVelocity[:])
			translational_velocity[1] = -translational_velocity[1]

			translational_acceleration = np.copy(state.pelvis.translationalAcceleration[:])
			translational_acceleration[1] = -translational_acceleration[1]

			rotational_velocity = np.copy(state.pelvis.rotationalVelocity)
			rotational_velocity[0] = -rotational_velocity[0]
			rotational_velocity[2] = -rotational_velocity[2]

			motor_position = np.zeros(10)
			motor_position[0:5] = np.copy(state.motor.position[5:10])
			motor_position[5:10] = np.copy(state.motor.position[0:5])
			motor_position[0] = -motor_position[0]
			motor_position[1] = -motor_position[1]
			motor_position[5] = -motor_position[5]
			motor_position[6] = -motor_position[6]

			motor_velocity = np.zeros(10)
			motor_velocity[0:5] = np.copy(state.motor.velocity[5:10])
			motor_velocity[5:10] = np.copy(state.motor.velocity[0:5])
			motor_velocity[0] = -motor_velocity[0]
			motor_velocity[1] = -motor_velocity[1]
			motor_velocity[5] = -motor_velocity[5]
			motor_velocity[6] = -motor_velocity[6]

			joint_position = np.zeros(6)
			joint_position[0:3] = np.copy(state.joint.position[3:6])
			joint_position[3:6] = np.copy(state.joint.position[0:3])

			joint_velocity = np.zeros(6)
			joint_velocity[0:3] = np.copy(state.joint.velocity[3:6])
			joint_velocity[3:6] = np.copy(state.joint.velocity[0:3])

			left_toeForce = np.copy(state.rightFoot.toeForce[:])
			left_toeForce[1] = -left_toeForce[1]
			left_heelForce = np.copy(state.rightFoot.heelForce[:])
			left_heelForce[1] = -left_heelForce[1]
			#print("phase", self.phase)
			#print("left", left_toeForce)

			right_toeForce = np.copy(state.leftFoot.toeForce[:])
			right_toeForce[1] = -right_toeForce[1]
			right_heelForce = np.copy(state.leftFoot.heelForce[:])
			right_heelForce[1] = -right_heelForce[1]
			#print("right", right_toeForce)
			
			new_orientation = quaternion_product(iquaternion, pelvis_orientation)
			new_translationalVelocity = rotate_by_quaternion(translational_velocity, iquaternion)
			new_translationalAcceleration = rotate_by_quaternion(translational_acceleration, iquaternion)
			new_rotationalVelocity = rotate_by_quaternion(rotational_velocity, quaternion)
			#print("vel", new_translationalVelocity)

			useful_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], motor_position, new_translationalVelocity[:], rotational_velocity, motor_velocity, new_translationalAcceleration[:], joint_position, joint_velocity]))
			#print("vel", useful_state[15]-new_translationalVelocity[0])
			return np.concatenate([useful_state, ref_pos[pos_index], ref_vel[vel_index]])
	def step_simulation(self, action):
		#self.sim.perturb_mass()
		#self.sim.get_mass()
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())

		'''if self.phase == 0 and self.time >= 100:
			self.record_state = True
		if self.record_state and len(self.recorded_state) < 1682:
			self.recorded_state.append((self.sim.qpos(), self.sim.qvel()))
		if len(self.recorded_state) == 1682 and not self.recorded:
			with open('trajectory/backward_trajectory_Nov', 'wb') as fp:
				pickle.dump(self.recorded_state, fp)
			self.recorded = True'''

		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		if self.phase < self.max_phase/2:
			target = action + ref_pos[pos_index]
		else:
			mirror_action = np.zeros(10)
			mirror_action[0:5] = np.copy(action[5:10])
			mirror_action[5:10] = np.copy(action[0:5])
			mirror_action[0] = -mirror_action[0]
			mirror_action[1] = -mirror_action[1]
			mirror_action[5] = -mirror_action[5]
			mirror_action[6] = -mirror_action[6]
			target = mirror_action + ref_pos[pos_index]
		#print(ref_pos[pos_index])
		#print(action)
		#control = self.P*(target-self.sim.data.qpos[index])-self.D*self.sim.data.qvel[vel_index]
		foot_forces = self.get_foot_forces()
		#self.foot_impact_penalty += (foot_forces[2] + foot_forces[8])/60.0
		foot_vel = np.zeros(6)
		foot_pos = np.zeros(6)
		self.sim.foot_pos(foot_pos)
		#print(self.sim.qpos()[0:3] - foot_pos[0:3])
		self.sim.foot_vel(foot_vel)
		self.foot_impact_penalty += ((foot_vel[2])**2 + (foot_vel[5])**2) / 60
		'''if foot_forces[2] > 400 and self.impact_time > 200:
			self.impact_time = 0
			self.left_stance = True
		elif foot_forces[8] > 400 and self.impact_time > 200:
			self.impact_time = 0
			self.left_stance = False
		if self.left_stance:
			stance_foot_pos = foot_pos[0:3]
		else:
			stance_foot_pos = foot_pos[3:6]
		if foot_forces[2] > 100 and foot_forces[8] > 100:
			self.double_stance_time += 1
		elif self.double_stance_time > 0:
			print("double_stance_time", self.double_stance_time)
			self.double_stance_time = 0
		self.impact_time += 1'''
		#print(min(foot_vel[2], foot_vel[5]))
		#print(foot_forces[2], foot_forces[8])
		'''if self.foot_forces[0] < 100 and foot_forces[2] > 100:
			#self.foot_forces[0] = foot_forces[2]
			self.foot_impact_penalty += foot_forces[2]
			print(min(foot_vel[2], foot_vel[5]))
			#print(self.phase, foot_forces[2])
			#print(self.phase, self.sim.qpos()[0:3] - foot_pos[3:6], self.sim.qvel()[0:3])
			#print("swing foot diff", foot_pos[0:3] - self.swing_foot_pos)
			self.swing_foot_pos = foot_pos[3:6]
		if self.foot_forces[1] < 100 and foot_forces[8] > 100:
			#self.foot_forces[1] = foot_forces[8]
			self.foot_impact_penalty += foot_forces[8]
			print(min(foot_vel[2], foot_vel[5]))
			#print(self.phase, foot_forces[8])
			#print(self.phase, self.sim.qpos()[0:3] - foot_pos[0:3], self.sim.qvel()[0:3])
			#print("swing foot diff", foot_pos[3:6] - self.swing_foot_pos)
			self.swing_foot_pos = foot_pos[0:3]
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]'''

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]

		self.state_buffer.append(self.sim.step_pd(self.u))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)
			#print(self.state_buffer[0].pelvis.translationalAcceleration[:])
			#print(self.state_buffer[1].pelvis.translationalAcceleration[:])

		self.cassie_state = self.state_buffer[len(self.state_buffer) - 1]
		#self.toe_torque += (self.cassie_state.motor.torque[9]/10)**2 + (self.cassie_state.motor.torque[4]/10)**2
		#print(self.cassie_state.motor.torque[9])
		#print("torque",self.cassie_state.motor.torque[:])
		'''if self.phase == 0 and self.time >= 200:
			self.record_state = True
		if self.record_state:
			self.recorded_target.append(target)
			self.recorded_motor_pos.append(self.cassie_state.motor.position[:])
			self.recorded_torque.append(self.cassie_state.motor.torque[:])
			self.recorded_ref.append(ref_pos[pos_index])
			self.recorded_motor_vel.append(self.cassie_state.motor.velocity[:])
			self.recorded_pelvis_pos.append(self.sim.qpos()[0:3])
			self.recorded_pelvis_vel.append(self.sim.qvel()[0:3])
			self.recorded_left_foot_pos.append(foot_pos[0:3])
			self.recorded_right_foot_pos.append(foot_pos[3:6])
			self.recorded_stance_foot_pos.append(stance_foot_pos)
		if len(self.recorded_torque) == 1682*5 and not self.recorded:
			with open('trajectory/target_v0_June', 'wb') as fp:
				pickle.dump(self.recorded_target, fp)
			with open('trajectory/motor_angle_v0_June', 'wb') as fp:
				pickle.dump(self.recorded_motor_pos, fp)
			with open('trajectory/motor_vel_v0_June', 'wb') as fp:
				pickle.dump(self.recorded_motor_vel, fp)
			with open('trajectory/torque_v0_June', 'wb') as fp:
				pickle.dump(self.recorded_torque, fp)
			with open('trajectory/ref_June', 'wb') as fp:
				pickle.dump(self.recorded_ref, fp)
			with open('trajectory/pelvis_pos_speed0', 'wb') as fp:
				pickle.dump(self.recorded_pelvis_pos, fp)
			with open('trajectory/pelvis_vel_speed0', 'wb') as fp:
				pickle.dump(self.recorded_pelvis_vel, fp)
			with open('trajectory/left_foot_pos_speed0', 'wb') as fp:
				pickle.dump(self.recorded_left_foot_pos, fp)
			with open('trajectory/right_foot_pos_speed0', 'wb') as fp:
				pickle.dump(self.recorded_right_foot_pos, fp)
			with open('trajectory/stance_foot_pos_speed0', 'wb') as fp:
				pickle.dump(self.recorded_stance_foot_pos, fp)
			self.recorded = True'''
		'''if self.phase == 0 and self.time >= 200:
			self.record_state = True
		if self.record_state:
			self.recorded_angular_vel.append(self.cassie_state.pelvis.rotationalVelocity[:])
		if len(self.recorded_angular_vel) == 1682*5 and not self.recorded:
			with open('trajectory/pelvis_angular_vel_Jan_noisy', 'wb') as fp:
				pickle.dump(self.recorded_angular_vel, fp)
			self.recorded = True'''

	def reset(self):
		self.orientation = 0
		self.speed = 1#random.randint(0, 10) / 10.0#random.randint(-5, 15) / 10.0#(random.randint(16, 20)) / 10.0#(random.randint(0, 15)) / 10.0#(random.randint(-10, 10)) / 10.0
		#print(self.speed)
		self.y_speed = 0
		orientation = self.orientation# + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, self.max_phase-1)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		#qpos[7] = 0.2
		#qpos[21] = -0.2
		#print(self.rest_pose)
		#print(qpos)
		#qpos = np.copy(self.rest_pose)
		#qvel *= 0
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		self.state_buffer = []
		self.toe_torque = 0
		return self.get_state()

	def reset_for_normalization(self):
		self.orientation = 0
		self.speed = (random.randint(-10, 10)) / 10.0
		self.y_speed = 0
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def reset_by_speed(self, speed, y_speed=0, phase=None):
		self.orientation = 0
		self.speed = speed#(random.randint(-10, 10)) / 10.0
		self.y_speed = 0
		orientation = self.orientation + (random.randint(0, 1) * 2 - 1) * np.pi / 10
		quaternion = euler2quat(z=orientation, y=0, x=0)
		if phase is None:
			self.phase = random.randint(0, 27)
		else:
			self.phase = phase
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()


	def reset_by_phase(self, phase):
		self.orientation = 0
		self.speed = 0#(random.randint(-10, 10)) / 10
		orientation = 0#self.orientation + random.randint(-20, 20) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = phase
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		return self.get_state()

	def reset_by_robot(self, robot_id=0):
		self.orientation = 0
		self.speed = 0#random.randint(-5, 0) / 10.0
		self.y_speed = 0
		orientation = self.orientation + random.randint(-10, 10) * np.pi / 100
		quaternion = euler2quat(z=orientation, y=0, x=0)
		self.phase = 0#random.randint(0, 27)
		self.time = 0
		self.counter = 0
		cassie_sim_free(self.sim.c)
		self.sim.c = cassie_sim_init()
		qpos, qvel = self.get_kin_state()
		qpos[3:7] = quaternion
		self.sim.set_qpos(qpos)
		self.sim.set_qvel(qvel)
		self.cassie_state = self.sim.step_pd(self.u)
		self.sim.perturb_body_mass(bodyid=1, percentage=self.mass_perturbations_list[robot_id])
		self.robot_id = robot_id
		return self.get_state()
