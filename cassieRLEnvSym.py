from cassieRLEnvMirror import *
from cassiemujoco_ctypes import *
import math
import pickle
from quaternion_function import *
import numpy as np

class cassieRLEnvSym(cassieRLEnvMirror):
	mirror_indices = {
	   "neg_obs_inds":[
		   # quat x
		   3,
		   # quat z
		   5,
		   # y velocity
		   17,
		   # x angular speed
		   19,
		   # z angular speed
		   21,
		   45
	   ],
	   "com_obs_inds": [1, 2, 4, 16, 18, 20, 44, 46],
	   "neg_side_inds": [6, 7, 22, 23],
	   "left_obs_inds": list(range(6, 11)) + list(range(22, 27)) + [32, 33, 34, 38, 39, 40],
	   "right_obs_inds": list(range(11, 16)) + list(range(27, 32)) + [35, 36, 37, 41, 42, 43],
	   "left_act_inds": list(range(0, 5)),
	   "right_act_inds": list(range(5, 10)),
	   "neg_act_inds": [],
	}

	mirror_counts = {
		"c_in": 8,
		"n_in": 6,
		"s_in": 17,
		"c_out": 1,
		"n_out": 1,
		"s_out": 5,
	}

	def __init__(self):
		super().__init__()
		self.observation_space = np.zeros(48)
		self.action_space = np.zeros(12)
		self.ref_pos = []
		for i in range(14):
			self.phase = i
			ref_pos, ref_vel = self.get_kin_next_state()
			self.ref_pos.append(ref_pos[[7, 8, 9, 14, 20, 21, 22, 23, 28, 34]])
		for i in range(14, 28):
			ref_pos_pseudo = np.copy(self.ref_pos[i-14])
			ref_pos = np.zeros(10)
			ref_pos[[0, 1]] = -ref_pos_pseudo[[5, 6]]
			ref_pos[[5, 6]] = -ref_pos_pseudo[[0, 1]]
			ref_pos[[2, 3, 4]] = ref_pos_pseudo[[7, 8, 9]]
			ref_pos[[7, 8, 9]] = ref_pos_pseudo[[2, 3, 4]]
			self.ref_pos.append(ref_pos)
		#self.control_rate = 30
		self.vis = CassieVis(self.sim.c)

	def old_state(self):
		if len(self.state_buffer) > 0:
			random_index = random.randint(0, len(self.state_buffer)-1)
			state = self.state_buffer[random_index]
		else:
			state = self.cassie_state
		state = self.cassie_state
		ref_pos, ref_vel = np.copy(self.get_kin_next_state())
		#print(state.motor.position[0])
		#state = self.cassie_state
		#print(ref_pos[3:7])

		pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
		vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])
		quaternion = euler2quat(z=self.orientation, y=0, x=0)
		iquaternion = inverse_quaternion(quaternion)
		new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
		#print(new_orientation)
		new_translationalVelocity = rotate_by_quaternion(state.pelvis.translationalVelocity[:], iquaternion)
		#print(new_translationalVelocity)
		new_translationalAcceleration = rotate_by_quaternion(state.pelvis.translationalAcceleration[:], iquaternion)
		new_rotationalVelocity = rotate_by_quaternion(state.pelvis.rotationalVelocity[:], quaternion)

		pelvis_euler = quaternion2euler(np.copy(state.pelvis.orientation[:]))
		#pelvis_euler[0] = -pelvis_euler[0]
		#pelvis_euler[2] = -pelvis_euler[2]

		useful_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], [1], pelvis_euler[:], state.motor.position[:], new_translationalVelocity[:], state.pelvis.rotationalVelocity[:], state.motor.velocity[:], state.joint.position[:], state.joint.velocity[:], state.pelvis.translationalAcceleration[:]]))
		# return useful_state
		return np.concatenate([[(self.phase%14)/14], useful_state])

	def get_state(self):
		state = self.old_state()
		state[self.mirror_indices["neg_side_inds"]] *= -1
		phase = (self.phase % 28) / 28
		#print(phase, (phase+0.5)%1)

		return np.concatenate([
			state[self.mirror_indices["com_obs_inds"]],
			state[self.mirror_indices["neg_obs_inds"]],
			[phase],
			state[self.mirror_indices["left_obs_inds"]],
			[(phase + 0.5) % 1],
			state[self.mirror_indices["right_obs_inds"]],
		])

	def step_simulation(self, action):
		action = action[2:]
		action[[0,1]] *= -1

		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		target = action + self.ref_pos[self.phase]#self.rest_pose[pos_index]

		#if self.phase == 6 and self.printed:
		#	print("target", target)
		#	print("action", action)
		#	self.printed = False
		#control = self.P*(target-self.sim.data.qpos[index])-self.D*self.sim.data.qvel[vel_index]

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]

		self.state_buffer.append(self.sim.step_pd(self.u))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)
			#print(self.state_buffer[0].pelvis.translationalAcceleration[:])
			#print(self.state_buffer[1].pelvis.translationalAcceleration[:])

		self.cassie_state = self.state_buffer[len(self.state_buffer) - 1]

	'''def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
		joint_penalty = 0
		joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		for i in range(10):
			error = weight[i] * (ref_pos[joint_index[i]]-self.sim.qpos()[joint_index[i]])**2
			joint_penalty += error*30

		pelvis_pos = np.copy(self.cassie_state.pelvis.position[:])
		pelvis_vel = np.copy(self.cassie_state.pelvis.translationalVelocity[:])
		com_penalty = (pelvis_vel[0]-ref_vel[0])**2 + (pelvis_vel[1]-ref_vel[1])**2 + (self.sim.qvel()[2])**2

		yaw = quat2yaw(self.sim.qpos()[3:7])

		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(yaw - self.orientation)**2
		#print(self.sim.qvel()[3])

		foot_forces = self.get_foot_forces()
		force_penalty = 0
		if self.foot_forces[0] < 10 and foot_forces[2] > 10:
			force_penalty += (foot_forces[2]-150)**2
		if self.foot_forces[1] < 10 and foot_forces[8] > 10:
			force_penalty += (foot_forces[8]-150)**2
		self.foot_forces[0] = foot_forces[2]
		self.foot_forces[1] = foot_forces[8]
		#print(force_penalty/1000)
		#print(0.5*np.exp(-joint_penalty), 0.3*np.exp(-com_penalty), 0.2*np.exp(-10*orientation_penalty))
		#print(pelvis_pos[0], ref_pos[0])
		pos = np.zeros(6)
		pos = self.sim.foot_pos(pos)
		foot_height = max(pos[2], pos[5])
		foot_penalty = 0
		if (self.phase <= 5  and self.phase >= 2) or (self.phase <= 19 and self.phase >= 16):
			desire_height = (2 - abs((self.phase % 14) - 3.5)) * 0.05 + 0.075
			#print(desire_height)
			foot_penalty = (min(foot_height - desire_height, 0))**2 * 400

		vel_penalty = (self.sim.qvel()[1]-ref_vel[1])**2 + (self.sim.qvel()[0]-ref_vel[0])**2 + (self.sim.qpos()[2]-1.05)**2 + (self.sim.qvel()[2])**2

		total_reward = 0.8*np.exp(-vel_penalty) + 0.2 * np.exp(-foot_penalty)
		return total_reward'''

