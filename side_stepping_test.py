from cassiemujoco import *
from cassieRLEnvSideStepping import *
import time as t

import argparse
import os
import sys
import gym
from gym import wrappers
import random
import numpy as np

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

from params import Params

import pickle
from model import ActorCriticNet, Shared_obs_stats, ActorCriticNetWithPhase
env = cassieRLEnvSideStepping()
#env = cassieRLEnvStablePelvis()

u = pd_in_t()
u.leftLeg.motorPd.torque[3] = 0 # Feedforward torque
u.leftLeg.motorPd.pTarget[3] = -2
u.leftLeg.motorPd.pGain[3] = 1000
u.leftLeg.motorPd.dTarget[3] = -2
u.leftLeg.motorPd.dGain[3] = 100
u.rightLeg.motorPd = u.leftLeg.motorPd

num_inputs = env.observation_space.shape[0]
num_outputs = env.action_space.shape[0]
model = ActorCriticNet(num_inputs, num_outputs,[256, 256])
model.load_state_dict(torch.load("torch_model/side_steppingForwardBackwardApr04.pt"))
#model.load_state_dict(torch.load("expert_model/MultiTrajBackwardExpertDec03.pt"))

with open('torch_model/cassie3dRLEnvMultiDirectionAll_shared_obs_stats.pkl', 'rb') as input:
	shared_obs_stats = pickle.load(input)

def run_test():
	t.sleep(1)
	state = env.reset()
	total_reward = 0
	done = False
	total_10_reward = 0
	current_scale = 0
	clock = 0
	done = False
	current_phase = 0

	for i in range(10):
		state = env.reset()
		print("speed",env.speed, "y_speed", env.y_speed)
		total_reward = 0
		counter = 0
		interpolate = 1
		while counter < 100000 and not done:
			start = t.time()
			#print(env.speed)
			for j in range(1):
				#state = env.get_mirror_state(env.cassie_state, np.array([1, 0, 0, 0]))
				#print(env.speed)
				counter += 1
				clock += 1
				state = Variable(torch.Tensor(state).unsqueeze(0))
				state = shared_obs_stats.normalize(state)
				mu, log_std, v = model(state)
				eps = torch.randn(mu.size())
				mu = (mu + 0.0*Variable(eps))

				env_action = mu.data.squeeze().numpy()
				state, reward, done, _ = env.step(np.array([env_action[0], env_action[1]]))
				env.vis.draw(env.sim)
				#print(reward)
				total_reward += reward
				force = np.zeros(12)
			while True:
				stop = t.time()
				#print(stop-start)
				if stop - start > 0.03 * env.control_rate / 60:
					break
				#print("stop")
			#total_reward += reward
		done = False
		counter = 0
		total_10_reward += total_reward
		print("total rewards", total_reward)
	print(total_10_reward)

def play_kin():
	env.vis.draw(env.sim)
	#env.set_data()
	env.phase = 0
	#env.speed = -0.0
	env.reset()
	env.phase = 0
	t.sleep(1)
	while True:
		#print(env.phase)
		start = t.time()
		while True:
			stop = t.time()
			#print(stop-start)
			#print("stop")
			if stop - start > 0.033:
				break
		pos, vel = env.get_kin_next_state()
		if env.phase == 6:
			print(pos[[7, 8, 9, 14, 20, 21, 22, 23, 28, 34]])
			print(vel)
			print(env.phase)
		env.phase += 1
		#print(env.speed)
		if env.phase >= env.max_phase:
			env.phase = 0
			env.counter += 1
			#break
		env.set_state(pos, vel)
		y = env.sim.step_pd(u)
		env.vis.draw(env.sim)
#play_kin()
run_test()