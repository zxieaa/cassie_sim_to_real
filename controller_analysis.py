from cassiemujoco import *
from cassieRLEnv import *
from cassieRLEnvInvariant import *
from cassieRLEnvWithMoreState import *
from cassieRLEnvAccelPenalty import *
from cassieRLEnvSimpleJump import *
from cassieRLEnvMultiSkill import *
from cassieRLEnvHorizon import *
from cassieRLEnvMultiDirection import *
from cassieRLEnvMultiTraj import *
from cassieRLEnvMirror import *
from cassieRLEnvTerrain import *
from cassieRLEnvStablePelvis import * 
from cassieRLEnvNoRef import *
from cassieRLEnvSlopeTerrain import *
from cassieRLEnvHigherFoot import *
from cassieRLEnvCleanMotor import *
import time as t

import statistics

import argparse
import os
import sys
import gym
from gym import wrappers
import random
import numpy as np

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data

from params import Params

import pickle
from model import ActorCriticNet, Shared_obs_stats
env = cassieRLEnvMirror()

u = pd_in_t()
u.leftLeg.motorPd.torque[3] = 0 # Feedforward torque
u.leftLeg.motorPd.pTarget[3] = -2
u.leftLeg.motorPd.pGain[3] = 1000
u.leftLeg.motorPd.dTarget[3] = -2
u.leftLeg.motorPd.dGain[3] = 100
u.rightLeg.motorPd = u.leftLeg.motorPd

num_inputs = env.observation_space.shape[0]
num_outputs = env.action_space.shape[0]

model = ActorCriticNet(num_inputs, num_outputs, [16, 16])
model.load_state_dict(torch.load("expert_model/SupervisedModel16X16Jan17.pt"))

#model = ActorCriticNet(num_inputs, num_outputs, [64, 64])
#model.load_state_dict(torch.load("expert_model/StablePelvis64X64Jan12.pt"))

expert_model = ActorCriticNet(num_inputs, num_outputs,[256, 256])
expert_model.load_state_dict(torch.load(("torch_model/MirrorNov11.pt")))
residual_model = ActorCriticNet(num_inputs, num_outputs, [256, 256])
residual_model.load_state_dict(torch.load("torch_model/StablePelvisNov14_v2.pt"))
#model.load_state_dict(torch.load("expert_model/MultiTrajBackwardExpertDec03.pt"))

with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
	shared_obs_stats = pickle.load(input)
#with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
#	shared_obs_stats = pickle.load(input)

state_list = []
env.visualize = True

def run_test():
	t.sleep(1)
	state = env.reset()
	total_reward = 0
	done = False
	total_10_reward = 0
	current_scale = 0
	clock = 0
	done = False
	current_phase = 0
	reward_list = []

	weight1 = model.p_fcs[0].weight
	weight2 = model.p_fcs[1].weight
	state = env.reset_by_phase(phase=4)
	state = Variable(torch.Tensor(state).unsqueeze(0))
	state = shared_obs_stats.normalize(state)
	output1 = (model.p_fcs[0](state))
	output1 = F.relu(output1)
	output2 = F.relu(model.p_fcs[1](output1))
	activation_index = []
	output1_np = output1.data.squeeze().numpy()
	output2_np = output2.data.squeeze().numpy()
	for i in range(output1_np.shape[0]):
		#if output1_np[i] > 0:
		#	activation_index.append(i)
		if output2_np[i] > 0:
			activation_index.append(i)
			#print(i)
	print((activation_index))

	for i in range(10):
		state = env.reset()
		#env.speed = -1.0
		total_reward = 0
		counter = 0
		interpolate = 1
		while counter < 100000 and not done:
			start = t.time()
			#print(env.speed)
			for j in range(1):
				counter += 1
				clock += 1
				state = Variable(torch.Tensor(state).unsqueeze(0))
				state = shared_obs_stats.normalize(state)
				mu, log_std, v = model(state)
				eps = torch.randn(mu.size())
				mu_expert, _, _ = expert_model(state)
				mu_residual, _, _ = residual_model(state)

				mu = (mu + 0.0*1*Variable(eps))

				if env.phase == 1:
					output1 = (model.p_fcs[0](state))
					output1 = F.relu(output1)
					output2 = (model.p_fcs[1](output1))
					activation_index = []
					output1_np = output1.data.squeeze().numpy()
					output2_np = output2.data.squeeze().numpy()
					for i in range(output1_np.shape[0]):
						#if output1_np[i] > 0:
						#	activation_index.append(i)
						if output1_np[i] > 0:
							activation_index.append(i)
					print("phase", env.phase, (activation_index), print(output1_np), print(mu.data))

				env_action = mu.data.squeeze().numpy() * 1
				state, reward, done, _ = env.step(env_action)
				env.vis.draw(env.sim)
				#print(reward)
				total_reward += reward
				force = np.zeros(12)
				pos = np.zeros(6)
			while True:
				stop = t.time()
				#print(stop-start)
				if stop - start > 0.03 * env.control_rate / 60:
					break
				#print("stop")
			#total_reward += reward
		done = False
		counter = 0
		reward_list.append(total_reward)
		total_10_reward += total_reward
		print("total rewards", total_reward)

run_test()