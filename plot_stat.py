import pickle
import numpy as np
import matplotlib.pyplot as plt
import statistics


#stats for different size
#stats for 32X32
'''with open ("stats/Mirror32X32Dec12Iter601.stat", 'rb') as fp:
	stats32_v1 = pickle.load(fp)
with open ("stats/Mirror32X32Dec13Iter601.stat", 'rb') as fp:
	stats32_v2 = pickle.load(fp)
with open ("stats/Mirror32X32Dec14Iter601.stat", 'rb') as fp:
	stats32_v3 = pickle.load(fp)
mean32 = stats32_v3[0]#[statistics.mean(k) for k in zip(stats32_v1[0], stats32_v2[0], stats32_v3[0])]
std32 = stats32_v3[1]#[statistics.stdev(k) for k in zip(stats32_v1[0], stats32_v2[0], stats32_v3[0])]
low32 = []
high32 = []

#stats for 64X64
with open ("stats/Mirror64X64Dec12Iter601.stat", 'rb') as fp:
	stats64_v1 = pickle.load(fp)
with open ("stats/Mirror64X64Dec13Iter601.stat", 'rb') as fp:
	stats64_v2 = pickle.load(fp)
with open ("stats/Mirror64X64Dec14Iter601.stat", 'rb') as fp:
	stats64_v3 = pickle.load(fp)
mean64 = stats64_v3[0]#[statistics.mean(k) for k in zip(stats64_v1[0], stats64_v2[0],stats64_v3[0])]
std64 = stats64_v3[1]#[statistics.stdev(k) for k in zip(stats64_v1[0], stats64_v2[0],stats64_v3[0])]
low64 = []
high64 = []

#stats for 256X256
with open ("stats/Mirror256X256Dec12Iter601.stat", 'rb') as fp:
	stats256_v1 = pickle.load(fp)
with open ("stats/Mirror256X256Dec13Iter601.stat", 'rb') as fp:
	stats256_v2 = pickle.load(fp)
with open ("stats/Mirror256X256Dec14Iter601.stat", 'rb') as fp:
	stats256_v3 = pickle.load(fp)
mean256 = stats256_v3[0]#[statistics.mean(k) for k in zip(stats256_v1[0], stats256_v2[0], stats256_v3[0])]
std256 = stats256_v3[1]#[statistics.stdev(k) for k in zip(stats256_v1[0], stats256_v2[0], stats256_v3[0])]
low256 = []
high256 = []


fig = plt.figure()
ax = fig.add_subplot(111)
index = []
for i in range(len(mean256)):
	low256.append(mean256[i] - std256[i])
	high256.append(mean256[i] + std256[i])
	low64.append(mean64[i] - std64[i])
	high64.append(mean64[i] + std64[i])
	low32.append(mean32[i] - std32[i])
	high32.append(mean32[i] + std32[i])
	index.append(i*3000)

plt.xlabel('samples')
plt.ylabel('average rewards')
ax.plot(index, mean256, 'b', label="(256, 256)")
ax.fill_between(index, low256, high256, alpha=1, color='royalblue')
ax.plot(index, mean64, 'r', label="(64, 64)")
ax.fill_between(index, low64, high64, alpha=0.5, color='salmon')
ax.plot(index, mean32, 'g', label="(32, 32)")
ax.fill_between(index, low32, high32, alpha=0.3, color='lightgreen')
plt.legend(loc='upper left')
#ax.set_xlim([0,2e6])
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.show()'''

#stats for multislope training
'''with open ("stats/Slope_plusdegree_Iter801.stat", 'rb') as fp:
	stats_multislope = pickle.load(fp)
mean_multislope = stats_multislope[0]
std_multislope = stats_multislope[1]
low_multislope = []
high_multislope = []

#stats for 30 degree only
with open ("stats/Slope_30degree_Iter801.stat", 'rb') as fp:
	stats_30degree = pickle.load(fp)
mean_30degree = stats_30degree[0]
std_30degree = stats_30degree[1]
low_30degree = []
high_30degree = []

#stats for curriculum
with open ("stats/Slope_0degree_Iter201.stat", 'rb') as fp:
	stats_0degree = pickle.load(fp)
with open ("stats/Slope_0to10degree_Iter201.stat", 'rb') as fp:
	stats_0to10degree = pickle.load(fp)
with open ("stats/Slope_10to20degree_Iter201.stat", 'rb') as fp:
	stats_10to20degree = pickle.load(fp)
with open ("stats/Slope_20to30degree_Iter201.stat", 'rb') as fp:
	stats_20to30degree = pickle.load(fp)
mean_curriculum = stats_0degree[0] + stats_0to10degree[0] + stats_10to20degree[0] + stats_20to30degree[0] #+  stats_20to30degree[0]
std_curriculum = stats_0degree[1] + stats_0to10degree[1] + stats_10to20degree[1] + stats_20to30degree[1] #+  stats_20to30degree[1]
low_curriculum = []
high_curriculum = []

fig = plt.figure()
ax = fig.add_subplot(111)
index = []
for i in range(800):
	low_multislope.append(mean_multislope[i]-std_multislope[i])
	high_multislope.append(mean_multislope[i]+std_multislope[i])
	low_30degree.append(mean_30degree[i]-std_30degree[i])
	high_30degree.append(mean_30degree[i]+std_30degree[i])
	low_curriculum.append(mean_curriculum[i]-std_curriculum[i])
	high_curriculum.append(mean_curriculum[i]+std_curriculum[i])
	index.append(i)

plt.xlabel('iterations')
plt.ylabel('average rewards')
ax.plot(mean_multislope, 'b', label="multiskill")
ax.fill_between(index, low_multislope, high_multislope, alpha=1, color='royalblue')
ax.plot(mean_30degree, 'r', label="30 degree only")
ax.fill_between(index, low_30degree, high_30degree, alpha=0.5, color='salmon')
ax.plot(mean_curriculum, 'g', label="curriculum")
ax.fill_between(index, low_curriculum, high_curriculum, alpha=1, color='lightgreen')
plt.legend(loc='upper left')
plt.show()'''

#stats for multiskill
# with open ("stats/ForwardBackwardIter201_v2.stat", 'rb') as fp:
# 	stats_multiskill = pickle.load(fp)
# mean_multiskill = stats_multiskill[0]
# std_multiskill = stats_multiskill[1]
# low_multiskill = []
# high_multiskill = []

# #stats for distill
# with open ("stats/BackwardIter201_v2.stat", 'rb') as fp:
# 	stats_backward = pickle.load(fp)
# mean_backward = stats_backward[0]
# std_backward = stats_backward[1]
# low_backward = []
# high_backward = []

# with open ("stats/ForwardIter201_v3.stat", 'rb') as fp:
# 	stats_forward = pickle.load(fp)
# mean_forward = stats_forward[0]
# std_forward = stats_forward[1]
# low_forward = []
# high_forward = []

# with open ("stats/ForwardBackwardIter201_v3.stat", 'rb') as fp:
# 	stats_multiskill_forward = pickle.load(fp)
# mean_multiskill_forward = stats_multiskill_forward[0]
# std_multiskill_forward = stats_multiskill_forward[1]
# low_multiskill_forward = []
# high_multiskill_forward = []

# fig = plt.figure()
# ax = fig.add_subplot(111)
# index = []
# for i in range(201):
# 	low_multiskill.append(mean_multiskill[i]-std_multiskill[i])
# 	high_multiskill.append(mean_multiskill[i]+std_multiskill[i])
# 	low_backward.append(mean_backward[i]-std_backward[i])
# 	high_backward.append(mean_backward[i]+std_backward[i])
# 	low_forward.append(mean_forward[i]-std_forward[i])
# 	high_forward.append(mean_forward[i]+std_forward[i])
# 	low_multiskill_forward.append(mean_multiskill_forward[i]-std_multiskill_forward[i])
# 	high_multiskill_forward.append(mean_multiskill_forward[i]+std_multiskill_forward[i])
# 	index.append(i * 30000)

# ax.plot(mean_forward, 'g', label="forward only")
# ax.fill_between(index, low_forward, high_forward, alpha=1, color='lightgreen')
# ax.plot(index, mean_backward, 'r', label="backward only")
# ax.fill_between(index, low_backward, high_backward, alpha=0.5, color='salmon')
# ax.plot(mean_multiskill_forward, 'black', label="multiskill_forward")
# ax.fill_between(index, low_multiskill_forward, high_multiskill_forward, alpha=0.5, color='gray')
# plt.xlabel('samples')
# plt.ylabel('average rewards')
# ax.plot(index, mean_multiskill, 'b', label="multiskill_backward")
# ax.fill_between(index, low_multiskill, high_multiskill, alpha=1, color='royalblue')
# plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
# ax.set_xlim([0,6e6])
# plt.legend(loc='upper left')
# #ax.plot(mean_curriculum, 'g', label="curriculum")
# plt.show()

#stats for net
with open ("stats/SymStatsJuly17101_v2.stat", 'rb') as fp:
	stats_net = pickle.load(fp)
mean_net = stats_net[0]
std_net = stats_net[1]
low_net = []
high_net = []

#stats for phase mirror
with open ("stats/MirrorJuly11Iter101_v2.stat", 'rb') as fp:
	stats_mirror = pickle.load(fp)
mean_mirror = stats_mirror[0]
std_mirror = stats_mirror[1]
low_mirror = []
high_mirror = []

#stats for non mirror
with open ("stats/nonMirrorJuly11Iter101_v2.stat", 'rb') as fp:
	stats_nonmirror = pickle.load(fp)
mean_nonmirror = stats_nonmirror[0]
std_nonmirror = stats_nonmirror[1]
low_nonmirror = []
high_nonmirror = []


fig = plt.figure()
ax = fig.add_subplot(111)
index = []
for i in range(101):
	low_net.append(mean_net[i]-std_net[i])
	high_net.append(mean_net[i]+std_net[i])
	low_mirror.append(mean_mirror[i]-std_mirror[i])
	high_mirror.append(mean_mirror[i]+std_mirror[i])
	low_nonmirror.append(mean_nonmirror[i]-std_nonmirror[i])
	high_nonmirror.append(mean_nonmirror[i]+std_nonmirror[i])
	index.append(i * 9000)

ax.plot(index, mean_net, 'g', label="net")
ax.fill_between(index, low_net, high_net, alpha=1, color='lightgreen')
ax.plot(index, mean_mirror, 'b', label="mirror")
ax.fill_between(index, low_mirror, high_mirror, alpha=1, color='royalblue')
ax.plot(index, mean_nonmirror, 'r', label="non_mirror")
ax.fill_between(index, low_nonmirror, high_nonmirror, alpha=1, color='salmon')
plt.xlabel('samples')
plt.ylabel('average rewards')
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
ax.set_xlim([0,1e6])
plt.legend(loc='upper left')
#ax.plot(mean_curriculum, 'g', label="curriculum")
plt.show()