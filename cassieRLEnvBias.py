from cassiemujoco_ctypes import *
import math
import pickle
from quaternion_function import *
from cassieRLEnvMultiTraj import *
from cassieRLEnvMultiDirection import *
from cassieRLEnvMirror import *
from cassieRLEnvMirrorWithTransition import *
from cassieRLEnvStablePelvis import *
from cassieRLEnvIKBackward import *

class cassieRLEnvBias(cassieRLEnvStablePelvis):
	def __init__(self):
		super().__init__()
		self.speed = 0
		self.observation_space = np.zeros(85)
		self.control_rate = 60
		self.max_phase = 28
		self.time_limit = 400
		self.phase_vel = 1

		self.rest_pose = np.array([0, 0, 0, 0, 0, 0, 0, 0.0045, 0, 0.4973, 0.9785, -0.0164, 0.01787, -0.2049,
         -1.1997, 0, 1.4267, 0, -1.5244, 1.5244, -1.5968,
         -0.0045, 0, 0.4973, 0.9786, 0.00386, -0.01524, -0.2051,
         -1.1997, 0, 1.4267, 0, -1.5244, 1.5244, -1.5968])

		self.push_bias = 0

	'''def compute_reward(self):
		ref_pos, ref_vel = self.get_kin_state()
		yaw = quaternion2euler(self.sim.qpos()[3:7])
		orientation_penalty = (self.sim.qpos()[4])**2+(self.sim.qpos()[5])**2+(self.sim.qpos()[6])**2
		orientation_vel_penalty = (self.sim.qvel()[3])**2+(self.sim.qvel()[4])**2+(self.sim.qvel()[5])**2
		orientation_acc_penalty = (0.1*self.sim.qacc()[3])**2+(0.1*self.sim.qacc()[4])**2+(0.1*self.sim.qacc()[5])**2
		vel_penalty = (self.sim.qpos()[1]-ref_pos[1])**2+(self.sim.qvel()[2])**2+(self.sim.qpos()[0]-ref_pos[0])**2
		#print(self.sim.qvel()[0:3])

		pelvis_vel = np.copy(self.cassie_state.pelvis.translationalVelocity[:])
		com_penalty = (pelvis_vel[0]-ref_vel[0])**2 + (pelvis_vel[1]-ref_vel[1])**2 + (self.sim.qvel()[2])**2

		pos = np.zeros(6)
		pos = self.sim.foot_pos(pos)
		foot_height = max(pos[2], pos[5])
		foot_penalty = 0
		if (self.phase <= 5  and self.phase >= 2) or (self.phase <= 19 and self.phase >= 16):
			desire_height = (2 - abs((self.phase % 14) - 3.5)) * 0.05 + 0.075
			#print(desire_height)
			foot_penalty = (min(foot_height - desire_height, 0))**2 * 400
		return 0.5*np.exp(-1*orientation_vel_penalty)+0.4*np.exp(-10*com_penalty)+0.1*np.exp(-foot_penalty)'''

	def step_simulation(self, action):
		qpos = np.copy(self.sim.qpos())
		qvel = np.copy(self.sim.qvel())

		pos_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
		vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]

		ref_pos, ref_vel = self.get_kin_next_state()
		if self.phase < 14:
			target = action + ref_pos[pos_index]
		else:
			mirror_action = np.zeros(10)
			mirror_action[0:5] = np.copy(action[5:10])
			mirror_action[5:10] = np.copy(action[0:5])
			mirror_action[0] = -mirror_action[0]
			mirror_action[1] = -mirror_action[1]
			mirror_action[5] = -mirror_action[5]
			mirror_action[6] = -mirror_action[6]
			target = mirror_action + ref_pos[pos_index]

		self.u = pd_in_t()
		for i in range(5):
			self.u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.leftLeg.motorPd.pTarget[i] = target[i]
			self.u.leftLeg.motorPd.pGain[i] = self.P[i]
			self.u.leftLeg.motorPd.dTarget[i] = 0
			self.u.leftLeg.motorPd.dGain[i] = self.D[i]
			self.u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
			self.u.rightLeg.motorPd.pTarget[i] = target[i+5]
			self.u.rightLeg.motorPd.pGain[i] = self.P[i+5]
			self.u.rightLeg.motorPd.dTarget[i] = 0
			self.u.rightLeg.motorPd.dGain[i] = self.D[i+5]

		self.state_buffer.append(self.sim.step_pd(self.u))
		if len(self.state_buffer) > self.buffer_size:
			self.state_buffer.pop(0)

		self.cassie_state = self.state_buffer[len(self.state_buffer) - 1]

	def step(self, action):
		#print(based_action, action)
		for _ in range(self.control_rate):
			self.step_simulation(action)
		#print("phase", self.phase)
		#print("action", based_action+action)

		height = self.sim.qpos()[2]
		#print("height", height)
		self.time += 1
		self.phase += self.phase_vel

		if self.phase >= self.max_phase:
			self.phase -= self.max_phase
			self.counter +=1
		#print("height", height)

		done = not(height > 0.4 and height < 100.0) or self.time >= self.time_limit
		yaw = quat2yaw(self.sim.qpos()[3:7])


		reward = self.compute_reward()
		#if reward < 0.3:
		#	done = True
		return self.get_state(), reward, done, {}

	def get_state(self):
		state = super().get_state()
		#state[46:] = np.zeros(39)
		#state[46] = self.speed
		#state[47] = self.phase / 28.0
		return state


	def reset(self):
		state = super().reset()
		self.phase_vel = 1#random.randint(1, 2)
		self.push_bias = random.randint(-10, 10) / 10.0
		self.sim.apply_force(np.array([self.push_bias * 10,0,0,0,0,0]))
		return state