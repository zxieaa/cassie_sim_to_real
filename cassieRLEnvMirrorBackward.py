from cassiemujoco_ctypes import *
import math
import pickle
from quaternion_function import *
from cassieRLEnvMirrorWithTransition import *

class cassieRLEnvMirrorBackward(cassieRLEnvMirrorWithTransition):
	def __init__(self):
		super().__init__()

	def get_kin_state(self):
		if self.speed < 0:
			phase = self.phase
			#pose = np.copy(self.trajectory.qpos[phase*self.control_rate])
			#pose[0] = self.trajectory.qpos[self.phase*self.control_rate, 0]
			#pose[0] = pose[0] * self.speed
			#pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			#pose[1] = 0
			#vel = np.copy(self.trajectory.qvel[phase*self.control_rate])
			#vel[0] *= self.speed
			pose = np.copy(self.step_in_place_trajectory[self.phase*self.control_rate][0])
			pose[0] = self.trajectory.qpos[self.phase*self.control_rate, 0]
			pose[0] *= self.speed
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel = np.copy(self.trajectory.qvel[self.phase*self.control_rate])
			vel[0] *= self.speed
		else:
			pose = np.copy(self.trajectory.qpos[self.phase*self.control_rate])
			pose[0] *= self.speed
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel = np.copy(self.trajectory.qvel[self.phase*self.control_rate])
			vel[0] *= self.speed
		pose[3] = 1
		pose[4:7] = 0
		pose[7] = 0
		pose[8] = 0
		pose[21] = 0
		pose[22] = 0
		vel[6] = 0
		vel[7] = 0
		vel[19] = 0
		vel[20] = 0
		return pose, vel
	def get_kin_next_state(self):
		if self.speed < 0:
			phase = self.phase + 1
			if phase >= self.max_phase:
				phase = 0
			#pose = np.copy(self.trajectory.qpos[phase*2*30])
			#vel = np.copy(self.trajectory.qvel[phase*2*30])
			#pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			#print(pose[0])
			#pose[1] = 0
			#vel[0] *= self.speed
			pose = np.copy(self.step_in_place_trajectory[phase*self.control_rate][0])
			pose[0] = self.trajectory.qpos[phase*self.control_rate, 0]
			pose[0] *= self.speed
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel = np.copy(self.trajectory.qvel[phase*self.control_rate])
			vel[0] *= self.speed
		else:
			phase = self.phase + 1
			if phase >= self.max_phase:
				phase = 0
			pose = np.copy(self.trajectory.qpos[phase*2*30])
			pose[0] *= self.speed
			vel = np.copy(self.trajectory.qvel[phase*2*30])
			pose[0] += (self.trajectory.qpos[1681, 0]- self.trajectory.qpos[0, 0])* self.counter * self.speed
			pose[1] = 0
			vel[0] *= self.speed
		pose[3] = 1
		pose[4:7] = 0
		pose[7] = 0
		pose[8] = 0
		pose[21] = 0
		pose[22] = 0
		vel[6] = 0
		vel[7] = 0
		vel[19] = 0
		vel[20] = 0
		return pose, vel
