from cassiemujoco import *
from cassiemujoco_ctypes import *
from cassieRLEnv import *
from cassieRLEnvInvariant import *
from cassieRLEnvWithMoreState import *
from cassieRLEnvAccelPenalty import *
from cassieRLEnvMultiSkill import *
from cassieRLEnvMultiTraj import *
from cassieRLEnvMirror import *
import time

import ctypes
import argparse
import os
import sys
import gym
from gym import wrappers
import random
import numpy as np
import ctypes

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.utils.data
from plot import *

from params import Params

import pickle
from model import ActorCriticNet, Shared_obs_stats

import socket

import quaternion_function


reorder_motor_matrix = np.zeros((10, 10))
reorder_motor_matrix[0:5, 5:10] = np.identity(5)
reorder_motor_matrix[5:10, 0:5] = np.identity(5)
reorder_motor_matrix[0, 5] = -1
reorder_motor_matrix[1, 6] = -1
reorder_motor_matrix[5, 0] = -1
reorder_motor_matrix[6, 1] = -1

reorder_joint_matrix = np.zeros((6, 6))
reorder_joint_matrix[0:3, 3:6] = np.identity(3)
reorder_joint_matrix[3:6, 0:3] = np.identity(3)

reorder_position_matrix = np.identity(3)
reorder_position_matrix[1, 1] = -1

reorder_orientation_matrix = np.identity(3) * -1
reorder_orientation_matrix[1, 1] = 1


def get_mirror_state(state, quaternion):
    #pelvis_euler = reorder_orientation_matrix.dot(quaternion2euler(np.copy(state.pelvis.orientation[:])))
    #pelvis_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
    iquaternion = inverse_quaternion(quaternion)
    new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
    if new_orientation[0] < 0:
        new_orientation = -new_orientation
    pelvis_euler = reorder_orientation_matrix.dot(quaternion2euler(new_orientation))
    new_orientation = euler2quat(z=pelvis_euler[2],y=pelvis_euler[1],x=pelvis_euler[0])
    #print(new_orientation)

    translationalVelocity = reorder_position_matrix.dot(state.pelvis.translationalVelocity[:])
    translationalAcceleration = reorder_position_matrix.dot(state.pelvis.translationalAcceleration[:])
    rotationalVelocity = reorder_orientation_matrix.dot(state.pelvis.rotationalVelocity[:])
    motor_position = reorder_motor_matrix.dot(state.motor.position[:])
    motor_velocity = reorder_motor_matrix.dot(state.motor.velocity[:])
    joint_position = reorder_joint_matrix.dot(state.joint.position[:])
    joint_velocity = reorder_joint_matrix.dot(state.joint.velocity[:])

    new_translationalVelocity = rotate_by_quaternion(translationalVelocity, quaternion)

    cassie_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], motor_position[:], new_translationalVelocity[:], rotationalVelocity[:], motor_velocity[:], translationalAcceleration[:], joint_position, joint_velocity]))
    return cassie_state

def compute_reward(state):
    ref_pos, ref_vel = env.get_kin_state()
    weight = [0.15, 0.15, 0.1, 0.05, 0.05, 0.15, 0.15, 0.1, 0.05, 0.05]
    joint_penalty = 0
    joint_index = [7, 8, 9, 14, 20, 21, 22, 23, 28, 34]
    vel_index = [6, 7, 8, 12, 18, 19, 20, 21, 25, 31]
    for i in range(10):
        error = weight[i] * (ref_pos[joint_index[i]]-state.motor.position[i])**2
        joint_penalty += error*30
    pelvis_pos = np.copy(state.pelvis.position[:])
    com_penalty = (pelvis_pos[0]-ref_pos[0])**2 + (pelvis_pos[1]-ref_pos[1])**2 + (pelvis_pos[2]-ref_pos[2])**2
    yaw = quat2yaw(state.pelvis.orientation[:])
    orientation_penalty = (state.pelvis.orientation[1])**2+(state.pelvis.orientation[2])**2+(yaw - env.orientation)**2
    total_reward = 0.5*np.exp(-joint_penalty)+0.3*np.exp(-com_penalty)+0.2*np.exp(-10*orientation_penalty)
    return total_reward


client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client_socket.settimeout(1.0)
addr = ("127.0.0.1", 25000)
env = cassieRLEnvMirror()
env.phase = 0#15 for high foot #10 for others
env.counter = 0
env.speed = 0
num_inputs = env.observation_space.shape[0]
num_outputs = env.action_space.shape[0]
model = ActorCriticNet(num_inputs, num_outputs,[64, 64])
model.load_state_dict(torch.load("expert_model/MirrorStablePelvisSupervisedModelDec2564X64.pt"))



with open('torch_model/cassie3dMirror2kHz_shared_obs_stats.pkl', 'rb') as input:
    shared_obs_stats = pickle.load(input)

state_out_t = struct_c__SA_state_out_t
# recvbuf = []
recvlen = 495
recvbuf = (ctypes.c_ubyte * 495)()

sendlen = 478
sendbuf = (ctypes.c_ubyte * 478)()

counter = 0
state = state_out_t()
u = pd_in_t()
u_test = pd_in_t()
target = np.zeros(10)
P = np.array([100, 100, 88, 96, 50, 100, 100, 88, 96, 50])
D = np.array([10.0, 10.0, 8.0, 9.6, 5.0, 10.0, 10.0, 8.0, 9.6, 5.0])
#P = np.array([200, 200, 200, 200, 50, 200, 200, 200, 200, 50])
#D = np.array([30.0, 30.0, 30.0, 30.0, 5.0, 30.0, 30.0, 30.0, 30.0, 5.0])

for i in range(5):
	u.leftLeg.motorPd.torque[i] = 0 # Feedforward torque
	u.leftLeg.motorPd.pTarget[i] = target[i]
	u.leftLeg.motorPd.pGain[i] = P[i]
	u.leftLeg.motorPd.dTarget[i] = 0
	u.leftLeg.motorPd.dGain[i] = D[i]
	u.rightLeg.motorPd.torque[i] = 0 # Feedforward torque
	u.rightLeg.motorPd.pTarget[i] = target[i+5]
	u.rightLeg.motorPd.pGain[i] = P[i+5]
	u.rightLeg.motorPd.dTarget[i] = 0
	u.rightLeg.motorPd.dGain[i] = D[i+5]

#pos_index = np.array([1, 2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
pos_index = np.array([2,3,4,5,6,7,8,9,14,15,16,20,21,22,23,28,29,30,34])
vel_index = np.array([0,1,2,3,4,5,6,7,8,12,13,14,18,19,20,21,25,26,27,31])
pos_mirror_index = np.array([2,3,4,5,6,21,22,23,28,29,30,34,7,8,9,14,15,16,20])
vel_mirror_index = np.array([0,1,2,3,4,5,19,20,21,25,26,27,31,6,7,8,12,13,14,18])

pack_pd_in_t(u, sendbuf)
#model.eval()
env.orientation = 0
cassie = CassieUdp()
y = None
while y is None:
    cassie.send_pd(pd_in_t())
    time.sleep(0.001)
    y = cassie.recv_newest_pd()
received_data = True

t = time.monotonic()
torch.set_num_threads(1)
#env.speed = 0.0

time_step = 0

diff = 0.1
total_reward = 0

while True:
    # Wait until next cycle time
    while time.monotonic() - t < 60/2000:
        time.sleep(0.000001)
    t = time.monotonic()

    # Get newest state
    state = cassie.recv_newest_pd()
    if state is None:
        print('Missed a cycle')
        continue

    if env.phase < 14:
        quaternion = euler2quat(z=env.orientation, y=0, x=0)
        iquaternion = inverse_quaternion(quaternion)
        new_orientation = quaternion_product(iquaternion, state.pelvis.orientation[:])
        if new_orientation[0] < 0:
        	new_orientation = -new_orientation
        new_translationalVelocity = rotate_by_quaternion(state.pelvis.translationalVelocity[:], iquaternion)

        # Construct input vector
        cassie_state = np.copy(np.concatenate([[state.pelvis.position[2] - state.terrain.height], new_orientation[:], state.motor.position[:], new_translationalVelocity[:], state.pelvis.rotationalVelocity[:], state.motor.velocity[:], state.pelvis.translationalAcceleration[:], state.joint.position[:], state.joint.velocity[:]]))
        ref_pos, ref_vel = env.get_kin_next_state()
        RL_state = np.concatenate([cassie_state, ref_pos[pos_index], ref_vel[vel_index]])
    else:
        quaternion = euler2quat(z=env.orientation, y=0, x=0)
        cassie_state = get_mirror_state(state, quaternion)
        ref_pos, ref_vel = env.get_kin_next_state()
        ref_vel[1] = -ref_vel[1]
        euler = quaternion2euler(ref_pos[3:7])
        euler[0] = -euler[0]
        euler[2] = -euler[2]
        ref_pos[3:7] = euler2quat(z=euler[2],y=euler[1],x=euler[0])
        RL_state = np.concatenate([cassie_state, ref_pos[pos_mirror_index], ref_vel[vel_mirror_index]])


    RL_state = torch.Tensor(RL_state).unsqueeze(0)
    RL_state = shared_obs_stats.normalize(RL_state)

    # Get action
    mu, log_std, v = model(RL_state)
    env_action = mu.data.squeeze().numpy()
    ref_action = env.ref_action()
    if env.phase < 14:
        target = ref_action + env_action
    else:
        env_action = reorder_motor_matrix.dot(env_action)
        target = ref_action + env_action

    # Send action
    for i in range(5):
        u.leftLeg.motorPd.pTarget[i] = target[i]
        u.rightLeg.motorPd.pTarget[i] = target[i+5]
    #time.sleep(0.005)
    cassie.send_pd(u)

    time_step += 1

    # Measure delay
    '''print('delay: {:6.1f} ms'.format((time.monotonic() - t) * 1000))
    total_reward += compute_reward(state)
    if time_step % 100 == 0:
        print("time_step", time_step, "total_reward", total_reward)'''

    # Track phase
    env.phase += 1
    if env.phase >= 28:
        env.phase = 0
        env.counter += 1
        env.speed += 0.1

        if env.speed >= 0.5:
        	env.speed = 0.5
        #env.speed = -0.1
        #if env.counter % 2 == 1:
        #	env.orientation += 0.1 * np.pi
        print(env.speed)
        	#print(env.orientation)
    #env.speed = -0.0
    #env.orientation = 0
    #print(env.speed)
    #print(time.time()-start)
        #if env.speed < 1:
        #	env.speed += 0.001
		#counter = 0
	#print(counter)


	#pack_pd_in_t(u, sendbuf)
	#client_socket.sendto(sendbuf, addr)
	#unpack_pd_in_t(sendbuf, u_test)
	#print(u_test.leftLeg.motorPd.pGain[1])
	#print(sendbuf[:])