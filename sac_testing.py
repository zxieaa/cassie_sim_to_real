from RL_SAC import *

env = gym.make('HumanoidBulletEnv-v0')

with open('torch_model/humanoid_shared_obs_stats.pkl', 'rb') as input:
	shared_obs_stats = pickle.load(input)

num_inputs = env.observation_space.shape[0]
num_outputs = env.action_space.shape[0]
actor = ActorNet(num_inputs, num_outputs, [256, 256])
actor.load_state_dict(torch.load("torch_model/humanoid.pt"))

#shared_obs_stats = Shared_obs_stats(num_inputs)

done = False
env.render()
state = env.reset()
rewards = 0
steps = 0
for i in range(10):
	while not done:
		start = t.time()
		state = Variable(torch.Tensor(state).unsqueeze(0))
		state = shared_obs_stats.normalize(state)
		#print(state)
		action, _, mu, _= actor.sample(state)
		env_action = mu.data.squeeze().numpy()
		#print(action - mu)
		#print(env.y_speed)
		state, reward, done, _ = env.step(env_action)
		print(env_action)
		rewards += reward
		steps += 1
		#env.vis.draw(env.sim)
		env.unwrapped._p.resetDebugVisualizerCamera(5, 0, -20, env.unwrapped.robot.body_xyz)
		while True:
			stop = t.time()
			if stop - start > 0.033:
				break
	state = env.reset()
	done = False
	print(rewards, steps)
	rewards = 0
	steps = 0